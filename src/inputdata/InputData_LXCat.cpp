#include "InputData_LXCat.h"

#include <iostream>
#include <fstream>

#include "GeneralUtilities.h"

InputData_LXCat::InputData_LXCat(std::string filename, std::string process_type, std::string reaction, double unit_x/*=1.0*/, double unit_y/*=1.0*/)
  : InputData(filename, true){

  _sanitized_reaction = TD::lowercase_and_remove_spaces(reaction);
  std::cout << _sanitized_reaction << std::endl;

  _sanitized_process_type = TD::lowercase_and_remove_spaces(process_type);

  _read_file();

  _n_datapoints = _x.size();

  multiply_x(unit_x);
  multiply_y(unit_y);

  _sort_x_y();
}

InputData_LXCat::~InputData_LXCat(){}

//Getters
////////////////////////////////////////////////////////////////////////////////
std::string InputData_LXCat::reaction() const{
  return _sanitized_reaction;
}

std::string InputData_LXCat::process_type() const{
  return _sanitized_process_type;
}
////////////////////////////////////////////////////////////////////////////////

void InputData_LXCat::_read_file(){
  std::string line;
  std::string token;

  // Dictates if process type matches: effective, elastic, excitation, ionization, or attachment
  bool is_correct_process_type = false;
  // Checks if the reaction is correct
  bool is_correct_reaction = false;
  // The block of data can still be wrong. For elastic we should not take
  // any "total" cross sections. We will look at comments for this bool
  bool is_correct_block = false;

  // We found the "-----" lines segmenting the data we want
  bool first_data_delimiter_line_found = false;
  bool second_data_delimiter_line_found = false;

  std::ifstream data_file;
  data_file.open(_filename);

  while(std::getline(data_file, line)){
    // Find correct process type
    if (TD::lowercase_and_remove_spaces(line) == _sanitized_process_type){
      is_correct_process_type = true;
    }

    // Find reaction/process we want
    // Process line is structured like:
    // PROCESS: reaction, reaction_type
    if (is_correct_process_type && line.find("PROCESS:") != std::string::npos){

      // remove spaces and lower case entire process line
      std::string sanitized_process_line = TD::lowercase_and_remove_spaces(line);

      if (sanitized_process_line.find(_sanitized_reaction) != std::string::npos){
        is_correct_reaction = true;
      }
    }

    if (is_correct_process_type && is_correct_reaction){
      if (_sanitized_process_type == "elastic"){
        // If the process type was elastic we need to look for the block whose
        // comment line contains "Elastic scattering". There are also blocks
        // with Elastic process type but representing total scattering cross sections
        // or momentum transfer cross sections which we do not want in general for PIC
        if (line.find("COMMENT:") != std::string::npos){
          std::string sanitized_comment_line = TD::lowercase_and_remove_spaces(line);

          if (sanitized_comment_line.find("elasticscattering") != std::string::npos){
            is_correct_block = true;
          }else{
            // This is the wrong elastic scattering process
            is_correct_process_type = false;
            is_correct_reaction = false;
            is_correct_block = false;
          }
        }
      }else if (_sanitized_process_type == "ionization"){
        // If the process type is ionization we need to look for the blocks who
        // do NOT contain "total ionization" in their comments.
        // LXCat supplies an ionization cross section list where each ionization
        // cross section is grouped together.
        // Normally when "reaction" is input correctly we should never get this one
        // but better safe than sorry.
        if (line.find("COMMENT:") != std::string::npos){
          std::string sanitized_comment_line = TD::lowercase_and_remove_spaces(line);

          if (sanitized_comment_line.find("totalionization") != std::string::npos){
            is_correct_process_type = false;
            is_correct_reaction = false;
            is_correct_block = false;
          }else{
            is_correct_block = true;
          }
        }
      }else{
        // For process types: excitation, attachment, and effective
        // TODO: Check what effective cross sections are. What to do with these ?
        is_correct_block = true;
      }
    }

    if (is_correct_block){
      // We now have to look for the data section.
      // This is segmented by 2 lines of "-----" (at least 5 '-' characters)

      if (line.find("-----") != std::string::npos){
        if (!first_data_delimiter_line_found){
          first_data_delimiter_line_found = true;
          continue;
        }else{
          second_data_delimiter_line_found = true;
        }
      }

      if (first_data_delimiter_line_found){
        if (!second_data_delimiter_line_found){
          // All lines read now are data lines
          std::pair<double, double> column_data = _extract_column_data(line, "\t", 1);
          _x.push_back(column_data.first);
          _y.push_back(column_data.second);
        }else{
          // Second data delimiter line found so all data was read
          break;
        }
      }
    }
  }

  data_file.close();
}
