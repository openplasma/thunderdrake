#include "InputData.h"

#include <iostream>
#include <fstream>
#include <algorithm>

#include "GeneralUtilities.h"

InputData::InputData(std::vector<double> x, std::vector<double> y, double unit_x/*=1.0*/, double unit_y/*=1.0*/)
  : _x(x),
    _y(y),
    _n_datapoints(x.size()),
    _filename(""),
    _delimiter(""),
    _delimiter_length(0),
    _n_header_lines(0),
    _max_x(0.0),
    _min_x(0.0),
    _max_y(0.0),
    _min_y(0.0),
    _is_constant(false){

  multiply_x(unit_x);
  _find_extrema_x();

  multiply_y(unit_y);
  _find_extrema_y();

  _sort_x_y();
}

InputData::InputData(std::string filename, double unit_x/*=1.0*/, double unit_y/*=1.0*/, std::string delimiter/*=" "*/, int n_header_lines/*=0*/)
  : _filename(filename),
    _n_header_lines(n_header_lines),
    _is_constant(false){

  if (delimiter.size() > 0){
    _delimiter = delimiter;
    _delimiter_length = _delimiter.size();
  }else{
    std::cerr << "ERROR: Cannot have a 0 length delimiter for input data !" << std::endl;
    exit(-1);
  }

  // Read x and y data into the vectors
  _read_file();

  _n_datapoints = _x.size();

  multiply_x(unit_x);
  _find_extrema_x();

  multiply_y(unit_y);
  _find_extrema_y();

  _sort_x_y();
}

InputData::InputData(double y)
  : _y(std::vector<double>(1, y)),
    _n_datapoints(0),
    _filename(""),
    _delimiter(""),
    _delimiter_length(0),
    _n_header_lines(0),
    _max_x(0.0),
    _min_x(0.0),
    _max_y(y),
    _min_y(y),
    _is_constant(true){

  _find_extrema_x();
  _find_extrema_y();
}

// This constructor is only used by derived classes.
// This constructor does not call read_file but let's the derived class
// call it.
InputData::InputData(std::string filename, bool called_from_derived)
  : _filename(filename),
    _is_constant(false){
}

InputData::~InputData(){}

InputData::InputData(const InputData& copy_input_data){
  _x = copy_input_data._x;
  _y = copy_input_data._y;
  _n_datapoints = copy_input_data._n_datapoints;
  _filename = copy_input_data._filename;
  _delimiter = copy_input_data._delimiter;
  _delimiter_length = copy_input_data._delimiter_length;
  _n_header_lines = copy_input_data._n_header_lines;
  _max_x = copy_input_data._max_x;
  _min_x = copy_input_data._min_x;
  _max_y = copy_input_data._max_y;
  _min_y = copy_input_data._min_y;

  _is_constant = copy_input_data._is_constant;
}

// Modifiers
////////////////////////////////////////////////////////////////////////////////
void InputData::multiply_x(double val){

  for (int i = 0; i < _n_datapoints; ++i){
    _x[i] *= val;
  }

  // Adjust extrema
  _max_x *= val;
  _min_x *= val;
}

void InputData::divide_x(double val){

  for (int i = 0; i < _n_datapoints; ++i){
    _x[i] /= val;
  }

  // Adjust extrema
  _max_x /= val;
  _min_x /= val;
}

void InputData::add_x(double val){

  for (int i = 0; i < _n_datapoints; ++i){
    _x[i] += val;
  }

  // Adjust extrema
  _max_x += val;
  _min_x += val;
}

void InputData::subtract_x(double val){

  for (int i = 0; i < _n_datapoints; ++i){
    _x[i] -= val;
  }

  // Adjust extrema
  _max_x -= val;
  _min_x -= val;
}

void InputData::multiply_y(double val){
  if (_is_constant){
    _y[0] *= val;
  }else{
    for (int i = 0; i < _n_datapoints; ++i){
      _y[i] *= val;
    }
  }

  // Adjust extrema
  _max_y *= val;
  _min_y *= val;
}

void InputData::divide_y(double val){
  if (_is_constant){
    _y[0] /= val;
  }else{
    for (int i = 0; i < _n_datapoints; ++i){
      _y[i] /= val;
    }
  }

  // Adjust extrema
  _max_y /= val;
  _min_y /= val;
}

void InputData::add_y(double val){
  if (_is_constant){
    _y[0] += val;
  }else{
    for (int i = 0; i < _n_datapoints; ++i){
      _y[i] += val;
    }
  }

  // Adjust extrema
  _max_y += val;
  _min_y += val;
}

void InputData::subtract_y(double val){
  if (_is_constant){
    _y[0] -= val;
  }else{
    for (int i = 0; i < _n_datapoints; ++i){
      _y[i] -= val;
    }
  }

  // Adjust extrema
  _max_y -= val;
  _min_y -= val;
}

void InputData::invert_y(){
  if (_is_constant && _y[0] != 0){
    _y[0] = 1.0 / _y[0];

    _max_y = _y[0];
    _min_y = _y[0];
  }else{
    std::vector<double> x_new;
    std::vector<double> y_new;

    // We only want to add the inverted datapoints which do not give inf (no 1 / 0)
    for (int i = 0; i < _n_datapoints; ++i){
      if (_y[i] != 0){
        x_new.push_back(_x[i]);
        y_new.push_back(1.0 / _y[i]);
      }
    }

    _y = y_new;
    _x = x_new;

    // Adjust extrema
    _max_y = *max_element(_y.begin(), _y.end());
    _min_y = *min_element(_y.begin(), _y.end());
  }
}
////////////////////////////////////////////////////////////////////////////////

// Operator Overloads
////////////////////////////////////////////////////////////////////////////////
InputData InputData::operator+(const InputData& rhs){
  if (_is_constant && rhs._is_constant){
    return InputData(_y[0] + rhs._y[0]);
  }else{
    std::vector<double> x_new = TD::merge_two_sorted_vectors(_x, rhs.x());

    // Now we linearly interpolate each InputData (lhs and rhs) to each x coordinate
    // and then perform the operator (+ in this case).
    std::vector<double> y_new(x_new.size());

    for (int i = 0; i < y_new.size(); ++i){
      y_new[i] = this->at(x_new[i]) + rhs.at(x_new[i]);
    }

    return InputData(x_new, y_new);
  }
}

InputData InputData::operator-(const InputData& rhs){
  if (_is_constant && rhs._is_constant){
    return InputData(_y[0] - rhs._y[0]);
  }else{
    std::vector<double> x_new = TD::merge_two_sorted_vectors(_x, rhs.x());

    // Now we linearly interpolate each InputData (lhs and rhs) to each x coordinate
    // and then perform the operator (- in this case).
    std::vector<double> y_new(x_new.size());

    for (int i = 0; i < y_new.size(); ++i){
      y_new[i] = this->at(x_new[i]) - rhs.at(x_new[i]);
    }

    return InputData(x_new, y_new);
  }
}

InputData InputData::operator*(const InputData& rhs){
  if (_is_constant && rhs._is_constant){
    return InputData(_y[0] * rhs._y[0]);
  }else{
    std::vector<double> x_new = TD::merge_two_sorted_vectors(_x, rhs.x());

    // Now we linearly interpolate each InputData (lhs and rhs) to each x coordinate
    // and then perform the operator (* in this case).
    std::vector<double> y_new(x_new.size());

    for (int i = 0; i < y_new.size(); ++i){
      y_new[i] = this->at(x_new[i]) * rhs.at(x_new[i]);
    }

    return InputData(x_new, y_new);
  }
}

InputData InputData::operator/(const InputData& rhs){
  if (_is_constant && rhs._is_constant){
    return InputData(_y[0] / rhs._y[0]);
  }else{
    std::vector<double> x_new = TD::merge_two_sorted_vectors(_x, rhs.x());

    // Now we linearly interpolate each InputData (lhs and rhs) to each x coordinate
    // and then perform the operator (/ in this case).
    std::vector<double> y_new(x_new.size());

    for (int i = 0; i < y_new.size(); ++i){
      y_new[i] = this->at(x_new[i]) / rhs.at(x_new[i]);
    }

    return InputData(x_new, y_new);
  }
}

InputData& InputData::operator+=(const InputData& rhs){
  if (_is_constant && rhs._is_constant){
    _y[0] += rhs._y[0];
    return *this;
  }else{
    std::vector<double> x_new = TD::merge_two_sorted_vectors(_x, rhs.x());

    // Now we linearly interpolate each InputData (lhs and rhs) to each x coordinate
    // and then perform the operator (+ in this case).
    std::vector<double> y_new(x_new.size());

    for (int i = 0; i < y_new.size(); ++i){
      y_new[i] = this->at(x_new[i]) + rhs.at(x_new[i]);
    }

    _x = x_new;
    _y = y_new;

    return *this;
  }
}

InputData& InputData::operator-=(const InputData& rhs){
  if (_is_constant && rhs._is_constant){
    _y[0] -= rhs._y[0];
    return *this;
  }else{
    std::vector<double> x_new = TD::merge_two_sorted_vectors(_x, rhs.x());

    // Now we linearly interpolate each InputData (lhs and rhs) to each x coordinate
    // and then perform the operator (+ in this case).
    std::vector<double> y_new(x_new.size());

    for (int i = 0; i < y_new.size(); ++i){
      y_new[i] = this->at(x_new[i]) - rhs.at(x_new[i]);
    }

    _x = x_new;
    _y = y_new;

    return *this;
  }
}

InputData& InputData::operator*=(const InputData& rhs){
  if (_is_constant && rhs._is_constant){
    _y[0] *= rhs._y[0];
    return *this;
  }else{
    std::vector<double> x_new = TD::merge_two_sorted_vectors(_x, rhs.x());

    // Now we linearly interpolate each InputData (lhs and rhs) to each x coordinate
    // and then perform the operator (+ in this case).
    std::vector<double> y_new(x_new.size());

    for (int i = 0; i < y_new.size(); ++i){
      y_new[i] = this->at(x_new[i]) * rhs.at(x_new[i]);
    }

    _x = x_new;
    _y = y_new;

    return *this;
  }
}

InputData& InputData::operator/=(const InputData& rhs){
  if (_is_constant && rhs._is_constant){
    _y[0] /= rhs._y[0];
    return *this;
  }else{
    std::vector<double> x_new = TD::merge_two_sorted_vectors(_x, rhs.x());

    // Now we linearly interpolate each InputData (lhs and rhs) to each x coordinate
    // and then perform the operator (+ in this case).
    std::vector<double> y_new(x_new.size());

    for (int i = 0; i < y_new.size(); ++i){
      y_new[i] = this->at(x_new[i]) / rhs.at(x_new[i]);
    }

    _x = x_new;
    _y = y_new;

    return *this;
  }
}

InputData& InputData::operator=(const InputData& rhs){

  _x = rhs._x;
  _y = rhs._y;
  _n_datapoints = rhs._n_datapoints;
  _filename = rhs._filename;
  _delimiter = rhs._delimiter;
  _delimiter_length = rhs._delimiter_length;
  _n_header_lines = rhs._n_header_lines;
  _max_x = rhs._max_x;
  _min_x = rhs._min_x;
  _max_y = rhs._max_y;
  _min_y = rhs._min_y;

  _is_constant = rhs._is_constant;

  return *this;
}
////////////////////////////////////////////////////////////////////////////////


// Getters
////////////////////////////////////////////////////////////////////////////////
std::vector<double> InputData::x() const{
  return _x;
}

std::vector<double> InputData::y() const{
  return _y;
}

bool InputData::is_constant() const{
  return _is_constant;
}

int InputData::n_datapoints() const{
  return _n_datapoints;
}

int InputData::n_header_lines() const{
  return _n_header_lines;
}

std::string InputData::filename() const{
  return _filename;
}

std::string InputData::delimiter() const{
  return _delimiter;
}

int InputData::delimiter_length() const{
  return _delimiter_length;
}

double InputData::max_x() const{
  return _max_x;
}

double InputData::min_x() const{
  return _min_x;
}

double InputData::max_y() const{
  return _max_y;
}

double InputData::min_y() const{
  return _min_y;
}

double InputData::operator()(double x/*=0.0*/) const{

  if (!_is_constant && _n_datapoints == 0){
    // Default constructed InputData has no y values
    // We will return 0.0 which will work fine for + and -, but will only give
    // 0 for * and /.
    return 0.0;
  }

  if (_is_constant){
    return _y[0];
  }else{
    std::pair<int, int> bnd_idx = _find_boundary_idx(x);

    // Linear interpolation between 2 points to get y_value at given x
    double m = (_y[bnd_idx.second] - _y[bnd_idx.first]) / (_x[bnd_idx.second] - _x[bnd_idx.first]);
    return m * (x - _x[bnd_idx.first]) + _y[bnd_idx.first];
  }
}

double InputData::at(double x/*=0.0*/) const{
  return this->operator()(x);
}
////////////////////////////////////////////////////////////////////////////////

void InputData::_read_file(){

  int n_read_lines = 0;
  std::string line;

  std::ifstream data_file;
  data_file.open(_filename);

  // Read the header and do nothing with it
  for (int i = 0; i < _n_header_lines; ++i){
    std::getline(data_file, line);
  }

  // Read the data and put it in the vectors
  while(std::getline(data_file, line)){

    std::pair<double, double> column_data = _extract_column_data(line, _delimiter, _delimiter_length);
    _x.push_back(column_data.first);
    _y.push_back(column_data.second);
  }

  data_file.close();
}


void InputData::_sort_x_y(){

  std::pair<std::vector<double>, std::vector<double>> x_y_sorted = TD::sort_two_vectors(_x, _y);

  _x = x_y_sorted.first;
  _y = x_y_sorted.second;
}


std::pair<int, int> InputData::_find_boundary_idx(double x) const{
  // Find two boundaries in _x points. Assuming ordered data x_i < x_i + 1

  if (x < _x[0]){
    return std::pair<int, int>(0, 1);
  }else if (x > _x[_n_datapoints - 1]){
    return std::pair<int, int>(_n_datapoints - 2, _n_datapoints - 1);
  }else{
    for (int i = _n_datapoints - 1; i >= 1; --i){
      if (x >= _x[i - 1] && x <= _x[i]){
        return std::pair<int, int>(i - 1, i);
      }
    }
  }
}

std::pair<double, double> InputData::_extract_column_data(std::string line, std::string delimiter, int delimiter_length) const{

  // Split line based on delimiter
  int delimiter_pos = 0;
  std::string token;
  int coord = 0;

  std::pair<double, double> column_data;

  while ((delimiter_pos = line.find(delimiter)) != std::string::npos){
    token = line.substr(0, delimiter_pos);

    if (token.size() > 0){
      if (coord == 0){
        column_data.first = std::stod(token);
        coord++;
      }else{
        column_data.second = std::stod(token);
      }
    }

    line.erase(0, delimiter_pos + delimiter_length);
  }
  // Last value on input line
  if (token.size() > 0){
    if (coord == 0){
      column_data.first = std::stod(line);
      coord++;
    }else{
      column_data.second = std::stod(line);
    }
  }

  return column_data;
}

void InputData::_find_extrema_x(){

  if (_is_constant){
    _max_x = 0.0;
    _min_x = 0.0;
  }else if (_n_datapoints > 0){
    _max_x = *std::max_element(_x.begin(), _x.end());
    _min_x = *std::min_element(_x.begin(), _x.end());
  }else{
    _max_x = 0.0;
    _min_x = 0.0;
  }
}

void InputData::_find_extrema_y(){

  if (_is_constant){
    _max_y = _y[0];
    _min_y = _y[0];
  }else if (_n_datapoints > 0){
    _max_y = *std::max_element(_y.begin(), _y.end());
    _min_y = *std::min_element(_y.begin(), _y.end());
  }else{
    _max_y = 0.0;
    _min_y = 0.0;
  }
}
