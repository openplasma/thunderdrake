#include "InputDataCollection_LXCat.h"

#include <iostream>
#include <fstream>

#include "GeneralUtilities.h"


InputDataCollection_LXCat::InputDataCollection_LXCat(std::string filename, std::string process_type, std::string neutral_species, double unit_x/*=1.0*/, double unit_y/*=1.0*/)
  : _filename(filename),
    _unit_x(unit_x),
    _unit_y(unit_y){

  _sanitized_process_type = TD::lowercase_and_remove_spaces(process_type);
  _sanitized_neutral_species = TD::lowercase_and_remove_spaces(neutral_species);
  _neutral_species_length = _sanitized_neutral_species.size();

  _read_file();
}

InputDataCollection_LXCat::~InputDataCollection_LXCat(){}

// Getters
////////////////////////////////////////////////////////////////////////////////
std::string InputDataCollection_LXCat::filename() const{
  return _filename;
}

std::string InputDataCollection_LXCat::process_type() const{
  return _sanitized_process_type;
}

std::string InputDataCollection_LXCat::neutral_species() const{
  return _sanitized_neutral_species;
}

double InputDataCollection_LXCat::unit_x() const{
  return _unit_x;
}

double InputDataCollection_LXCat::unit_y() const{
  return _unit_y;
}

InputData_LXCat* InputDataCollection_LXCat::get_process_inputdata(std::string process){

  std::string sanitized_process = TD::lowercase_and_remove_spaces(process);

  auto process_inputdata_it = _processes_inputdata.find(sanitized_process);
  if (process_inputdata_it != _processes_inputdata.end()){
    return &(process_inputdata_it->second);
  }else{
    return NULL;
  }

}
////////////////////////////////////////////////////////////////////////////////


void InputDataCollection_LXCat:: _read_file(){

  // All reactions for a given process type should be extracted from the lxcat file
  // After this we can simply instantiate InputData_LXCat objects with
  // given process type and each found reaction
  std::vector<std::string> sanitized_processes;

  // Dictates if process type matches: effective, elastic, excitation, ionization, or attachment
  bool is_correct_process_type = false;
  // Check the first line after process type to see if target is the same as neutral species given
  bool is_correct_neutral_species = false;

  // We found the "-----" lines segmenting the data
  bool first_data_delimiter_line_found = false;

  std::string line;

  std::ifstream data_file;
  data_file.open(_filename);

  while (std::getline(data_file, line)){

    // Don't do all of this when we are in the data section.
    if (!first_data_delimiter_line_found){

      //std::cout << line << std::endl;

      if (TD::lowercase_and_remove_spaces(line) == _sanitized_process_type){
        is_correct_process_type = true;
      }

      if (is_correct_process_type){
        // Check if target species is the same as given
        std::string sanitized_string = TD::lowercase_and_remove_spaces(line);

        if (sanitized_string.compare(0, _neutral_species_length, _sanitized_neutral_species) == 0){
          // The first _neutral_species_length characters of the line read
          // match the _sanitized_neutral_species meaning the target matches to what is given.
          is_correct_neutral_species = true;
        }
      }

      if (is_correct_process_type && is_correct_neutral_species){
        // Get process from PROCESS line
        if (line.find("PROCESS:") != std::string::npos){
          std::string sanitized_process_line = TD::lowercase_and_remove_spaces(line);

          // Process is inbetween : and , charachters
          int first_bounding_char_pos = sanitized_process_line.find(":") + 1;
          int second_bounding_char_pos = sanitized_process_line.find_last_of(",");

          std::string sanitized_process = sanitized_process_line.substr(first_bounding_char_pos, second_bounding_char_pos - first_bounding_char_pos);

          sanitized_processes.push_back(sanitized_process);
        }
      }
    }

    // Make sure to skip the 6x "------" dashed example in the intro of the lxcat file
    // We are looking for 7 "-" minimum
    if (line.find("-------") != std::string::npos){
        // Data segmenting line found
        if (!first_data_delimiter_line_found){
          // This is the first line
          first_data_delimiter_line_found = true;
        }else{
          // Both lines are found. Reset all bools.
          is_correct_process_type = false;
          is_correct_neutral_species = false;
          first_data_delimiter_line_found = false;
        }
    }
  }

  data_file.close();


  // Input all sanitized reactions into InputData_LXCat objects
  for (std::string process: sanitized_processes){

    auto it_find = _processes_inputdata.find(process);

    if (it_find == _processes_inputdata.end()){
      // Process not yet in the input data collection
      _processes_inputdata.insert(std::pair<std::string, InputData_LXCat>(process, InputData_LXCat(_filename, _sanitized_process_type, process, _unit_x, _unit_y)));
    }else{
      // Overwrite input data
      it_find->second = std::move(InputData_LXCat(_filename, _sanitized_process_type, process, _unit_x, _unit_y));
    }
  }


}
