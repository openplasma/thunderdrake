#include "VelocityVerletMover.h"

#include <iostream>

#include "ParticleSwarm.h"

VelocityVerletMover::VelocityVerletMover(): CloneableParticleMover(){
}

VelocityVerletMover::~VelocityVerletMover(){
}

void VelocityVerletMover::move_particle(int i_part, double dt){

  const VectorField* force = _particle_swarm->force();
  double inv_mass = _particle_swarm->inv_mass();

  double& pos_x = _particle_swarm->pos_x[i_part];
  double& pos_y = _particle_swarm->pos_y[i_part];
  double& pos_z = _particle_swarm->pos_z[i_part];

  double& vel_x = _particle_swarm->vel_x[i_part];
  double& vel_y = _particle_swarm->vel_y[i_part];
  double& vel_z = _particle_swarm->vel_z[i_part];

  double& accel_x = _particle_swarm->accel_x[i_part];
  double& accel_y = _particle_swarm->accel_y[i_part];
  double& accel_z = _particle_swarm->accel_z[i_part];

  double& t_left = _particle_swarm->t_left[i_part];

  vel_x += accel_x * 0.5 * dt;
  pos_x += vel_x * dt;

  vel_y += accel_y * 0.5 * dt;
  pos_y += vel_y * dt;

  vel_z += accel_z * 0.5 * dt;
  pos_z += vel_z * dt;

  accel_x = force->x_at_pos(pos_x, pos_y, pos_z) * inv_mass;
  accel_y = force->y_at_pos(pos_x, pos_y, pos_z) * inv_mass;
  accel_z = force->z_at_pos(pos_x, pos_y, pos_z) * inv_mass;

  vel_x += accel_x * 0.5 * dt;
  vel_y += accel_y * 0.5 * dt;
  vel_z += accel_z * 0.5 * dt;

  t_left -= dt;
}

void VelocityVerletMover::move_particles(int i_part_start, int i_part_end){
  // Update velocity with 0.5 dt and the position with dt.
  _update_pos(i_part_start, i_part_end);
  // _calc_accel needs to be called after this since the positions of particles have changed.
  _update_accel(i_part_start, i_part_end);
  // After accel update we need to update velocity for another 0.5 dt so call _update_vel after.
  _update_vel(i_part_start, i_part_end);
  // After all movement we need to reduce t_left with the t_step just taken
  _update_times(i_part_start, i_part_end);
}

void VelocityVerletMover::_update_pos(int i_part_start, int i_part_end){

  std::vector<double>& pos_x = _particle_swarm->pos_x;
  std::vector<double>& pos_y = _particle_swarm->pos_y;
  std::vector<double>& pos_z = _particle_swarm->pos_z;

  std::vector<double>& vel_x = _particle_swarm->vel_x;
  std::vector<double>& vel_y = _particle_swarm->vel_y;
  std::vector<double>& vel_z = _particle_swarm->vel_z;

  std::vector<double>& accel_x = _particle_swarm->accel_x;
  std::vector<double>& accel_y = _particle_swarm->accel_y;
  std::vector<double>& accel_z = _particle_swarm->accel_z;

  std::vector<double>& t_step = _particle_swarm->t_step;

  // Update velocity with 0.5 dt and the position with dt.
  #pragma vector nodynamic_align
  #pragma omp simd
  for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
    vel_x[i_part] += accel_x[i_part] * 0.5 * t_step[i_part];
    pos_x[i_part] += vel_x[i_part] * t_step[i_part];

    vel_y[i_part] += accel_y[i_part] * 0.5 * t_step[i_part];
    pos_y[i_part] += vel_y[i_part] * t_step[i_part];

    vel_z[i_part] += accel_z[i_part] * 0.5 * t_step[i_part];
    pos_z[i_part] += vel_z[i_part] * t_step[i_part];
  }
}

void VelocityVerletMover::_update_accel(int i_part_start, int i_part_end){

  const VectorField* force = _particle_swarm->force();
  double inv_mass = _particle_swarm->inv_mass();

  std::vector<double>& pos_x = _particle_swarm->pos_x;
  std::vector<double>& pos_y = _particle_swarm->pos_y;
  std::vector<double>& pos_z = _particle_swarm->pos_z;

  std::vector<double>& accel_x = _particle_swarm->accel_x;
  std::vector<double>& accel_y = _particle_swarm->accel_y;
  std::vector<double>& accel_z = _particle_swarm->accel_z;

  #pragma vector nodynamic_align
  #pragma omp simd
  for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
    accel_x[i_part] = force->x_at_pos(pos_x[i_part], pos_y[i_part], pos_z[i_part]) * inv_mass;
    accel_y[i_part] = force->y_at_pos(pos_x[i_part], pos_y[i_part], pos_z[i_part]) * inv_mass;
    accel_z[i_part] = force->z_at_pos(pos_x[i_part], pos_y[i_part], pos_z[i_part]) * inv_mass;
  }
}

void VelocityVerletMover::_update_vel(int i_part_start, int i_part_end){

  std::vector<double>& vel_x = _particle_swarm->vel_x;
  std::vector<double>& vel_y = _particle_swarm->vel_y;
  std::vector<double>& vel_z = _particle_swarm->vel_z;

  std::vector<double>& accel_x = _particle_swarm->accel_x;
  std::vector<double>& accel_y = _particle_swarm->accel_y;
  std::vector<double>& accel_z = _particle_swarm->accel_z;

  std::vector<double>& t_step = _particle_swarm->t_step;

  #pragma vector nodynamic_align
  #pragma omp simd
  for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
    vel_x[i_part] += accel_x[i_part] * 0.5 * t_step[i_part];
    vel_y[i_part] += accel_y[i_part] * 0.5 * t_step[i_part];
    vel_z[i_part] += accel_z[i_part] * 0.5 * t_step[i_part];
  }
}

void VelocityVerletMover::_update_times(int i_part_start, int i_part_end){

  std::vector<double>& t_left = _particle_swarm->t_left;
  std::vector<double>& t_step = _particle_swarm->t_step;

  #pragma omp simd
  for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
    t_left[i_part] -= t_step[i_part];
  }
}
