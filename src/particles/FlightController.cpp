#include "FlightController.h"

#include <iostream>
#include <algorithm>
#include "omp.h"

#include "ParticleSpecies.h"
#include "ParticleSwarm.h"
#include "VectorField.h"
#include "RngType.h"
#include "RngXoroshiro128p.h"
#include "RngSplitMix64.h"

FlightController::FlightController(int n_threads, int rng_seed)
  : _rng_seed(rng_seed),
    _rng(std::make_unique<RngXoroshiro128p>(rng_seed)),
    _current_time(0.0){
  if (omp_get_max_threads() >= n_threads){
    omp_set_num_threads(n_threads);
    _n_threads = n_threads;
  }else{
    omp_set_num_threads(omp_get_max_threads());
  }
}

FlightController::~FlightController(){
}

int FlightController::n_threads() const{
  return _n_threads;
}

int FlightController::rng_seed() const{
  return _rng_seed;
}

double FlightController::current_time() const{
  return _current_time;
}

void FlightController::set_electric_field(VectorField* electric_field){
  _electric_field = electric_field;
}

void FlightController::set_rng(Rng& rng){

  _rng = std::move(rng.clone());
  _rng->set_seed(_rng_seed);

  for (ParticleSpecies* particle_species: _particle_species){
    particle_species->_set_rng(std::move(_rng->clone()));
  }
}

void FlightController::set_rng(RngType rng_type){

  switch (rng_type){
      case RngType::SplitMix64:{
        std::unique_ptr<Rng> rng = std::make_unique<RngSplitMix64>(_rng_seed);
        this->set_rng(*rng);
        break;
      }
      case RngType::Xoroshiro128p:{
        std::unique_ptr<Rng> rng = std::make_unique<RngXoroshiro128p>(_rng_seed);
        this->set_rng(*rng);
        break;
      }
  }
}

void FlightController::advance_particles(double dt){

  //TODO: Multiply with charge of e in Coulomb to get SI units
  VectorField force(*_electric_field);

  // Reset advanced state of all particle species.
  this->_set_has_advanced_particle_species(false);

  // Advance all particle species once
  for (ParticleSpecies* particle_species: _particle_species){
    particle_species->advance_species(dt, force);
  }

  // Keep advancing all species until no particles are added to species
  // after they have already been advanced (for instance an ionization collision
  // creating an ion in a species that has already advanced)
  while (this->_particle_was_added_after_advancement_of_species()){
    for (ParticleSpecies* particle_species: _particle_species){
      if (particle_species->particle_was_added_during_or_after_advancement()){
        particle_species->advance_species(dt, force);
      }
    }
  }

  _current_time += dt;
}

void FlightController::add_particle_species(ParticleSpecies* particle_species){
  _particle_species.push_back(particle_species);
  particle_species->_set_flightcontroller(this);
  particle_species->_set_rng(std::move(_rng->clone()));
}

bool FlightController::_particle_was_added_after_advancement_of_species(){

  for (ParticleSpecies* particle_species: _particle_species){
    if (particle_species->particle_was_added_during_or_after_advancement()){
      return true;
    }
  }
  return false;
}

void FlightController::_set_has_advanced_particle_species(bool has_advanced){
  for (ParticleSpecies* particle_species: _particle_species){
    particle_species->_set_has_advanced(has_advanced);
  }
}
