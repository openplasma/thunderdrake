#include "ParticleSwarm.h"

#include <iostream>
#include <cmath>

#include "ParticleSpecies.h"
#include "Collision.h"
#include "Domain.h"
#include "Boundary.h"
#include "InputData.h"
#include "VelocityVerletMover.h"
#include "ConstantPressureNullCollider.h"
#include "DefaultBoundaryCollider.h"
#include "RngXoroshiro128p.h"

/* PARTICLE SWARM LIFETIME */
////////////////////////////////////////////////////////////////////////////////

ParticleSwarm::ParticleSwarm(ParticleSpecies* particle_species_parent, Domain* domain, int n_max_particles, int thread_id/*=0*/)
  : _particle_species_parent(particle_species_parent),
    _current_domain(domain),
    _particle_mover(std::make_unique<VelocityVerletMover>()),
    _particle_collider(std::make_unique<ConstantPressureNullCollider>()),
    _boundary_collider(std::make_unique<DefaultBoundaryCollider>()),
    _dt_max(0.5),
    _n_max_particles(n_max_particles),
    _n_active_particles(0),
    _n_collisions(0),
    _thread_id(thread_id),
    _currently_advancing(false),
    _has_advanced(false),
    _particle_added_during_or_after_advancement(false),
    _first_idx_of_particle_added_during_or_after_advancement(0),
    _max_tleft_of_particles_added_during_or_after_advancement(0.0),
    _force(NULL),
    _rng(std::make_unique<RngXoroshiro128p>(5)){

  std::cout << "Creating particle swarm" << std::endl;

  _particle_mover->set_particle_swarm(this);
  _particle_collider->set_particle_swarm(this);
  _boundary_collider->set_particle_swarm(this);

  set_mass(particle_species_parent->mass());
  set_n_max_particles(n_max_particles);
}

ParticleSwarm::ParticleSwarm(const ParticleSwarm& copy_swarm, bool with_particles/*=true*/)
  : _particle_species_parent(copy_swarm._particle_species_parent),
    _current_domain(copy_swarm._current_domain),
    _n_max_particles(copy_swarm._n_max_particles),
    _n_collisions(copy_swarm._n_collisions),
    _thread_id(copy_swarm._thread_id),
    _currently_advancing(copy_swarm._currently_advancing),
    _has_advanced(copy_swarm._has_advanced),
    _first_idx_of_particle_added_during_or_after_advancement(copy_swarm._first_idx_of_particle_added_during_or_after_advancement),
    _max_tleft_of_particles_added_during_or_after_advancement(copy_swarm._max_tleft_of_particles_added_during_or_after_advancement),
    _force(copy_swarm._force){

  std::cout << "Copying other particle swarm" << std::endl;

  _particle_mover = std::move((copy_swarm._particle_mover)->clone());
  _particle_mover->set_particle_swarm(this);
  _particle_collider = std::move((copy_swarm._particle_collider)->clone());
  _particle_collider->set_particle_swarm(this);
  _boundary_collider = std::move((copy_swarm._boundary_collider)->clone());
  _boundary_collider->set_particle_swarm(this);
  _rng = std::move((copy_swarm._rng)->clone());

  set_mass(copy_swarm._mass);
  set_dt_max(copy_swarm._dt_max);

  if (with_particles){
    pos_x = copy_swarm.pos_x;
    pos_y = copy_swarm.pos_y;
    pos_z = copy_swarm.pos_z;

    vel_x = copy_swarm.vel_x;
    vel_y = copy_swarm.vel_y;
    vel_z = copy_swarm.vel_z;

    accel_x = copy_swarm.accel_x;
    accel_y = copy_swarm.accel_y;
    accel_z = copy_swarm.accel_z;

    weight = copy_swarm.weight;

    t_coll = copy_swarm.t_coll;
    t_step = copy_swarm.t_step;
    t_left = copy_swarm.t_left;

    _n_active_particles = copy_swarm._n_active_particles;
  }else{
    _n_active_particles = 0;
  }

  // Deep copy collisions
  for (int i = 0; i < copy_swarm._collisions.size(); ++i){
    _collisions.push_back(std::move((copy_swarm._collisions[i])->clone()));
  }

  // Deep copy boundary conditions
  for (auto it = copy_swarm._boundary_conditions.begin(); it != copy_swarm._boundary_conditions.end(); ++it){
    Domain* copy_swarm_domain = it->first;
    const std::map<int, std::unique_ptr<Boundary>>& copy_swarm_map = it->second;
    for (auto it_bc = copy_swarm_map.begin(); it_bc != copy_swarm_map.end(); ++it_bc){
      int boundary_idx = it_bc->first;
      _boundary_conditions[copy_swarm_domain][boundary_idx] = std::move((it_bc->second)->clone());
    }
  }
}

ParticleSwarm::~ParticleSwarm(){
  std::cout << "Particleswarm Destructor" << std::endl;
}
////////////////////////////////////////////////////////////////////////////////


/* PARTICLE SWARM OPERATORS */
////////////////////////////////////////////////////////////////////////////////
ParticleSwarm& ParticleSwarm::operator=(const ParticleSwarm& rhs){

  pos_x = rhs.pos_x;
  pos_y = rhs.pos_y;
  pos_z = rhs.pos_z;

  vel_x = rhs.vel_x;
  vel_y = rhs.vel_y;
  vel_z = rhs.vel_z;

  accel_x = rhs.accel_x;
  accel_y = rhs.accel_y;
  accel_z = rhs.accel_z;

  weight = rhs.weight;
  t_coll = rhs.t_coll;
  t_step = rhs.t_step;
  t_left = rhs.t_left;

  _mass = rhs._mass;
  _inv_mass = rhs._inv_mass;
  _dt_max = rhs._dt_max;
  _n_active_particles = rhs._n_active_particles;
  _n_max_particles = rhs._n_max_particles;
  _n_collisions = rhs._n_collisions;
  _thread_id = rhs._thread_id;
  _force = rhs._force;
  _idx_parts_to_remove = rhs._idx_parts_to_remove;

  _currently_advancing = rhs._currently_advancing;
  _has_advanced = rhs._has_advanced;
  _particle_added_during_or_after_advancement = rhs._particle_added_during_or_after_advancement;
  _first_idx_of_particle_added_during_or_after_advancement = rhs._first_idx_of_particle_added_during_or_after_advancement;
  _max_tleft_of_particles_added_during_or_after_advancement = rhs._max_tleft_of_particles_added_during_or_after_advancement;

  _particle_species_parent = rhs._particle_species_parent;
  _current_domain = rhs._current_domain;
  _particle_mover = std::move((rhs._particle_mover)->clone());
  _particle_mover->set_particle_swarm(this);
  _particle_collider = std::move((rhs._particle_collider)->clone());
  _particle_collider->set_particle_swarm(this);
  _boundary_collider = std::move((rhs._boundary_collider)->clone());
  _boundary_collider->set_particle_swarm(this);

  _rng = std::move((rhs._rng)->clone());

  _collisions.clear();
  for (int i_collision = 0; i_collision < _n_collisions; ++i_collision){
    _collisions.push_back(std::move(rhs._collisions[i_collision]->clone()));
  }

  _boundary_conditions.clear();
  for (auto it = rhs._boundary_conditions.begin(); it != rhs._boundary_conditions.end(); ++it){
    Domain* rhs_domain = it->first;
    const std::map<int, std::unique_ptr<Boundary>>& rhs_map = it->second;
    for (auto it_bc = rhs_map.begin(); it_bc != rhs_map.end(); ++it_bc){
      int boundary_idx = it_bc->first;
      _boundary_conditions[rhs_domain][boundary_idx] = std::move((it_bc->second)->clone());
    }
  }


  return *this;
}
////////////////////////////////////////////////////////////////////////////////



/// PARTICLE SWARM PROPERTIES
////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////
std::string ParticleSwarm::species_name() const{
  return _particle_species_parent->species_name();
}

double ParticleSwarm::charge() const{
  return _particle_species_parent->charge();
}

int ParticleSwarm::charge_in_e() const{
  return _particle_species_parent->charge_in_e();
}

double ParticleSwarm::mass() const{
  return _mass;
}

double ParticleSwarm::inv_mass() const{
  return _inv_mass;
}

double ParticleSwarm::dt_max() const{
  return _dt_max;
}

int ParticleSwarm::n_active_particles() const{
    return _n_active_particles;
}

int ParticleSwarm::n_max_particles() const{
  return _n_max_particles;
}

int ParticleSwarm::n_collisions() const{
  return _n_collisions;
}

int ParticleSwarm::thread_id() const{
  return _thread_id;
}

const VectorField* ParticleSwarm::force() const{
  return _force;
}

const std::set<int, std::greater<int>>& ParticleSwarm::idx_parts_to_remove() const{
  return _idx_parts_to_remove;
}

bool ParticleSwarm::currently_advancing() const{
  return _currently_advancing;
}

bool ParticleSwarm::has_advanced() const{
  return _has_advanced;
}

bool ParticleSwarm::particle_added_during_or_after_advancement() const{
  return _particle_added_during_or_after_advancement;
}

int ParticleSwarm::first_idx_of_particle_added_during_or_after_advancement() const{
  return _first_idx_of_particle_added_during_or_after_advancement;
}

double ParticleSwarm::max_tleft_of_particles_added_during_or_after_advancement() const{
  return _max_tleft_of_particles_added_during_or_after_advancement;
}

const std::vector<std::unique_ptr<Collision>>& ParticleSwarm::collisions() const{
  return _collisions;
}

const std::map<Domain*, std::map<int, std::unique_ptr<Boundary>>>& ParticleSwarm::boundary_conditions() const{
  return _boundary_conditions;
}

const std::map<int, std::unique_ptr<Boundary>>& ParticleSwarm::boundary_conditions(Domain* domain_to) const{
  return _boundary_conditions.at(domain_to);
}


const std::vector<InputData>& ParticleSwarm::collision_cross_sections() const{
  return _particle_species_parent->collision_cross_sections(_current_domain);
}

const std::vector<InputData>& ParticleSwarm::collision_frequencies() const{
  return _particle_species_parent->collision_frequencies(_current_domain);
}

const std::vector<std::string>& ParticleSwarm::collision_partners() const{
  return _particle_species_parent->collision_partners(_current_domain);
}

const InputData& ParticleSwarm::total_collision_frequency() const{
  return _particle_species_parent->total_collision_frequency(_current_domain);
}

const InputData& ParticleSwarm::inv_total_collision_frequency() const{
  return _particle_species_parent->inv_total_collision_frequency(_current_domain);
}

const ParticleSpecies* ParticleSwarm::particle_species_parent() const{
  return _particle_species_parent;
}

const Domain* ParticleSwarm::current_domain() const{
  return _current_domain;
}

const Rng* ParticleSwarm::rng() const{
  return _rng.get();
}

double ParticleSwarm::random_01(){
  return _rng->random_01();
}
////////////////////////////////////////////////////////////////////////////////
// Setters
////////////////////////////////////////////////////////////////////////////////
void ParticleSwarm::set_mass(double mass){
  if (mass > 0.0){
    _mass = mass;
    _inv_mass = 1.0 / mass;
  }else{
    std::cerr << "Mass of this particle swarm cannot be < 0.0" << std::endl;
    //TODO: Throw an error and exit the program
    exit(-1);
  }
}

void ParticleSwarm::set_dt_max(double dt_max){
  _dt_max = dt_max;
}

void ParticleSwarm::set_n_max_particles(int n_max_particles){
  // If we have less particles than our desired buffer size we can simply extend
  // the buffer.
  if (n_max_particles != _n_max_particles){
    if (n_max_particles > _n_active_particles){
      _n_max_particles = n_max_particles;

      pos_x.reserve(_n_max_particles);
      pos_y.reserve(_n_max_particles);
      pos_z.reserve(_n_max_particles);

      vel_x.reserve(_n_max_particles);
      vel_y.reserve(_n_max_particles);
      vel_z.reserve(_n_max_particles);

      accel_x.reserve(_n_max_particles);
      accel_y.reserve(_n_max_particles);
      accel_z.reserve(_n_max_particles);

      weight.reserve(_n_max_particles);

      t_coll.reserve(_n_max_particles);
      t_step.reserve(_n_max_particles);
      t_left.reserve(_n_max_particles);
    }else{
      std::cerr << "Desired buffer size is less than the amount of active particles in the swarm. Cannot simply delete particles." << std::endl;
      // TODO: Throw an error and exit the simulation.
    }
  }
}

void ParticleSwarm::set_t_left(double tleft){
  #pragma omp simd
  for (int i_part = 0; i_part < _n_active_particles; ++i_part){
    if (t_left[i_part] == 0){
      // It is possible that particles are added to the swarms due to other
      // particlespecies having a collision. These particles will have a t_left > 0
      // and the t_left should not be reset, otherwise it would move more in total than dt
      t_left[i_part] = tleft;
    }
  }
}

void ParticleSwarm::set_thread_id(int thread_id){
  if (thread_id >= 0){
    _thread_id = thread_id;

    _initialize_collisions();
    _initialize_boundary_conditions();
  }
}

void ParticleSwarm::set_force(VectorField& force){
  _force = &force;
  this->_update_accel(0, _n_active_particles - 1);
}

void ParticleSwarm::set_has_advanced(bool has_advanced){
  _has_advanced = has_advanced;
}

void ParticleSwarm::initialize_rng_with_seed(int rng_seed){
  // The rng will be initialized with a seed combined with the thread id of the swarm
  // and the hash of the species name. This way all ParticleSwarms have a distinct Rng
  // that is not correlated to other Rngs.
  _rng->set_seed(rng_seed + _thread_id, _particle_species_parent->species_name());
}

void ParticleSwarm::add_collision(std::unique_ptr<Collision> my_collision){
  _collisions.push_back(std::move(my_collision));
  _n_collisions++;
}

void ParticleSwarm::add_boundary_condition(std::unique_ptr<Boundary> my_boundary, Domain* domain_to, int boundary_idx_from){
  auto it = _boundary_conditions.find(domain_to);

  if (it == _boundary_conditions.end()){
    // Create an entry in the map.
    _boundary_conditions[domain_to] = std::map<int, std::unique_ptr<Boundary>>();
  }

  _boundary_conditions[domain_to][boundary_idx_from] = std::move(my_boundary);
}

void ParticleSwarm::set_particle_mover(std::unique_ptr<ParticleMover> particle_mover){
  _particle_mover = std::move(particle_mover);
  _particle_mover->set_particle_swarm(this);
}

void ParticleSwarm::set_particle_collider(std::unique_ptr<ParticleCollider> particle_collider){
  _particle_collider = std::move(particle_collider);
  _particle_collider->set_particle_swarm(this);
}

void ParticleSwarm::set_boundary_collider(std::unique_ptr<BoundaryCollider> boundary_collider){
  _boundary_collider = std::move(boundary_collider);
  _boundary_collider->set_particle_swarm(this);
}

void ParticleSwarm::set_rng(std::unique_ptr<Rng> rng){
  _rng = std::move(rng->clone());
}

void ParticleSwarm::_initialize_collisions(){
  for (std::unique_ptr<Collision>& collision: _collisions){
    collision->initialize(_thread_id);
  }

  _n_collisions = _collisions.size();
}

void ParticleSwarm::_initialize_boundary_conditions(){
  for (auto it = _boundary_conditions.begin(); it != _boundary_conditions.end(); ++it){
    std::map<int, std::unique_ptr<Boundary>>& bc_with_domain = it->second;

    for (auto it_bc = bc_with_domain.begin(); it_bc != bc_with_domain.end(); ++it_bc){
      std::unique_ptr<Boundary>& bc = it_bc->second;
      bc->initialize(_thread_id);
    }
  }
}
////////////////////////////////////////////////////////////////////////////////


/* SWARM MANIPULATION */
////////////////////////////////////////////////////////////////////////////////
void ParticleSwarm::add_particle(double x,  double y, double z, double vx,  double vy, double vz, double ax, double ay, double az, int w, double tleft/*=0*/){

  // Check if the vectors have the capacity to hold an extra particle
  if (_n_active_particles + 1 <= _n_max_particles){

    // If there are more initialized particles (size) than active particles
    // this means there are particles deactivated. Use those first
    if (_n_active_particles < pos_x.size()){
      pos_x[_n_active_particles] = x;
      pos_y[_n_active_particles] = y;
      pos_z[_n_active_particles] = z;

      vel_x[_n_active_particles] = vx;
      vel_y[_n_active_particles] = vy;
      vel_z[_n_active_particles] = vz;

      accel_x[_n_active_particles] = ax;
      accel_y[_n_active_particles] = ay;
      accel_z[_n_active_particles] = az;

      weight[_n_active_particles] = w;

      t_coll[_n_active_particles] = 0.0;
      t_step[_n_active_particles] = 0.0;
      t_left[_n_active_particles] = tleft;
    }else{
      pos_x.push_back(x);
      pos_y.push_back(y);
      pos_z.push_back(z);

      vel_x.push_back(vx);
      vel_y.push_back(vy);
      vel_z.push_back(vz);

      accel_x.push_back(ax);
      accel_y.push_back(ay);
      accel_z.push_back(az);

      weight.push_back(w);

      t_coll.push_back(0.0);
      t_step.push_back(0.0);
      t_left.push_back(tleft);
    }
    // We are adding a particle while we are advancing all other particles.
    // This means we will have to come back to this swarm and advance the newly
    // added particles so that also their tleft == 0
    if (tleft > 0 && (_currently_advancing || _has_advanced)){
      if (!_particle_added_during_or_after_advancement){
        _particle_added_during_or_after_advancement = true;
        _first_idx_of_particle_added_during_or_after_advancement = _n_active_particles;
        _max_tleft_of_particles_added_during_or_after_advancement = tleft;
      }else{
        if (_max_tleft_of_particles_added_during_or_after_advancement < tleft){
          _max_tleft_of_particles_added_during_or_after_advancement = tleft;
        }
      }
    }

    _n_active_particles++;
  }else{
    std::cout << "n_max: " << _n_max_particles << ", n_active: " << _n_active_particles << std::endl;
    std::cerr << "Cannot fit another particle inside current arrays." << std::endl;
    // TODO: Merge, resize or error out of the simulation.
    // Cannot fit another particle in current arrays.
  }
}

void ParticleSwarm::remove_particle(int i_part){
  // This function is called by users/collisionss/boundary conditions.
  // When this function is called the particle is immediately DEACTIVATED (weight = 0)
  // and added to a list of particles to be removed when it is safe.

  if (weight[i_part] > 0){
    // Deactivate particle
    weight[i_part] = 0;

    // Queue particle for actual removal
    _idx_parts_to_remove.insert(i_part);
  }
}

void ParticleSwarm::remove_particles(std::vector<int>& i_parts){
  // This function is called by users/collisions/boundary conditions.
  // When this function is called the particles in the list are immediately DEACTIVATED (weight = 0)
  // and added to a list of particles to be removed when it is safe.

  for (int i_part: i_parts){
    this->remove_particle(i_part);
  }
}

void ParticleSwarm::clear_particles(){

  pos_x.clear();
  pos_y.clear();
  pos_z.clear();

  vel_x.clear();
  vel_y.clear();
  vel_z.clear();

  accel_x.clear();
  accel_y.clear();
  accel_z.clear();

  weight.clear();

  t_coll.clear();
  t_step.clear();
  t_left.clear();

  _idx_parts_to_remove.clear();

  _n_active_particles = 0;
}

void ParticleSwarm::_remove_particles_queued_for_removal(){
  // This function is never called by a user/collision/boundary condition.
  // Removing a particle will invalidate indices which will mess up further
  // evaluation of the particles. We only do this when we KNOW indices are no
  // longer needed.

  // This function is called by ParticleSpecies to clean up deleted particles
  // after advancing all swarms until no particles were left to advance.
  for (int i_part: _idx_parts_to_remove){
    this->_remove_particle(i_part);
  }

  _idx_parts_to_remove.clear();
}

void ParticleSwarm::_remove_particle(int i_part){
  // This function is never called by a user/collision/boundary condition.
  // Removing a particle will invalidate indices which will mess up further
  // evaluation of the particles. We only do this when we KNOW indices are no
  // longer needed.

  // We swap the last particle of the swarm to the position of the particle we
  // want to remove. This way there are no gaps in our arrays and all active
  // particles are stored contiguously.
  pos_x[i_part] = pos_x[_n_active_particles - 1];
  pos_y[i_part] = pos_y[_n_active_particles - 1];
  pos_z[i_part] = pos_z[_n_active_particles - 1];

  vel_x[i_part] = vel_x[_n_active_particles - 1];
  vel_y[i_part] = vel_y[_n_active_particles - 1];
  vel_z[i_part] = vel_z[_n_active_particles - 1];

  weight[i_part] = weight[_n_active_particles - 1];

  t_coll[i_part] = t_coll[_n_active_particles - 1];
  t_step[i_part] = t_step[_n_active_particles - 1];
  t_left[i_part] = t_left[_n_active_particles - 1];

  // Make particle we just swapped inactive
  weight[_n_active_particles - 1] = 0;

  _n_active_particles--;
}
////////////////////////////////////////////////////////////////////////////////

/* PARTICLE MOVEMENT */
////////////////////////////////////////////////////////////////////////////////
void ParticleSwarm::advance_swarm(double dt, int i_part_start/*=-1*/, int i_part_end/*=-1*/){
  _currently_advancing = true;

  if (_n_active_particles > 0){

    int i_part_advance_start = i_part_start >= 0 ? i_part_start : 0;
    int i_part_advance_end = i_part_end >= 0 ? i_part_end : _n_active_particles - 1;

    if (i_part_advance_end < i_part_advance_start){
      std::cerr << "End index of particle swarm advancement is smaller than start index !" << std::endl;
      std::cerr << "start_idx: " << i_part_advance_start << std::endl;
      std::cerr << "end_idx: " << i_part_advance_end << std::endl;
      exit(-1);
    }

    if (_n_collisions > 0){
      this->_advance_swarm_w_collisions(dt, i_part_advance_start, i_part_advance_end);
    }else{
      this->_advance_swarm_wo_collisions(dt, i_part_advance_start, i_part_advance_end);
    }
  }

  _currently_advancing = false;
  _has_advanced = true;
}

void ParticleSwarm::_advance_swarm_w_collisions(double dt, int i_part_start, int i_part_end){

  _particle_mover->before_advance_swarm(i_part_start, i_part_end);
  _particle_collider->before_advance_swarm(i_part_start, i_part_end);
  // Calculate collision times
  _particle_collider->calc_t_collision(i_part_start, i_part_end);
  _boundary_collider->before_advance_swarm(i_part_start, i_part_end);

  double swarm_t_left = dt;
  while(swarm_t_left > 0.0){
    // We either take the maximum time step _dt_max or the remainder time to reach
    // the intended dt.
    double swarm_t_step = _dt_max > swarm_t_left ? swarm_t_left : _dt_max;

    // Calculate step to move all particles
    this->_calc_t_step_all_w_collisions(i_part_start, i_part_end, swarm_t_step);

    // Move a range of particles where their t_step[i_part] has been calculated before
    _particle_mover->move_particles(i_part_start, i_part_end);

    // reduce collision times of all particles with the step taken
    #pragma omp simd
    for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
      t_coll[i_part] -= t_step[i_part];
    }

    // Boundary check
    _boundary_collider->before_boundary_check(i_part_start, i_part_end);
    _boundary_collider->boundary_check(i_part_start, i_part_end);
    _boundary_collider->after_boundary_check(i_part_start, i_part_end);

    // Collide particles
    _particle_collider->before_collide_particles(i_part_start, i_part_end);
    _particle_collider->collide_particles(i_part_start, i_part_end);
    _particle_collider->after_collide_particles(i_part_start, i_part_end);

    // Calculate step to move all particles
    this->_calc_t_step_all_w_collisions(i_part_start, i_part_end, swarm_t_step);

    // Move a range of particles where their t_step[i_part] has been calculated before
    _particle_mover->move_particles(i_part_start, i_part_end);

    // reduce collision times of all particles with the step taken
    #pragma omp simd
    for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
      t_coll[i_part] -= t_step[i_part];
    }

    // Boundary check
    _boundary_collider->before_boundary_check(i_part_start, i_part_end);
    _boundary_collider->boundary_check(i_part_start, i_part_end);
    _boundary_collider->after_boundary_check(i_part_start, i_part_end);

    // Reduce remaining movement time with the step all particles just took
    swarm_t_left -= swarm_t_step;
  }

  _boundary_collider->after_advance_swarm(i_part_start, i_part_end);
  _particle_collider->after_advance_swarm(i_part_start, i_part_end);
  _particle_mover->after_advance_swarm(i_part_start, i_part_end);
}

void ParticleSwarm::_advance_swarm_wo_collisions(double dt, int i_part_start, int i_part_end){

  _particle_mover->before_advance_swarm(i_part_start, i_part_end);
  _boundary_collider->before_advance_swarm(i_part_start, i_part_end);

  double swarm_t_left = dt;
  while(swarm_t_left > 0.0){
    // We either take the maximum time step _dt_max or the remainder time to reach
    // the intended dt.
    double swarm_t_step = _dt_max > swarm_t_left ? swarm_t_left : _dt_max;

    // Calculate step to move all particles
    this->_calc_t_step_all_wo_collisions(i_part_start, i_part_end, swarm_t_step);

    // Move a range of particles where their t_step[i_part] has been calculated before
    _particle_mover->move_particles(i_part_start, i_part_end);

    // Boundary check
    _boundary_collider->before_boundary_check(i_part_start, i_part_end);
    _boundary_collider->boundary_check(i_part_start, i_part_end);
    _boundary_collider->after_boundary_check(i_part_start, i_part_end);

    // Calculate step to move all particles
    this->_calc_t_step_all_wo_collisions(i_part_start, i_part_end, swarm_t_step);

    // Move a range of particles where their t_step[i_part] has been calculated before
    _particle_mover->move_particles(i_part_start, i_part_end);

    // Boundary check
    _boundary_collider->before_boundary_check(i_part_start, i_part_end);
    _boundary_collider->boundary_check(i_part_start, i_part_end);
    _boundary_collider->after_boundary_check(i_part_start, i_part_end);

    // Reduce remaining movement time with the step all particles just took
    swarm_t_left -= swarm_t_step;
  }

  _boundary_collider->after_advance_swarm(i_part_start, i_part_end);
  _particle_mover->after_advance_swarm(i_part_start, i_part_end);
}

void ParticleSwarm::move_particle(int i_part, double dt){
  _particle_mover->move_particle(i_part, dt);
}

void ParticleSwarm::move_particles(int i_part_start, int i_part_end){
  _particle_mover->move_particles(i_part_start, i_part_end);
}

void ParticleSwarm::_update_accel(int i_part_start, int i_part_end){
  #pragma vector nodynamic_align
  #pragma omp simd
  for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
    accel_x[i_part] = _force->x_at_pos(pos_x[i_part], pos_y[i_part], pos_z[i_part]) * _inv_mass;
    accel_y[i_part] = _force->y_at_pos(pos_x[i_part], pos_y[i_part], pos_z[i_part]) * _inv_mass;
    accel_z[i_part] = _force->z_at_pos(pos_x[i_part], pos_y[i_part], pos_z[i_part]) * _inv_mass;
  }
}

double ParticleSwarm::_calc_t_step_w_collisions(int i_part, double& swarm_t_step){
  // The particle can maximally move swarm_t_step per timestep
  // this swarm_t_step is either dt_max or the remainder time that the entire
  // swarm should move to fulfill the required 'dt' advancement

  // Each particle also has its own internal clock 't_left' and a calculated
  // time for collision 't_coll'.

  // A particle can never move more than 't_left' in total during a swarm
  // advancement

  // If t_coll < swarm_t_step and t_coll < t_left and t_coll > 0 -> Particle moves to collision time
  // If t_left < swarm_t_step and t_left < t_coll -> Particle moves to t_left and will not move again this swarm advancement
  // If swarm_t_step < t_coll and swarm_t_step < t_left -> Particle moves swarm_t_step. Will most likely be dt_max.

  if (swarm_t_step < t_coll[i_part] && swarm_t_step < t_left[i_part]){
    return swarm_t_step;
  }

  if (t_coll[i_part] < swarm_t_step && t_coll[i_part] < t_left[i_part] && t_coll[i_part] > 0.0){
    return t_coll[i_part];
  }

  // if (t_left[i_part] < swarm_t_step && t_left[i_part] < t_coll[i_part])
  return t_left[i_part];
}

double ParticleSwarm::_calc_t_step_wo_collisions(int i_part, double& swarm_t_step){
  // The particle can maximally move swarm_t_step per timestep
  // this swarm_t_step is either dt_max or the remainder time that the entire
  // swarm should move to fulfill the required 'dt' advancement

  // Each particle also has its own internal clock 't_left' and a calculated
  // time for collision 't_coll'.

  // A particle can never move more than 't_left' in total during a swarm
  // advancement

  // If t_left < swarm_t_step -> Particle moves to t_left and will not move again this swarm advancement
  // If swarm_t_step < t_left -> Particle moves swarm_t_step. Will most likely be dt_max.

  if (swarm_t_step < t_left[i_part]){
    return swarm_t_step;
  }else{
    return t_left[i_part];
  }
}

void ParticleSwarm::_calc_t_step_all_w_collisions(int i_part_start, int i_part_end, double swarm_t_step){
  #pragma omp simd
  for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
    t_step[i_part] = _calc_t_step_w_collisions(i_part, swarm_t_step);
  }
}

void ParticleSwarm::_calc_t_step_all_wo_collisions(int i_part_start, int i_part_end, double swarm_t_step){
  #pragma omp simd
  for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
    t_step[i_part] = _calc_t_step_wo_collisions(i_part, swarm_t_step);
  }
}
////////////////////////////////////////////////////////////////////////////////
