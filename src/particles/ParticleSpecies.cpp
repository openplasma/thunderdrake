#include "ParticleSpecies.h"

#include <iostream>
#include <memory>
#include <cmath>
#include <chrono>

#include "FlightController.h"
#include "ParticleSwarm.h"

#include "Collision.h"
// Include pre-defined collisions
#include "Attachment_EB_Bm.h"
#include "Detachment_AmB_E.h"
#include "Elastic_AB_AB.h"
#include "Excitation_AB_ABs.h"
#include "Ionization_AB_ABpE.h"
#include "Ionization_EB_2EBp.h"
#include "ParticleCollider.h"
#include "ParticleColliderType.h"
#include "ConstantPressureNullCollider.h"
#include "VariablePressureNullCollider.h"

#include "ParticleMover.h"
#include "ParticleMoverType.h"
#include "VelocityVerletMover.h"

#include "BoundaryCollider.h"
#include "BoundaryColliderType.h"
#include "DefaultBoundaryCollider.h"
#include "PeriodicBoundary_Box.h"
#include "TransmissionBoundary.h"

#include "BoxDomain.h"
#include "InputData.h"
#include "GeneralUtilities.h"
#include "PhysicalConstants.h"
#include "RngXoroshiro128p.h"

ParticleSpecies::ParticleSpecies(std::string species_name, double mass, int charge_in_e)
  : _flight_controller(NULL),
    _rng(std::make_unique<RngXoroshiro128p>(5)){

  std::cout << "Creating particle species" << std::endl;

  set_species_name(species_name);
  set_mass(mass);
  set_charge_in_e(charge_in_e);
}

ParticleSpecies::ParticleSpecies(const ParticleSpecies& copy_particlespecies){

  // Since ParticleSpecies contain a map with a vector of unique_ptr to ParticleSwarm
  // we need to copy this map in a specific way since unique_ptr can not be copied.
  const std::map<Domain*, std::vector<std::unique_ptr<ParticleSwarm>>>& to_copy_particle_swarms = copy_particlespecies._particle_swarms;
  for (auto it = to_copy_particle_swarms.begin(); it != to_copy_particle_swarms.end(); ++it){
    std::vector<std::unique_ptr<ParticleSwarm>> copied_particle_swarms;
    copied_particle_swarms.reserve((it->second).size());

    for (auto& to_copy_particle_swarm: it->second){
      copied_particle_swarms.push_back(std::move(std::make_unique<ParticleSwarm>(*(to_copy_particle_swarm.get()))));
    }

    _particle_swarms[it->first] = std::move(copied_particle_swarms);
  }

  _collision_cross_sections = copy_particlespecies._collision_cross_sections;
  _collision_frequencies = copy_particlespecies._collision_frequencies;
  _collision_partners = copy_particlespecies._collision_partners;
  _total_collision_frequency = copy_particlespecies._total_collision_frequency;
  _inv_total_collision_frequency = copy_particlespecies._inv_total_collision_frequency;
  _n_collisions = copy_particlespecies._n_collisions;

  _flight_controller = copy_particlespecies._flight_controller;

  _species_name = copy_particlespecies._species_name;
  _mass = copy_particlespecies._mass;
  _charge_in_e = copy_particlespecies._charge_in_e;
  _charge = copy_particlespecies._charge;

  _rng = std::move((copy_particlespecies._rng)->clone());

  std::cout << "Copying Particle Species" << std::endl;
}

ParticleSpecies::~ParticleSpecies(){
  std::cout << "Particlespecies Destructor" << std::endl;
}


/// PARTICLE SPECIES PROPERTIES
////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////
double ParticleSpecies::mass() const{
  return _mass;
}

double ParticleSpecies::inv_mass() const{
  return _inv_mass;
}

int ParticleSpecies::charge_in_e() const{
  return _charge_in_e;
}

double ParticleSpecies::charge() const{
  return _charge;
}

std::string ParticleSpecies::species_name() const{
  return _species_name;
}

int ParticleSpecies::n_active_particles(Domain* domain) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    int n_active_particles = 0;

    for (auto& particle_swarm: it->second){
      n_active_particles += particle_swarm->n_active_particles();
    }

    return n_active_particles;
  }
}

bool ParticleSpecies::is_in_domain(Domain& domain) const{
  auto it = _particle_swarms.find(&domain);
  if (it != _particle_swarms.end()){
    return true;
  }else{
    return false;
  }
}

bool ParticleSpecies::swarms_have_advanced(Domain* domain) const{

  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    // If 1 swarm has advanced then all swarms have advanced.
    const std::unique_ptr<ParticleSwarm>& particle_swarm = it->second[0];
    return particle_swarm->has_advanced();
  }
}

ParticleSwarm* ParticleSpecies::particle_swarm(Domain& domain, int thread_id/*=0*/) const{
  auto it = _particle_swarms.find(&domain);

  if (it != _particle_swarms.end()){
    ParticleSwarm* particle_swarm_of_thread = (it->second)[thread_id].get();
    return particle_swarm_of_thread;
  }else{
    std::cout << "Swarm not found" << std::endl;
    return NULL;
  }
}

const std::vector<InputData>& ParticleSpecies::collision_cross_sections(Domain* domain) const{
  auto it = _collision_cross_sections.find(domain);

  if (it != _collision_cross_sections.end()){
    return it->second;
  }
}

const std::vector<InputData>& ParticleSpecies::collision_frequencies(Domain* domain) const{
  auto it = _collision_frequencies.find(domain);

  if (it != _collision_frequencies.end()){
    return it->second;
  }
}

const std::vector<std::string>& ParticleSpecies::collision_partners(Domain* domain) const{
  auto it = _collision_partners.find(domain);

  if (it != _collision_partners.end()){
    return it->second;
  }
}

const InputData& ParticleSpecies::total_collision_frequency(Domain* domain) const{
  auto it = _total_collision_frequency.find(domain);

  if (it != _total_collision_frequency.end()){
    return it->second;
  }
}

const InputData& ParticleSpecies::inv_total_collision_frequency(Domain* domain) const{
  auto it = _inv_total_collision_frequency.find(domain);

  if (it != _inv_total_collision_frequency.end()){
    return it->second;
  }
}

int ParticleSpecies::n_collisions(Domain* domain) const{
  auto it = _n_collisions.find(domain);

  if (it != _n_collisions.end()){
    return it->second;
  }
}

bool ParticleSpecies::particle_was_added_during_or_after_advancement() const{
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: it->second){
      if (particle_swarm->particle_added_during_or_after_advancement()){
        return true;
      }
    }
  }
  return false;
}

std::vector<double> ParticleSpecies::pos_x(Domain* domain) const{

  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> pos_x_concat;
    pos_x_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      pos_x_concat.insert(pos_x_concat.begin() + insert_offset, particle_swarm->pos_x.begin(), particle_swarm->pos_x.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return pos_x_concat;
  }
}

std::vector<double> ParticleSpecies::pos_y(Domain* domain) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> pos_y_concat;
    pos_y_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      pos_y_concat.insert(pos_y_concat.begin() + insert_offset, particle_swarm->pos_y.begin(), particle_swarm->pos_y.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return pos_y_concat;
  }
}

std::vector<double> ParticleSpecies::pos_z(Domain* domain) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> pos_z_concat;
    pos_z_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      pos_z_concat.insert(pos_z_concat.begin() + insert_offset, particle_swarm->pos_z.begin(), particle_swarm->pos_z.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return pos_z_concat;
  }
}

std::vector<double> ParticleSpecies::vel_x(Domain* domain) const{

  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> vel_x_concat;
    vel_x_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      vel_x_concat.insert(vel_x_concat.begin() + insert_offset, particle_swarm->vel_x.begin(), particle_swarm->vel_x.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return vel_x_concat;
  }
}

std::vector<double> ParticleSpecies::vel_y(Domain* domain) const{

  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> vel_y_concat;
    vel_y_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      vel_y_concat.insert(vel_y_concat.begin() + insert_offset, particle_swarm->vel_y.begin(), particle_swarm->vel_y.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return vel_y_concat;
  }
}

std::vector<double> ParticleSpecies::vel_z(Domain* domain) const{

  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> vel_z_concat;
    vel_z_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      vel_z_concat.insert(vel_z_concat.begin() + insert_offset, particle_swarm->vel_z.begin(), particle_swarm->vel_z.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return vel_z_concat;
  }
}

std::vector<double> ParticleSpecies::accel_x(Domain* domain) const{

  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> accel_x_concat;
    accel_x_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      accel_x_concat.insert(accel_x_concat.begin() + insert_offset, particle_swarm->accel_x.begin(), particle_swarm->accel_x.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return accel_x_concat;
  }
}

std::vector<double> ParticleSpecies::accel_y(Domain* domain) const{

  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> accel_y_concat;
    accel_y_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      accel_y_concat.insert(accel_y_concat.begin() + insert_offset, particle_swarm->accel_y.begin(), particle_swarm->accel_y.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return accel_y_concat;
  }
}

std::vector<double> ParticleSpecies::accel_z(Domain* domain) const{

  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> accel_z_concat;
    accel_z_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      accel_z_concat.insert(accel_z_concat.begin() + insert_offset, particle_swarm->accel_z.begin(), particle_swarm->accel_z.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return accel_z_concat;
  }
}

std::vector<int> ParticleSpecies::weight(Domain* domain) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<int> weight_concat;
    weight_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      weight_concat.insert(weight_concat.begin() + insert_offset, particle_swarm->weight.begin(), particle_swarm->weight.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return weight_concat;
  }
}

std::vector<double> ParticleSpecies::t_coll(Domain* domain) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> t_coll_concat;
    t_coll_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      t_coll_concat.insert(t_coll_concat.begin() + insert_offset, particle_swarm->t_coll.begin(), particle_swarm->t_coll.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return t_coll_concat;
  }
}

std::vector<double> ParticleSpecies::t_step(Domain* domain) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> t_step_concat;
    t_step_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      t_step_concat.insert(t_step_concat.begin() + insert_offset, particle_swarm->t_step.begin(), particle_swarm->t_step.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return t_step_concat;
  }
}

std::vector<double> ParticleSpecies::t_left(Domain* domain) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){

    int n_active_particles = this->n_active_particles(domain);
    std::vector<double> t_left_concat;
    t_left_concat.reserve(n_active_particles);

    int insert_offset = 0;
    for (auto& particle_swarm: it->second){
      t_left_concat.insert(t_left_concat.begin() + insert_offset, particle_swarm->t_left.begin(), particle_swarm->t_left.end());
      insert_offset += particle_swarm->n_active_particles();
    }

    return t_left_concat;
  }
}


void ParticleSpecies::print_pos_x(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& pos_x: particle_swarm->pos_x){
        output << pos_x << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_pos_y(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& pos_y: particle_swarm->pos_y){
        output << pos_y << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_pos_z(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& pos_z: particle_swarm->pos_z){
        output << pos_z << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_vel_x(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& vel_x: particle_swarm->vel_x){
        output << vel_x << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_vel_y(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& vel_y: particle_swarm->vel_y){
        output << vel_y << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_vel_z(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& vel_z: particle_swarm->vel_z){
        output << vel_z << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_accel_x(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& accel_x: particle_swarm->accel_x){
        output << accel_x << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_accel_y(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& accel_y: particle_swarm->accel_y){
        output << accel_y << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_accel_z(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& accel_z: particle_swarm->accel_z){
        output << accel_z << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_weight(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (int& weight: particle_swarm->weight){
        output << weight << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_t_coll(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& t_coll: particle_swarm->t_coll){
        output << t_coll << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_t_step(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& t_step: particle_swarm->t_step){
        output << t_step << "\t";
      }
    }
    output << "\n";
  }
}

void ParticleSpecies::print_t_left(Domain* domain, std::ostream& output/*=std::cout*/) const{
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    const std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    for (const std::unique_ptr<ParticleSwarm>& particle_swarm: particle_swarms){
      for (double& t_left: particle_swarm->t_left){
        output << t_left << "\t";
      }
    }
    output << "\n";
  }
}
////////////////////////////////////////////////////////////////////////////////
// Setters
////////////////////////////////////////////////////////////////////////////////
void ParticleSpecies::set_species_name(std::string species_name){
  _species_name = species_name;
}

void ParticleSpecies::set_mass(double mass){
  _mass = mass;
  _inv_mass = 1.0 / mass;
}

void ParticleSpecies::set_charge_in_e(int charge_in_e){
  _charge_in_e = charge_in_e;
  _charge = charge_in_e * TD::e;
}

void ParticleSpecies::add_domain(Domain* domain, int n_max_particles/*=2e6*/){

  auto it = _particle_swarms.find(domain);

  int n_threads = 1;
  if (_flight_controller){
    n_threads = _flight_controller->n_threads();
  }

  std::vector<std::unique_ptr<ParticleSwarm>> particle_swarms;
  particle_swarms.reserve(n_threads);
  for (int i_thread = 0; i_thread < n_threads; ++i_thread){
    particle_swarms.push_back(std::move(std::make_unique<ParticleSwarm>(this, domain, n_max_particles, i_thread)));
  }

  if (it == _particle_swarms.end()){
    // Domain not yet added
    _particle_swarms.insert(std::pair<Domain*, std::vector<std::unique_ptr<ParticleSwarm>>>(domain, std::move(particle_swarms)));
  }else{
    // Overwrite existing entry
    it->second = std::move(particle_swarms);
  }
}

void ParticleSpecies::add_domain_recursive(Domain* domain, int n_max_particles/*=2e6*/){

  this->add_domain(domain, n_max_particles);

  for (Domain* contained_domain: domain->contained_domains()){
    this->add_domain(contained_domain, n_max_particles);
  }
}

void ParticleSpecies::set_dt_max(Domain* domain, double dt_max){
  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    for (auto& particle_swarm: it->second){
      particle_swarm->set_dt_max(dt_max);
    };
  }
}

void ParticleSpecies::set_particle_mover(ParticleMover& particle_mover, Domain& domain){

  auto it = _particle_swarms.find(&domain);

  if (it != _particle_swarms.end()){
    for (std::unique_ptr<ParticleSwarm>& particle_swarm: it->second){
      particle_swarm->set_particle_mover(std::move(particle_mover.clone()));
    }
  }
}

void ParticleSpecies::set_particle_mover(ParticleMover& particle_mover){
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    this->set_particle_mover(particle_mover, *(it->first));
  }
}

void ParticleSpecies::set_particle_mover(ParticleMoverType particle_mover_type, Domain& domain){

  switch (particle_mover_type) {
    case ParticleMoverType::VelocityVerletMover:{
      std::unique_ptr<ParticleMover> particle_mover = std::make_unique<VelocityVerletMover>();
      this->set_particle_mover(*particle_mover, domain);
      break;
    }
  }
}

void ParticleSpecies::set_particle_mover(ParticleMoverType particle_mover_type){
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    this->set_particle_mover(particle_mover_type, *(it->first));
  }
}

void ParticleSpecies::set_particle_collider(ParticleCollider& particle_collider, Domain& domain){

  auto it = _particle_swarms.find(&domain);

  if (it != _particle_swarms.end()){
    for (std::unique_ptr<ParticleSwarm>& particle_swarm: it->second){
      particle_swarm->set_particle_collider(std::move(particle_collider.clone()));
    }
  }
}

void ParticleSpecies::set_particle_collider(ParticleCollider& particle_collider){
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    this->set_particle_collider(particle_collider, *(it->first));
  }
}

void ParticleSpecies::set_particle_collider(ParticleColliderType particle_collider_type, Domain& domain){

  switch (particle_collider_type) {
    case ParticleColliderType::ConstantPressureNullCollider:{
      std::unique_ptr<ParticleCollider> particle_collider = std::make_unique<ConstantPressureNullCollider>();
      this->set_particle_collider(*particle_collider, domain);
      break;
    }
    case ParticleColliderType::VariablePressureNullCollider:{
      std::unique_ptr<ParticleCollider> particle_collider = std::make_unique<VariablePressureNullCollider>();
      this->set_particle_collider(*particle_collider, domain);
      break;
    }
  }
}

void ParticleSpecies::set_particle_collider(ParticleColliderType particle_collider_type){
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    this->set_particle_collider(particle_collider_type, *(it->first));
  }
}

void ParticleSpecies::set_boundary_collider(BoundaryCollider& boundary_collider, Domain& domain){
  auto it = _particle_swarms.find(&domain);

  if (it != _particle_swarms.end()){
    for (std::unique_ptr<ParticleSwarm>& particle_swarm: it->second){
      particle_swarm->set_boundary_collider(std::move(boundary_collider.clone()));
    }
  }
}

void ParticleSpecies::set_boundary_collider(BoundaryCollider& boundary_collider){
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    this->set_boundary_collider(boundary_collider, *(it->first));
  }
}

void ParticleSpecies::set_boundary_collider(BoundaryColliderType boundary_collider_type, Domain& domain){

  switch (boundary_collider_type){
      case BoundaryColliderType::DefaultBoundaryCollider:{
        std::unique_ptr<BoundaryCollider> boundary_collider = std::make_unique<DefaultBoundaryCollider>();
        this->set_boundary_collider(*boundary_collider, domain);
        break;
      }
  }
}

void ParticleSpecies::set_boundary_collider(BoundaryColliderType boundary_collider_type){
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    this->set_boundary_collider(boundary_collider_type, *(it->first));
  }
}

void ParticleSpecies::_set_has_advanced(bool has_advanced){
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    for (std::unique_ptr<ParticleSwarm>& particle_swarm: it->second){
      particle_swarm->set_has_advanced(has_advanced);
    }
  }
}

void ParticleSpecies::_set_rng(std::unique_ptr<Rng> rng){
  this->_rng = std::move(rng->clone());

  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    for (std::unique_ptr<ParticleSwarm>& particle_swarm: it->second){
      particle_swarm->set_rng(std::move(_rng->clone()));
    }
  }

  if (_flight_controller != NULL){
    this->_initialize_rng_with_seed(_flight_controller->rng_seed());
  }
}

void ParticleSpecies::_initialize_rng_with_seed(int rng_seed){
  // Set the rng seed of the particle species
  // Because we want each particle species to have a distinct Rng we will add the
  // hash of the species name to the rng seed.
  // We also do this for all particle swarms. This would mean that the ParticleSpecies
  // and all ParticleSwarms would have the same Rng (because they have the same species name).
  // To counter this we add a number to the rng_seed.
  // For ParticleSpecies = -1
  // For ParticleSwarms = thread_id
  (this->_rng)->set_seed(rng_seed - 1, this->_species_name);

  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    for (std::unique_ptr<ParticleSwarm>& particle_swarm: it->second){
      particle_swarm->initialize_rng_with_seed(rng_seed);
    }
  }
}

void ParticleSpecies::_set_flightcontroller(FlightController* flight_controller){
  std::cout << "Set flightcontroller" << std::endl;
  if (!_flight_controller){
    _flight_controller = flight_controller;

    // We now have to check if the number of threads changed due to the flight controller.
    // This means we have to duplicate the particle swarms per domain to the amount of threads.

    if (_particle_swarms.size() > 0){
      auto it = _particle_swarms.begin();

      int n_threads_before = (_particle_swarms.begin()->second).size();
      int n_threads_after = _flight_controller->n_threads();
      int diff_n_threads = n_threads_after - n_threads_before;

      if (diff_n_threads > 0){
        for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){

          std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
          particle_swarms.reserve(particle_swarms.size() + diff_n_threads);

          for (int i_thread_extra = 0; i_thread_extra < diff_n_threads; ++i_thread_extra){
            // We will add copies of the particle swarm already present
            // but we will not copy the particles with it.
            // Later we will load balance the particle swarms.
            particle_swarms.push_back(std::move(std::make_unique<ParticleSwarm>(*(particle_swarms[0].get()), false)));
            // Set the correct thread id. Needed for all collisions and boundary conditions
            particle_swarms[n_threads_before + i_thread_extra]->set_thread_id(n_threads_before + i_thread_extra);
          }
        }
        this->_load_balance_particle_swarms();
      }
    }
  }else{
    std::cerr << "You cannot add a ParticleSpecies to multiple FlightControllers ! " << std::endl;
    std::cerr << "Only ONE FlightController per program should be used" << std::endl;
    exit(-1);
  }
}
////////////////////////////////////////////////////////////////////////////////


// PARTICLE SPECIES MOVEMENT
////////////////////////////////////////////////////////////////////////////////
void ParticleSpecies::advance_species(double dt, VectorField& force){
  // Advance the particle swarms once
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms_in_domain = it->second;

    // We only want to advance the swarms that have not yet been advanced once this dt.
    // When the flightcontroller detects that particles were added to a species after
    // the species has advanced it should not advance all the swarms again, but
    // only the swarms where the particles were added AND only that section of the swarm
    // that was added. This extra advancement is done in the while loop below.
    if (!swarms_have_advanced(it->first)){
      #pragma omp parallel for
      for (int i = 0; i < particle_swarms_in_domain.size(); ++i){
        //std::cout << "Swarm i: " << i << ", n_parts: " << particle_swarms_in_domain[i]->n_active_particles() << std::endl;
        particle_swarms_in_domain[i]->set_t_left(dt);
        particle_swarms_in_domain[i]->set_force(force);
        particle_swarms_in_domain[i]->advance_swarm(dt);
      }
    }
  }

  // Continue to advance the particle swarms until no particles were added during advancement
  while (this->particle_was_added_during_or_after_advancement()){
    for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){

      // We only advance the particles in a domain when that domain had a particle
      // added during or after advancement of the particles in that domain.
      bool particle_was_added_to_domain_swarms = false;
      for (std::unique_ptr<ParticleSwarm>& particle_swarm: it->second){
        if (particle_swarm->_particle_added_during_or_after_advancement){
          particle_was_added_to_domain_swarms = true;
          break;
        }
      }

      if (particle_was_added_to_domain_swarms){
        std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms_in_domain = it->second;

        #pragma omp parallel for
        for (int i = 0; i < particle_swarms_in_domain.size(); ++i){
          std::unique_ptr<ParticleSwarm>& particle_swarm = particle_swarms_in_domain[i];

          if (particle_swarm->particle_added_during_or_after_advancement()){
            // Reset particle added during advancement flag
            particle_swarm->_particle_added_during_or_after_advancement = false;

            int first_idx_particle_added = particle_swarm->first_idx_of_particle_added_during_or_after_advancement();
            double tleft_advance = particle_swarm->max_tleft_of_particles_added_during_or_after_advancement();

            particle_swarm->advance_swarm(tleft_advance, first_idx_particle_added);
          }
        }
      }
    }
  }

  // Clear all particles queued for removal
  this->_remove_queued_particles_from_swarms();
}
////////////////////////////////////////////////////////////////////////////////


// PARTICLE SPECIES COLLISIONS
////////////////////////////////////////////////////////////////////////////////
void ParticleSpecies::add_collision(Collision& collision, InputData& cross_section, std::string collision_partner){

  Domain* domain = collision.domain();

  auto it = _particle_swarms.find(domain);

  if (it != _particle_swarms.end()){
    for (int thread_id = 0; thread_id < (it->second).size(); ++thread_id){
      std::unique_ptr<ParticleSwarm>& particle_swarm_of_thread = (it->second)[thread_id];

      std::unique_ptr<Collision> my_collision = std::move(collision.clone());
      my_collision->initialize(thread_id);
      particle_swarm_of_thread->add_collision(std::move(my_collision));
    }
    _n_collisions[domain]++;
    _set_collision_input(domain, cross_section, collision_partner);
  }else{
    std::cout << "Collision not added because ParticleSwarm of Domain was not found" << std::endl;
  }
}

void ParticleSpecies::add_collision(Collision& collision, double cross_section, std::string collision_partner){

  InputData my_cross_section(cross_section);
  this->add_collision(collision, my_cross_section, collision_partner);
}

void ParticleSpecies::add_ionization_collision_EB_2EBp(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& positive_species_Bp, double ionization_energy){

  Ionization_EB_2EBp my_ionization("e + B -> 2e + B+", *this, collision_partner_B, domain, positive_species_Bp, ionization_energy);
  this->add_collision(my_ionization, cross_section, collision_partner_B);
}

void ParticleSpecies::add_ionization_collision_EB_2EBp(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& positive_species_Bp, double ionization_energy){

  InputData my_cross_section(cross_section);
  this->add_ionization_collision_EB_2EBp(domain, my_cross_section, collision_partner_B, positive_species_Bp, ionization_energy);
}

void ParticleSpecies::add_ionization_collision_AB_ABpE(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& positive_species_Bp, ParticleSpecies& electron_species_E, double ionization_energy){

  Ionization_AB_ABpE my_ionization("A + B -> A + B+ + e", *this, collision_partner_B, domain, positive_species_Bp, electron_species_E, ionization_energy);
  this->add_collision(my_ionization, cross_section, collision_partner_B);
}

void ParticleSpecies::add_ionization_collision_AB_ABpE(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& positive_species_Bp, ParticleSpecies& electron_species_E, double ionization_energy){

  InputData my_cross_section(cross_section);
  this->add_ionization_collision_AB_ABpE(domain, my_cross_section, collision_partner_B, positive_species_Bp, electron_species_E, ionization_energy);
}

void ParticleSpecies::add_elastic_collision_AB_AB(Domain& domain, InputData& cross_section, std::string collision_partner_B){

  Elastic_AB_AB my_elastic_collision("A + B -> A + B", *this, collision_partner_B, domain);
  this->add_collision(my_elastic_collision, cross_section, collision_partner_B);
}

void ParticleSpecies::add_elastic_collision_AB_AB(Domain& domain, double cross_section, std::string collision_partner_B){

  InputData my_cross_section(cross_section);
  this->add_elastic_collision_AB_AB(domain, my_cross_section, collision_partner_B);
}

void ParticleSpecies::add_attachment_collision_EB_Bm(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& negative_species_Bm){

  Attachment_EB_Bm my_attachment("e + B -> B-", *this, collision_partner_B, domain, negative_species_Bm);
  this->add_collision(my_attachment, cross_section, collision_partner_B);
}

void ParticleSpecies::add_attachment_collision_EB_Bm(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& negative_species_Bm){

  InputData my_cross_section(cross_section);
  this->add_attachment_collision_EB_Bm(domain, my_cross_section, collision_partner_B, negative_species_Bm);
}

void ParticleSpecies::add_excitation_collision_AB_ABs(Domain& domain, InputData& cross_section, std::string collision_partner_B, double excitation_energy){

  Excitation_AB_ABs my_excitation("A + B -> A + B*", *this, collision_partner_B, domain, excitation_energy);
  this->add_collision(my_excitation, cross_section, collision_partner_B);
}

void ParticleSpecies::add_excitation_collision_AB_ABs(Domain& domain, double cross_section, std::string collision_partner_B, double excitation_energy){

  InputData my_cross_section(cross_section);
  this->add_excitation_collision_AB_ABs(domain, my_cross_section, collision_partner_B, excitation_energy);
}

void ParticleSpecies::add_excitation_collision_AB_ABs(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& excited_species_Bs, double excitation_energy){

  Excitation_AB_ABs my_excitation("A + B -> A + B*", *this, collision_partner_B, domain, excited_species_Bs, excitation_energy);
  this->add_collision(my_excitation, cross_section, collision_partner_B);
}

void ParticleSpecies::add_excitation_collision_AB_ABs(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& excited_species_Bs, double excitation_energy){

  InputData my_cross_section(cross_section);
  this->add_excitation_collision_AB_ABs(domain, my_cross_section, collision_partner_B, excited_species_Bs, excitation_energy);
}

void ParticleSpecies::add_detachment_collision_AmB_E(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& electron_species_E){

  Detachment_AmB_E my_detachment("A- + B -> e", *this, collision_partner_B, domain, electron_species_E);
  this->add_collision(my_detachment, cross_section, collision_partner_B);
}

void ParticleSpecies::add_detachment_collision_AmB_E(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& electron_species_E){

  InputData my_cross_section(cross_section);
  this->add_detachment_collision_AmB_E(domain, my_cross_section, collision_partner_B, electron_species_E);
}

void ParticleSpecies::_set_collision_input(Domain* domain, InputData& cross_section, std::string collision_partner){
  _collision_partners[domain].push_back(TD::lowercase_and_remove_spaces(collision_partner));

  InputData cross_section_in_v2(cross_section);
  // Convert cross section x_unit from energy to velocity^2
  // E = 0.5 * m * |v|^2
  // |v|^2 = 2 * E / m
  cross_section_in_v2.multiply_x(2.0 / _mass);
  _collision_cross_sections[domain].push_back(cross_section_in_v2);

  // We only work with collision frequencies, not cross sections
  // Get maximum number density of collision partner in current domain
  double max_number_density = (domain->number_density(collision_partner)).max();

  // Calculate collision frequency using max number density
  std::vector<double> v2 = cross_section_in_v2.x();

  std::vector<double> collision_frequency;
  for (int i = 0; i < v2.size(); ++i){
    collision_frequency.push_back(sqrt(v2[i]));
    collision_frequency[i] *= cross_section_in_v2(v2[i]);
    collision_frequency[i] *= max_number_density;
  }

  InputData collision_frequency_in_v2(v2, collision_frequency);

  // Store collision frequency
  _collision_frequencies[domain].push_back(collision_frequency_in_v2);

  // Add collision frequency to the total
  auto it_find = _total_collision_frequency.find(domain);
  if (it_find == _total_collision_frequency.end()){
    // Total collision frequency entry not yet added for this domain
    _total_collision_frequency.insert(std::pair<Domain*, InputData>(domain, collision_frequency_in_v2));
  }else{
    _total_collision_frequency.at(domain) += collision_frequency_in_v2;
  }

  // Set the inverse of total collision frequency
  auto it_find2 = _inv_total_collision_frequency.find(domain);
  if (it_find2 == _inv_total_collision_frequency.end()){
    _inv_total_collision_frequency.insert(std::pair<Domain*, InputData>(domain, _total_collision_frequency.at(domain)));
  }else{
    it_find2->second = _total_collision_frequency.at(domain);
  }

  _inv_total_collision_frequency.at(domain).invert_y();
}
////////////////////////////////////////////////////////////////////////////////


// BOUNDARY CONDITIONS
////////////////////////////////////////////////////////////////////////////////
void ParticleSpecies::add_boundary_condition(Boundary& boundary_condition){

  Domain* domain_from = boundary_condition.domain_from();
  Domain* domain_to = boundary_condition.domain_to();
  int boundary_idx_from = boundary_condition.boundary_idx();

  auto it_from = _particle_swarms.find(domain_from);
  auto it_to = _particle_swarms.find(domain_to);

  if (it_from != _particle_swarms.end()){
    for (int thread_id = 0; thread_id < (it_from->second).size(); ++thread_id){
      std::unique_ptr<ParticleSwarm>& particle_swarm_of_thread = (it_from->second)[thread_id];

      std::unique_ptr<Boundary> my_boundary = std::move(boundary_condition.clone());
      my_boundary->initialize(thread_id);
      particle_swarm_of_thread->add_boundary_condition(std::move(my_boundary), domain_to, boundary_idx_from);
    }
  }else{
    std::cerr << "Domain not added to particlespecies !" << std::endl;
    exit(-1);
  }
}

void ParticleSpecies::set_boundary_periodic(BoxDomain& box_domain_from){
  for (int boundary_idx: box_domain_from.boundary_indices()){
    this->set_boundary_periodic(box_domain_from, boundary_idx);
  }
}

void ParticleSpecies::set_boundary_periodic(BoxDomain& box_domain_from, int boundary_idx){

  PeriodicBoundary_Box periodic_boundary(*this, box_domain_from, boundary_idx);
  this->add_boundary_condition(periodic_boundary);
}

void ParticleSpecies::set_boundary_periodic(BoxDomain& box_domain_from, std::string boundary_label){
  int boundary_idx = box_domain_from.boundary_idx(boundary_label);
  this->set_boundary_periodic(box_domain_from, boundary_idx);
}

void ParticleSpecies::set_boundary_transmission(Domain& domain_from, Domain& domain_to){
  for (int boundary_idx: domain_from.boundary_indices()){

    TransmissionBoundary transmission_boundary(*this, domain_from, boundary_idx, domain_to);
    this->add_boundary_condition(transmission_boundary);
  }
}

void ParticleSpecies::set_boundary_transmission(Domain& domain_from, int boundary_idx_from, Domain& domain_to){

  TransmissionBoundary transmission_boundary(*this, domain_from, boundary_idx_from, domain_to);
  this->add_boundary_condition(transmission_boundary);
}

void ParticleSpecies::set_boundary_transmission(Domain& domain_from, std::string boundary_label_from, Domain& domain_to){
  int boundary_idx_from = domain_from.boundary_idx(boundary_label_from);
  TransmissionBoundary transmission_boundary(*this, domain_from, boundary_idx_from, domain_to);
  this->add_boundary_condition(transmission_boundary);
}

////////////////////////////////////////////////////////////////////////////////


// PARTICLE GENERATION
////////////////////////////////////////////////////////////////////////////////
bool ParticleSpecies::add_particle(Domain* domain, double x_coord, double y_coord, double z_coord, double vx, double vy, double vz, int weight, int thread_id/*=0*/){
  // Don't place this particle in a position that a contained domain occupies
  bool coord_inside_contained_domain = false;
  for (Domain* contained_domain: domain->contained_domains()){
    if (contained_domain->is_coord_inside(x_coord, y_coord, z_coord)){
      coord_inside_contained_domain = true;
      break;
    }
  }

  if (!coord_inside_contained_domain){
    // We can add the particle to the particle swarm inside domain
    auto it = _particle_swarms.find(domain);
    if (it != _particle_swarms.end()){
      std::unique_ptr<ParticleSwarm>& particle_swarm = (it->second)[thread_id];
      particle_swarm->add_particle(x_coord, y_coord, z_coord, vx, vy, vz, 0.0, 0.0, 0.0, weight);
      return true;
    }else{
      std::cerr << "Domain not added to ParticleSpecies !" << std::endl;
      exit(-1);
    }
  }else{
    return false;
  }
}

void ParticleSpecies::generate_particles_origin(Domain* domain, int n_particles){

  // TODO: Load balance between all particle swarms in the threads
  for (int i = 0; i < n_particles; ++i){
    this->add_particle(domain, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1);
  }
}

void ParticleSpecies::generate_particles_uniform(BoxDomain* box_domain, int n_particles){
  auto it = _particle_swarms.find(box_domain);

  if (it != _particle_swarms.end()){
    double x_min = box_domain->x_min();
    double delta_x = box_domain->delta_x();
    double y_min = box_domain->y_min();
    double delta_y = box_domain->delta_y();
    double z_min = box_domain->z_min();
    double delta_z = box_domain->delta_z();

    // TODO: Sample velocity from a distribution ?
    double vel_x = 0.0;
    double vel_y = 0.0;
    double vel_z = 0.0;

    int weight = 1;

    std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms = it->second;
    int n_swarms = particle_swarms.size();
    int idx_swarm = 0;
    int n_particles_left = n_particles;
    while (n_particles_left > 0){
      std::unique_ptr<ParticleSwarm>& particle_swarm = particle_swarms[idx_swarm];
      double x_rnd = x_min + _rng->random_01() * delta_x;
      double y_rnd = y_min + _rng->random_01() * delta_y;
      double z_rnd = z_min + _rng->random_01() * delta_z;

      if (this->add_particle(box_domain, x_rnd, y_rnd, z_rnd, vel_x, vel_y, vel_z, weight, idx_swarm)){
        n_particles_left--;
        // We want to cycle through all particle swarms to have a even load across all threads.
        if (idx_swarm + 1 < n_swarms){
          idx_swarm++;
        }else{
          idx_swarm = 0;
        }
      }
    }
  }
}

void ParticleSpecies::_remove_queued_particles_from_swarms(){
  auto it = _particle_swarms.begin();

  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms_in_domain = it->second;
    #pragma omp parallel for
    for (int i = 0; i < particle_swarms_in_domain.size(); ++i){
      particle_swarms_in_domain[i]->_remove_particles_queued_for_removal();
    }
  }
}

void ParticleSpecies::_load_balance_particle_swarms(){
  std::cout << "Load balancing" << std::endl;
  //#pragma omp parallel for
  for (auto it = _particle_swarms.begin(); it != _particle_swarms.end(); ++it){
    std::vector<std::unique_ptr<ParticleSwarm>>& particle_swarms_in_domain = it->second;
    std::vector<int> n_active_particles_per_swarm(particle_swarms_in_domain.size());
    int n_total_active_particles = 0;

    for (int i = 0; i < particle_swarms_in_domain.size(); ++i){
      n_active_particles_per_swarm[i] = particle_swarms_in_domain[i]->n_active_particles();
      n_total_active_particles += particle_swarms_in_domain[i]->n_active_particles();
    }

    double ideal_n_per_swarm = n_total_active_particles / particle_swarms_in_domain.size();
    std::cout << ideal_n_per_swarm << std::endl;

    // If any of the thread is 25% over this ideal number we should equalize all
    // TODO: Remove magic number 1.25
    bool thread_is_over_threshold = false;
    for (int n_active_particles_in_swarm: n_active_particles_per_swarm){
      if (n_active_particles_in_swarm >= 1.25 * ideal_n_per_swarm){
        thread_is_over_threshold = true;
        break;
      }
    }

    if (thread_is_over_threshold){
      std::cout << "thread is over threshold" << std::endl;
      // Check the difference of each thread with the ideal.
      // Threads that have too many will have a positive difference
      // Threads that have too little will have a negative difference
      std::vector<int> difference_per_thread(particle_swarms_in_domain.size());
      for (int i = 0; i < difference_per_thread.size(); ++i){
        difference_per_thread[i] = floor(n_active_particles_per_swarm[i] - ideal_n_per_swarm);
      }

      // Take a thread with too many particles and loop through all threads
      // with too little particles and fill them up while also updating the difference
      for (int i = 0; i < difference_per_thread.size(); ++i){
        // We will keep going until the difference can no longer be divided among
        // all threads equally.
        while (difference_per_thread[i] > particle_swarms_in_domain.size()){
          std::unique_ptr<ParticleSwarm>& particle_swarm_too_many = particle_swarms_in_domain[i];

          for (int j = 0; j < difference_per_thread.size(); ++j){
            if (difference_per_thread[j] < 0){
              std::unique_ptr<ParticleSwarm>& particle_swarm_too_little = particle_swarms_in_domain[j];

              int n_particles_added_to_too_little = 0;
              if (difference_per_thread[i] >= abs(difference_per_thread[j])){
                // All particles too little in swarm j can be taken from swarm i
                n_particles_added_to_too_little = abs(difference_per_thread[j]);
              }else{
                // Only part of the deficit in swarm j can be taken from swarm i
                n_particles_added_to_too_little = difference_per_thread[i];
              }

              this->_copy_n_particles_between_swarms(particle_swarm_too_many.get(), particle_swarm_too_little.get(), n_particles_added_to_too_little);

              difference_per_thread[i] -= n_particles_added_to_too_little;
              difference_per_thread[j] += n_particles_added_to_too_little;
            }
          }
        }
      }
    }
  }
  std::cout << "End of load balance" << std::endl;
}

void ParticleSpecies::_copy_n_particles_between_swarms(ParticleSwarm* swarm_from, ParticleSwarm* swarm_to, int n_particles){

  int idx_start_copy_from = swarm_from->n_active_particles() - n_particles;

  // We want to copy part of swarm_from to swarm_to if there is room
  int space_for_n_particles_copy = swarm_to->pos_x.size() - swarm_to->n_active_particles();
  int n_particles_to_copy = n_particles > space_for_n_particles_copy ? space_for_n_particles_copy : n_particles;

  std::copy(swarm_from->pos_x.begin() + idx_start_copy_from, swarm_from->pos_x.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->pos_x.begin() + swarm_to->n_active_particles());
  std::copy(swarm_from->pos_y.begin() + idx_start_copy_from, swarm_from->pos_y.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->pos_y.begin() + swarm_to->n_active_particles());
  std::copy(swarm_from->pos_z.begin() + idx_start_copy_from, swarm_from->pos_z.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->pos_z.begin() + swarm_to->n_active_particles());

  std::copy(swarm_from->vel_x.begin() + idx_start_copy_from, swarm_from->vel_x.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->vel_x.begin() + swarm_to->n_active_particles());
  std::copy(swarm_from->vel_y.begin() + idx_start_copy_from, swarm_from->vel_y.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->vel_y.begin() + swarm_to->n_active_particles());
  std::copy(swarm_from->vel_z.begin() + idx_start_copy_from, swarm_from->vel_z.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->vel_z.begin() + swarm_to->n_active_particles());

  std::copy(swarm_from->accel_x.begin() + idx_start_copy_from, swarm_from->accel_x.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->accel_x.begin() + swarm_to->n_active_particles());
  std::copy(swarm_from->accel_y.begin() + idx_start_copy_from, swarm_from->accel_y.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->accel_y.begin() + swarm_to->n_active_particles());
  std::copy(swarm_from->accel_z.begin() + idx_start_copy_from, swarm_from->accel_z.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->accel_z.begin() + swarm_to->n_active_particles());

  std::copy(swarm_from->weight.begin() + idx_start_copy_from, swarm_from->weight.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->weight.begin() + swarm_to->n_active_particles());

  std::copy(swarm_from->t_coll.begin() + idx_start_copy_from, swarm_from->t_coll.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->t_coll.begin() + swarm_to->n_active_particles());
  std::copy(swarm_from->t_step.begin() + idx_start_copy_from, swarm_from->t_step.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->t_step.begin() + swarm_to->n_active_particles());
  std::copy(swarm_from->t_left.begin() + idx_start_copy_from, swarm_from->t_left.begin() + idx_start_copy_from + n_particles_to_copy, swarm_to->t_left.begin() + swarm_to->n_active_particles());



  // When there is no more room we want to push back the rest
  int n_particles_to_push_back = n_particles - n_particles_to_copy;

  if (n_particles_to_push_back > 0){
    std::copy(swarm_from->pos_x.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->pos_x.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->pos_x));
    std::copy(swarm_from->pos_y.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->pos_y.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->pos_y));
    std::copy(swarm_from->pos_z.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->pos_z.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->pos_z));

    std::copy(swarm_from->vel_x.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->vel_x.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->vel_x));
    std::copy(swarm_from->vel_y.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->vel_y.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->vel_y));
    std::copy(swarm_from->vel_z.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->vel_z.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->vel_z));

    std::copy(swarm_from->accel_x.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->accel_x.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->accel_x));
    std::copy(swarm_from->accel_y.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->accel_y.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->accel_y));
    std::copy(swarm_from->accel_z.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->accel_z.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->accel_z));

    std::copy(swarm_from->weight.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->weight.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->weight));

    std::copy(swarm_from->t_coll.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->t_coll.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->t_coll));
    std::copy(swarm_from->t_step.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->t_step.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->t_step));
    std::copy(swarm_from->t_left.begin() + idx_start_copy_from + n_particles_to_copy, swarm_from->t_left.begin() + swarm_from->n_active_particles(), back_inserter(swarm_to->t_left));
  }

  // Set weights of the particles to 0 in the swarm where we removed them from
  std::fill(swarm_from->weight.begin() + idx_start_copy_from, swarm_from->weight.begin() + swarm_from->n_active_particles(), 0);

  swarm_from->_n_active_particles -= n_particles;
  swarm_to->_n_active_particles += n_particles;
}
////////////////////////////////////////////////////////////////////////////////
