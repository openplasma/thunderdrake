#include "VectorField.h"


VectorField::VectorField()
  : _is_constant(true),
    _x_component(0),
    _y_component(0),
    _z_component(0){
}

VectorField::VectorField(double x_comp, double y_comp, double z_comp)
  : _is_constant(true),
    _x_component(x_comp),
    _y_component(y_comp),
    _z_component(z_comp){
}

VectorField::VectorField(const VectorField& copy_field)
  : _is_constant(copy_field._is_constant),
    _x_component(copy_field._x_component),
    _y_component(copy_field._y_component),
    _z_component(copy_field._z_component){
}

VectorField::~VectorField(){}

VectorField& VectorField::operator=(const VectorField& rhs){

  _is_constant = rhs._is_constant;
  _x_component = rhs._x_component;
  _y_component = rhs._y_component;
  _z_component = rhs._z_component;
}

////////////////////////////////////////////////////////////////////////////////
void VectorField::set_x_component(double x_comp){
  _x_component = x_comp;
}

void VectorField::set_y_component(double y_comp){
  _y_component = y_comp;
}

void VectorField::set_z_component(double z_comp){
  _z_component = z_comp;
}

double VectorField::x_at_pos(double pos_x, double pos_y, double pos_z) const{
  if (_is_constant){
    return _x_component;
  }else{
    return 0.0;
  }
}

double VectorField::y_at_pos(double pos_x, double pos_y, double pos_z) const{
  if (_is_constant){
    return _y_component;
  }else{
    return 0.0;
  }
}

double VectorField::z_at_pos(double pos_x, double pos_y, double pos_z) const{
  if (_is_constant){
    return _z_component;
  }else{
    return 0.0;
  }
}
////////////////////////////////////////////////////////////////////////////////
