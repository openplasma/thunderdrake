#include "RngXoroshiro128p.h"

#include <cstring>

#include "RngSplitMix64.h"

RngXoroshiro128p::RngXoroshiro128p(uint64_t seed)
  : CloneableRng<RngXoroshiro128p>(seed),
    _a(24),
    _b(16),
    _c(37),
    _generated_initial_seeds(std::vector<uint64_t>(2)),
    _seeds_for_next_random_call(std::vector<uint64_t>(2)){

  this->_generate_initial_seeds(seed);
}

RngXoroshiro128p::~RngXoroshiro128p(){
}

// Setters
////////////////////////////////////////////////////////////////////////////////
void RngXoroshiro128p::set_a(uint64_t a){
  _a = a;
}

void RngXoroshiro128p::set_b(uint64_t b){
  _b = b;
}

void RngXoroshiro128p::set_c(uint64_t c){
  _c = c;
}

void RngXoroshiro128p::set_seed(uint64_t seed){
  _seed = seed;
  _generate_initial_seeds(seed);
}
////////////////////////////////////////////////////////////////////////////////

// Getters
////////////////////////////////////////////////////////////////////////////////
uint64_t RngXoroshiro128p::a() const{
  return _a;
}

uint64_t RngXoroshiro128p::b() const{
  return _b;
}

uint64_t RngXoroshiro128p::c() const{
  return _c;
}

std::vector<uint64_t> RngXoroshiro128p::generated_initial_seeds() const{
  return _generated_initial_seeds;
}

std::vector<uint64_t> RngXoroshiro128p::seeds_for_next_random_call() const{
  return _seeds_for_next_random_call;
}
////////////////////////////////////////////////////////////////////////////////

// Operations
////////////////////////////////////////////////////////////////////////////////
uint64_t RngXoroshiro128p::random_01(){

  uint64_t random = this->random();

  return cast_random_to_range01(random);
}

uint64_t RngXoroshiro128p::random(){
  uint64_t s0 = _seeds_for_next_random_call[0];
	uint64_t s1 = _seeds_for_next_random_call[1];
	uint64_t result = s0 + s1;

	s1 ^= s0;
	_seeds_for_next_random_call[0] = this->_rotl(s0, 24) ^ s1 ^ (s1 << 16); // a, b
	_seeds_for_next_random_call[1] = this->_rotl(s1, 37); // c

	return result;
}

uint64_t RngXoroshiro128p::cast_random_to_range01(uint64_t random){
  // This is some weird/old union shenanigans. Look at https://www.ibm.com/support/knowledgecenter/SSLTBW_2.1.0/com.ibm.zos.v2r1.cbclx01/strin.htm
  // for some info of this notation to initialize a member of the union.
  // Also check https://stackoverflow.com/a/2313676
  // for a good explanation on WHY we use a union for this. We don't use it to save memory, but for type punning.
  // The point of this union is to convert the uint64_t result of the Rng to a double in the range [1, 2).
  /*const union{
    uint64_t i;
    double d;
  } u = {.i = UINT64_C(0x3FF) << 52 | random >> 12 };*/
  // Type punning with unions gives undefined behavior in C++11 and onwards as
  // documented here: https://stackoverflow.com/a/25672839 and here: https://stackoverflow.com/a/31080901
  // We will use a method of type punning that SHOULD (otherwise it is a compiler bug) produce the same machine code
  // but is well defined always.
  uint64_t i = UINT64_C(0x3FF) << 52 | random >> 12;
  double d;
  // We convert the Rng uint64_t result to a double in the range [1, 2)
  std::memcpy(&d, &i, sizeof i);
  // We now convert a random double in the range [1, 2) to a random double in the range [0, 1)
  return d - 1.0;
}

uint64_t RngXoroshiro128p::_rotl(uint64_t x, int k){
  return (x << k) | (x >> (64 - k));
}

void RngXoroshiro128p::_generate_initial_seeds(uint64_t seed){
  RngSplitMix64 my_splitmix64(seed);

  _generated_initial_seeds[0] = my_splitmix64.random();
  _generated_initial_seeds[1] = my_splitmix64.random();

  _seeds_for_next_random_call = _generated_initial_seeds;
}
////////////////////////////////////////////////////////////////////////////////
