#include "RngSplitMix64.h"

#include <cstring>

RngSplitMix64::RngSplitMix64(uint64_t seed): CloneableRng(seed){
  _seed_for_next_random_call = seed;
}

RngSplitMix64::~RngSplitMix64(){
}

// Setters
////////////////////////////////////////////////////////////////////////////////
void RngSplitMix64::set_seed(uint64_t seed){
  _seed = seed;
  _seed_for_next_random_call = seed;
}
////////////////////////////////////////////////////////////////////////////////

// Getters
////////////////////////////////////////////////////////////////////////////////
uint64_t RngSplitMix64::seed_for_next_random_call() const{
  return _seed_for_next_random_call;
}
////////////////////////////////////////////////////////////////////////////////

// Operations
////////////////////////////////////////////////////////////////////////////////
uint64_t RngSplitMix64::random_01(){
  uint64_t random = this->random();

  return this->cast_random_to_range01(random);
}

uint64_t RngSplitMix64::random(){
  // Implementation of SplitMix64 in accordance to: http://prng.di.unimi.it/splitmix64.c
	uint64_t z = (_seed_for_next_random_call += 0x9e3779b97f4a7c15);
	z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
	z = (z ^ (z >> 27)) * 0x94d049bb133111eb;

  return z ^ (z >> 31);
}

uint64_t RngSplitMix64::cast_random_to_range01(uint64_t random){
  // This is some weird/old union shenanigans. Look at https://www.ibm.com/support/knowledgecenter/SSLTBW_2.1.0/com.ibm.zos.v2r1.cbclx01/strin.htm
  // for some info of this notation to initialize a member of the union.
  // Also check https://stackoverflow.com/a/2313676
  // for a good explanation on WHY we use a union for this. We don't use it to save memory, but for type punning.
  // The point of this union is to convert the uint64_t result of the Rng to a double in the range [1, 2).
  /*const union{
    uint64_t i;
    double d;
  } u = {.i = UINT64_C(0x3FF) << 52 | random >> 12 };*/
  // Type punning with unions gives undefined behavior in C++11 and onwards as
  // documented here: https://stackoverflow.com/a/25672839 and here: https://stackoverflow.com/a/31080901
  // We will use a method of type punning that SHOULD (otherwise it is a compiler bug) produce the same machine code
  // but is well defined always.
  uint64_t i = UINT64_C(0x3FF) << 52 | random >> 12;
  double d;
  // We convert the Rng uint64_t result to a double in the range [1, 2)
  std::memcpy(&d, &i, sizeof i);
  // We now convert a random double in the range [1, 2) to a random double in the range [0, 1)
  return d - 1.0;
}
////////////////////////////////////////////////////////////////////////////////
