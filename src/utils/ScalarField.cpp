#include "ScalarField.h"


ScalarField::ScalarField(double value)
  : _value_const(value),
    _is_constant(true),
    _max(value),
    _min(value){
  _find_extrema();
}

ScalarField::~ScalarField(){}

double ScalarField::operator()(double x/*=0.0*/, double y/*=0.0*/, double z/*=0.0*/) const{
  if (_is_constant){
    return _value_const;
  }
  // TODO: else interpolate x, y, z to gridded data
}

double ScalarField::at(double x/*=0.0*/, double y/*=0.0*/, double z/*=0.0*/) const{
  return operator()(x, y, z);
}

double ScalarField::max() const{
  return _max;
}

double ScalarField::min() const{
  return _min;
}

bool ScalarField::is_constant() const{
  return _is_constant;
}

// Operator overloads
////////////////////////////////////////////////////////////////////////////////
ScalarField ScalarField::operator+(const ScalarField& rhs){
  if (_is_constant && rhs._is_constant){
    return ScalarField(_value_const + rhs._value_const);
  }
}

ScalarField ScalarField::operator-(const ScalarField& rhs){
  if (_is_constant && rhs._is_constant){
    return ScalarField(_value_const - rhs._value_const);
  }
}

ScalarField ScalarField::operator*(const ScalarField& rhs){
  if (_is_constant && rhs._is_constant){
    return ScalarField(_value_const * rhs._value_const);
  }
}

ScalarField ScalarField::operator/(const ScalarField& rhs){
  if (_is_constant && rhs._is_constant){
    return ScalarField(_value_const / rhs._value_const);
  }
}

ScalarField& ScalarField::operator+=(const ScalarField& rhs){
  if (_is_constant && rhs._is_constant){
    _value_const += rhs._value_const;

    // Adjust extrema
    _max += rhs._value_const;
    _min += rhs._value_const;
  }

  return *this;
}

ScalarField& ScalarField::operator-=(const ScalarField& rhs){
  if (_is_constant && rhs._is_constant){
    _value_const -= rhs._value_const;

    // Adjust extrema
    _max -= rhs._value_const;
    _min -= rhs._value_const;
  }

  return *this;
}

ScalarField& ScalarField::operator*=(const ScalarField& rhs){
  if (_is_constant && rhs._is_constant){
    _value_const *= rhs._value_const;

    // Adjust extrema
    _max *= rhs._value_const;
    _min *= rhs._value_const;
  }

  return *this;
}

ScalarField& ScalarField::operator/=(const ScalarField& rhs){
  if (_is_constant && rhs._is_constant){
    _value_const /= rhs._value_const;

    // Adjust extrema
    _max /= rhs._value_const;
    _min /= rhs._value_const;
  }

  return *this;
}

ScalarField ScalarField::operator+(const double& rhs){
  if (_is_constant){
    return ScalarField(_value_const + rhs);
  }
}

ScalarField ScalarField::operator-(const double& rhs){
  if (_is_constant){
    return ScalarField(_value_const - rhs);
  }
}

ScalarField ScalarField::operator*(const double& rhs){
  if (_is_constant){
    return ScalarField(_value_const * rhs);
  }
}

ScalarField ScalarField::operator/(const double& rhs){
  if (_is_constant){
    return ScalarField(_value_const / rhs);
  }
}

ScalarField& ScalarField::operator+=(const double& rhs){
  if (_is_constant){
    _value_const += rhs;

    // Adjust extrema
    _max += rhs;
    _min += rhs;
  }

  return *this;
}

ScalarField& ScalarField::operator-=(const double& rhs){
  if (_is_constant){
    _value_const -= rhs;

    // Adjust extrema
    _max -= rhs;
    _min -= rhs;
  }

  return *this;
}

ScalarField& ScalarField::operator*=(const double& rhs){
  if (_is_constant){
    _value_const *= rhs;

    // Adjust extrema
    _max *= rhs;
    _min *= rhs;
  }

  return *this;
}

ScalarField& ScalarField::operator/=(const double& rhs){
  if (_is_constant){
    _value_const /= rhs;

    // Adjust extrema
    _max /= rhs;
    _min /= rhs;
  }

  return *this;
}
////////////////////////////////////////////////////////////////////////////////

void ScalarField::_find_extrema(){
  if (_is_constant){
    _max = _value_const;
    _min = _value_const;
  }
}
