#include <iostream>

#include "Domain.h"
#include "ScalarField.h"
#include "GeneralUtilities.h"
#include "PhysicalConstants.h"

Domain::Domain()
  : _n_contained_domains(0),
    _parent_domain(NULL),
    _pressure_set(false),
    _temperature_set(false),
    _pressure(ScalarField(0)),
    _temperature(ScalarField(0)){
  std::cout << "Creating domain" << std::endl;
}

Domain::Domain(const Domain& copy_domain)
  : _boundary_indices(copy_domain._boundary_indices),
    _n_boundaries(copy_domain._n_boundaries),
    _n_contained_domains(copy_domain._n_contained_domains),
    _contained_domains(copy_domain._contained_domains),
    _pressure(copy_domain._pressure),
    _temperature(copy_domain._temperature),
    _pressure_set(copy_domain._pressure_set),
    _temperature_set(copy_domain._temperature_set),
    _species_fraction(copy_domain._species_fraction),
    _number_densities(copy_domain._number_densities),
    _number_densities_max(copy_domain._number_densities_max),
    _inv_number_densities_max(copy_domain._inv_number_densities_max),
    _parent_domain(copy_domain._parent_domain){

  std::cout << "Copying domain" << std::endl;
}

Domain::~Domain(){
  std::cout << "In domain destructor" << std::endl;
}

// Modifiers / Setters
////////////////////////////////////////////////////////////////////////////////
void Domain::contains_domain(Domain* contained_domain){
  _contained_domains.push_back(contained_domain);
  _n_contained_domains++;

  contained_domain->_set_parent_domain(this);
}

void Domain::_set_parent_domain(Domain* parent_domain){
  _parent_domain = parent_domain;
}

void Domain::set_pressure(ScalarField pressure){
  _pressure = pressure;
  _pressure_set = true;
  // If pressure changes, number densities also change
  this->_calculate_number_densities();
}

void Domain::set_pressure(double pressure){
  _pressure = ScalarField(pressure);
  _pressure_set = true;
  // If pressure changes, number densities also change
  this->_calculate_number_densities();
}

void Domain::set_temperature(ScalarField temperature){
  _temperature = temperature;
  _temperature_set = true;
  // If temperature changes, number densities also change
  this->_calculate_number_densities();
}

void Domain::set_temperature(double temperature){
  _temperature = ScalarField(temperature);
  _temperature_set = true;
  // If temperature changes, number densities also change
  this->_calculate_number_densities();
}

void Domain::add_species(std::string species_name, double fraction_of_gas/*=1.0*/){
  // lowercase and remove spaces of species_name
  std::string sanitized_species_name = TD::lowercase_and_remove_spaces(species_name);

  if (fraction_of_gas <= 1.0){
    _species_fraction[sanitized_species_name] = fraction_of_gas;
  }else{
    std::cerr << "Species fraction of gas goes from 0 to 1 !" << std::endl;
    exit(-1);
  }

  // If another species is added, number densities should be recalculated
  this->_calculate_number_densities();
}

void Domain::_calculate_number_densities(){
  if (_pressure_set && _temperature_set && _species_fraction.size() > 0){
    ScalarField total_number_density = _pressure / (_temperature * TD::kb);

    for (auto it = _species_fraction.begin(); it != _species_fraction.end(); ++it){
      ScalarField species_number_density(total_number_density * it->second);

      auto it_find = _number_densities.find(it->first);
      if (it_find == _number_densities.end()){
        _number_densities.insert(std::pair<std::string, ScalarField>(it->first, species_number_density));
      }else{
        it_find->second = species_number_density;
      }

      _number_densities_max[it->first] = species_number_density.max();
      _inv_number_densities_max[it->first] = 1.0 / species_number_density.max();
    }
  }
}

// Getters
////////////////////////////////////////////////////////////////////////////////
int Domain::n_contained_domains() const{
  return _n_contained_domains;
}

const std::vector<Domain*>& Domain::contained_domains() const{
  return _contained_domains;
}

Domain* Domain::parent_domain() const{
  return _parent_domain;
}

const std::vector<int>& Domain::boundary_indices() const{
  return _boundary_indices;
}

int Domain::n_boundaries() const{
  return _n_boundaries;
}

const ScalarField& Domain::pressure() const{
  return _pressure;
}

const ScalarField& Domain::temperature() const{
  return _temperature;
}

const ScalarField& Domain::number_density(std::string species_name/*=""*/) const{
  std::string sanitized_species_name = TD::lowercase_and_remove_spaces(species_name);

  auto it_find = _number_densities.find(sanitized_species_name);

  if (it_find != _number_densities.end()){
    return it_find->second;
  }else{
    std::cerr << "Species not found in this domain. Did you add_species ?" << std::endl;
    exit(-1);
  }
}

const std::map<std::string, ScalarField>& Domain::number_densities() const{
  return _number_densities;
}

const std::map<std::string, double>& Domain::number_densities_max() const{
  return _number_densities_max;
}

double Domain::number_density_max(std::string species_name) const{
  std::string sanitized_species_name = TD::lowercase_and_remove_spaces(species_name);

  auto it = _number_densities_max.find(sanitized_species_name);

  if (it != _number_densities_max.end()){
    return it->second;
  }else{
    std::cerr << "Species not found in this domain. Did you add_species ?" << std::endl;
    exit(-1);
  }
}

const std::map<std::string, double>& Domain::inv_number_densities_max() const{
  return _inv_number_densities_max;
}

double Domain::inv_number_density_max(std::string species_name) const{
  std::string sanitized_species_name = TD::lowercase_and_remove_spaces(species_name);

  auto it = _inv_number_densities_max.find(sanitized_species_name);
  
  if (it != _inv_number_densities_max.end()){
    return it->second;
  }else{
    std::cerr << "Species not found in this domain. Did you add_species ?" << std::endl;
    exit(-1);
  }
}
////////////////////////////////////////////////////////////////////////////////
