#include "BoxDomain.h"

#include <iostream>
#include <cmath>

#include "ParticleSwarm.h"
#include "GeneralUtilities.h"

BoxDomain::BoxDomain(double x_min, double x_max, double y_min, double y_max, double z_min, double z_max)
  : Domain(),
    _opposite_boundary_indices(std::vector<int>(6)){
  std::cout << "Creating boxdomain" << std::endl;


  _boundary_indices.push_back(_y_max_boundary_idx);
  _boundary_indices.push_back(_x_max_boundary_idx);
  _boundary_indices.push_back(_y_min_boundary_idx);
  _boundary_indices.push_back(_x_min_boundary_idx);
  _boundary_indices.push_back(_z_min_boundary_idx);
  _boundary_indices.push_back(_z_max_boundary_idx);
  _n_boundaries = 6;

  set_x_coords(x_min, x_max);
  set_y_coords(y_min, y_max);
  set_z_coords(z_min, z_max);

  _opposite_boundary_indices[_x_min_boundary_idx] = _x_max_boundary_idx;
  _opposite_boundary_indices[_x_max_boundary_idx] = _x_min_boundary_idx;
  _opposite_boundary_indices[_y_min_boundary_idx] = _y_max_boundary_idx;
  _opposite_boundary_indices[_y_max_boundary_idx] = _y_min_boundary_idx;
  _opposite_boundary_indices[_z_min_boundary_idx] = _z_max_boundary_idx;
  _opposite_boundary_indices[_z_max_boundary_idx] = _z_min_boundary_idx;
}

BoxDomain::~BoxDomain(){
}


void BoxDomain::set_x_coords(double x_min, double x_max){
  if (x_min < x_max){
    _x_min = x_min;
    _x_max = x_max;

    _boundaries[_x_min_boundary_idx] = _x_min;
    _boundaries[_x_max_boundary_idx] = _x_max;
    _delta_x = _x_max - _x_min;
  }else{
    // TODO: Yell at user
  }
}

void BoxDomain::set_y_coords(double y_min, double y_max){
  if (y_min < y_max){
    _y_min = y_min;
    _y_max = y_max;

    _boundaries[_y_min_boundary_idx] = _y_min;
    _boundaries[_y_max_boundary_idx] = _y_max;
    _delta_y = _y_max - _y_min;
  }else{
    // TODO: Yell at user
  }
}

void BoxDomain::set_z_coords(double z_min, double z_max){
  if (z_min < z_max){
    _z_min = z_min;
    _z_max = z_max;

    _boundaries[_z_min_boundary_idx] = _z_min;
    _boundaries[_z_max_boundary_idx] = _z_max;
    _delta_z  = _z_max - _z_min;
  }else{
    // TODO: YELL AT USER !!!!
  }
}

double BoxDomain::delta_x(){
  return _delta_x;
}

double BoxDomain::delta_y(){
  return _delta_y;
}

double BoxDomain::delta_z(){
  return _delta_z;
}

double BoxDomain::x_min() const{
  return _x_min;
}

double BoxDomain::x_max() const{
  return _x_max;
}

double BoxDomain::y_min() const{
  return _y_min;
}

double BoxDomain::y_max() const{
  return _y_max;
}

double BoxDomain::z_min() const{
  return _z_min;
}

double BoxDomain::z_max() const{
  return _z_max;
}

int BoxDomain::boundary_idx(std::string boundary_label){
  boundary_label = TD::lowercase_and_remove_spaces(boundary_label);

  auto it = _boundary_idx_from_label.find(boundary_label);
  if (it != _boundary_idx_from_label.end()){
    return it->second;
  }else{
    // TODO: Yell at user
    std::cerr << "Label is not recognized" << std::endl;
    return -1;
  }
}

int BoxDomain::oppposite_boundary_idx(int boundary_idx){
  return _opposite_boundary_indices[boundary_idx];
}

double BoxDomain::boundary_pos(int boundary_idx){
  auto it = _boundaries.find(boundary_idx);

  if (it != _boundaries.end()){
    return it->second;
  }else{
    // TODO: Yell at user more clearly
    std::cerr << "Boundary idx not recognized" << std::endl;
    return -1;
  }
}

double BoxDomain::boundary_pos(std::string boundary_label){
  return boundary_pos(boundary_idx(boundary_label));
}

int BoxDomain::boundary_idx(double x, double y, double z){
  if (x == _x_min){
    return _x_min_boundary_idx;
  }else if (x == _x_max){
    return _x_max_boundary_idx;
  }else if (y == _y_min){
    return _y_min_boundary_idx;
  }else if (y == _y_max){
    return _y_max_boundary_idx;
  }else if (z == _z_min){
    return _z_min_boundary_idx;
  }else if (z == _z_max){
    return _z_max_boundary_idx;
  }
}

int BoxDomain::is_particle_inside(ParticleSwarm* particle_swarm, int i_part) const noexcept{

  int particle_inside = 1;

  particle_inside &= particle_swarm->pos_x[i_part] >= _x_min;
  particle_inside &= particle_swarm->pos_x[i_part] <= _x_max;

  particle_inside &= particle_swarm->pos_y[i_part] >= _y_min;
  particle_inside &= particle_swarm->pos_y[i_part] <= _y_max;


  particle_inside &= particle_swarm->pos_z[i_part] >= _z_min;
  particle_inside &= particle_swarm->pos_z[i_part] <= _z_max;

  return particle_inside;
}


int BoxDomain::is_coord_inside(double& pos_x, double& pos_y, double& pos_z) const noexcept{

  if (_x_min <= pos_x && _x_max >= pos_x){
    if (_y_min <= pos_y && _y_max >= pos_y){
      if (_z_min <= pos_z && _z_max >= pos_z){
        return 1;
      }
    }
  }
  return 0;
}

std::pair<int, double> BoxDomain::get_boundary_collision_time(ParticleSwarm* particle_swarm, int i_part) const noexcept{

  // X BOUNDARY CHECK
  double pos_x = particle_swarm->pos_x[i_part];
  if (pos_x >= _x_max){
    double vel_x = particle_swarm->vel_x[i_part];
    double accel_x = particle_swarm->accel_x[i_part];
    return std::pair<int, double>(_x_max_boundary_idx, _calc_boundary_collision_time(_x_max, pos_x, vel_x, accel_x));
  }else if (pos_x <= _x_min){
    double vel_x = particle_swarm->vel_x[i_part];
    double accel_x = particle_swarm->accel_x[i_part];
    return std::pair<int, double>(_x_min_boundary_idx, _calc_boundary_collision_time(_x_min, pos_x, vel_x, accel_x));
  }

  // Y BOUNDARY CHECK
  double pos_y = particle_swarm->pos_y[i_part];
  if (pos_y >= _y_max){
    // _y_max boundary check
    double vel_y = particle_swarm->vel_y[i_part];
    double accel_y = particle_swarm->accel_y[i_part];
    return std::pair<int, double>(_y_max_boundary_idx, _calc_boundary_collision_time(_y_max, pos_y, vel_y, accel_y));
  }else if (pos_y <= _y_min){
    // _y_min boundary check
    double vel_y = particle_swarm->vel_y[i_part];
    double accel_y = particle_swarm->accel_y[i_part];
    return std::pair<int, double>(_y_min_boundary_idx, _calc_boundary_collision_time(_y_min, pos_y, vel_y, accel_y));
  }

  // Z BOUNDARY CHECK
  double pos_z = particle_swarm->pos_z[i_part];
  if (pos_z >= _z_max){
    // _z_max boundary check
    double vel_z = particle_swarm->vel_z[i_part];
    double accel_z = particle_swarm->accel_z[i_part];
    return std::pair<int, double>(_z_max_boundary_idx, _calc_boundary_collision_time(_z_max, pos_z, vel_z, accel_z));
  }else if (pos_z <= _z_min){
    // _z_min boundary check
    double vel_z = particle_swarm->vel_z[i_part];
    double accel_z = particle_swarm->accel_z[i_part];
    return std::pair<int, double>(_z_min_boundary_idx, _calc_boundary_collision_time(_z_min, pos_z, vel_z, accel_z));
  }

  return std::pair<int, double>(-1, 0.0);
}

double BoxDomain::_calc_boundary_collision_time(double boundary_pos, double pos, double vel, double accel) const{
  // We solve a quadratic equation to find the dt' which gives us an intersection
  // with a boundary when we move the particle dt' backwards (it will be negative).
  // x(t + dt + dt') = boundary_pos
  // 0 = 0.5 * dt'^2 * (F(t + dt) / m) + dt' * v(t + dt) + x(t + dt) - boundary_pos
  // Discriminant gives:
  // D = v^2(t + dt) - 4 * (F(t + dt) / (2 * m)) * (x(t + dt) - x_boundary)
  // Note: F(t + dt) / m == a (acceleration)
  double D = vel * vel - 2 * accel * (pos - boundary_pos);

  // Did someone ask for a micro optimzation ????????????
  double denominator = 1.0 / accel;
  double dt1 = (-vel + sqrt(D)) * denominator;
  double dt2 = (-vel - sqrt(D)) * denominator;
  return fabs(dt1) < fabs(dt2) && dt1 < 0.0 ? dt1 : dt2;
}
