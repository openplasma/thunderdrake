#include "Excitation_AB_ABs.h"

#include "ParticleSpecies.h"
#include "ParticleSwarm.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Excitation: A + B -> A + B*
Excitation_AB_ABs::Excitation_AB_ABs(std::string collision_name, ParticleSpecies& colliding_species_A, std::string collision_partner_B, Domain& domain, ParticleSpecies& excited_species_Bs, double energy_loss)
  : CloneableCollision(collision_name, colliding_species_A, collision_partner_B, domain, energy_loss){

    if (excited_species_Bs.is_in_domain(domain)){
      _excited_species_Bs = &excited_species_Bs;
    }else{
      std::cerr << "Excited species is not in domain !" << std::endl;
      exit(-1);
    }
}

Excitation_AB_ABs::Excitation_AB_ABs(std::string collision_name, ParticleSpecies& colliding_species_A, std::string collision_partner_B, Domain& domain, double energy_loss)
  : CloneableCollision(collision_name, colliding_species_A, collision_partner_B, domain, energy_loss),
    _excited_species_Bs(NULL),
    _excited_swarm_Bs(NULL){
}

Excitation_AB_ABs::~Excitation_AB_ABs(){
}

void Excitation_AB_ABs::user_initialize(int thread_id){

  if (_excited_species_Bs != NULL){
    _excited_swarm_Bs = _excited_species_Bs->particle_swarm(*_domain, thread_id);
  }
}

void Excitation_AB_ABs::collide(int i_part){

  // Reduce energy of exciting species
  _colliding_particle_swarm->vel_x[i_part] /= 1.4;
  _colliding_particle_swarm->vel_y[i_part] /= 1.4;
  _colliding_particle_swarm->vel_z[i_part] /= 1.2;

  // Get all initial values of newly created excited particle from exciting particle properties
  double pos_x = _colliding_particle_swarm->pos_x[i_part];
  double pos_y = _colliding_particle_swarm->pos_y[i_part];
  double pos_z = _colliding_particle_swarm->pos_z[i_part];

  int weight = _colliding_particle_swarm->weight[i_part];

  double t_left = _colliding_particle_swarm->t_left[i_part] - _colliding_particle_swarm->t_step[i_part];

  // Create excited particle
  if (_excited_swarm_Bs != NULL){
    _excited_swarm_Bs->add_particle(pos_x, pos_y, pos_z, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, weight, t_left);
  }
}
