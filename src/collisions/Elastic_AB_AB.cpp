#include "Elastic_AB_AB.h"

#include "ParticleSwarm.h"


// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Elastic Collision: A + B -> A + B
Elastic_AB_AB::Elastic_AB_AB(std::string collision_name, ParticleSpecies& colliding_species_A, std::string collision_partner_B, Domain& domain)
  : CloneableCollision(collision_name, colliding_species_A, collision_partner_B, domain, 0.0){
}

Elastic_AB_AB::~Elastic_AB_AB(){
};

void Elastic_AB_AB::collide(int i_part){

  // Direct the incident particle to a random direction
  _colliding_particle_swarm->vel_x[i_part] *= _colliding_particle_swarm->random_01();
  _colliding_particle_swarm->vel_y[i_part] *= _colliding_particle_swarm->random_01();
  _colliding_particle_swarm->vel_z[i_part] *= _colliding_particle_swarm->random_01();
}
