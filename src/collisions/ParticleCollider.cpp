#include "ParticleCollider.h"

#include "ParticleSwarm.h"
#include "Collision.h"
#include "InputData.h"
#include "MathApprox.h"

ParticleCollider::ParticleCollider(){
}

ParticleCollider::~ParticleCollider(){
}

// GETTERS
////////////////////////////////////////////////////////////////////////////////
ParticleSwarm* ParticleCollider::particle_swarm() const{
  return _particle_swarm;
}
////////////////////////////////////////////////////////////////////////////////

// SETTERS
////////////////////////////////////////////////////////////////////////////////
void ParticleCollider::set_particle_swarm(ParticleSwarm* particle_swarm){
  _particle_swarm = particle_swarm;
}
////////////////////////////////////////////////////////////////////////////////

// OPERATIONS
////////////////////////////////////////////////////////////////////////////////
void ParticleCollider::before_advance_swarm(int i_part_start, int i_part_end){
}

void ParticleCollider::after_advance_swarm(int i_part_start, int i_part_end){
}

void ParticleCollider::before_collide_particles(int i_part_start, int i_part_end){
  const std::vector<std::unique_ptr<Collision>>& collisions = _particle_swarm->collisions();

  for (const std::unique_ptr<Collision>& collision: collisions){
    collision->before_collide_particles(i_part_start, i_part_end);
  }
}

void ParticleCollider::after_collide_particles(int i_part_start, int i_part_end){
  const std::vector<std::unique_ptr<Collision>>& collisions = _particle_swarm->collisions();

  for (const std::unique_ptr<Collision>& collision: collisions){
    collision->after_collide_particles(i_part_start, i_part_end);
  }
}

void ParticleCollider::calc_t_collision(int i_part_start, int i_part_end){
  // Calculate collision times
  if (_particle_swarm->n_collisions() > 0){
    double inv_max_total_collision_frequency = (_particle_swarm->inv_total_collision_frequency()).min_y();
    std::vector<double>& t_coll = _particle_swarm->t_coll;

    #pragma omp simd
    for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){
      t_coll[i_part] = -logapprox_d(1 - _particle_swarm->random_01()) * inv_max_total_collision_frequency;
    }
  }
}
////////////////////////////////////////////////////////////////////////////////
