#include "Attachment_EB_Bm.h"

#include "ParticleSpecies.h"
#include "ParticleSwarm.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Attachment: e- + B -> B-
Attachment_EB_Bm::Attachment_EB_Bm(std::string collision_name, ParticleSpecies& colliding_electron_species_E, std::string collision_partner_B, Domain& domain, ParticleSpecies& negative_species_Bm)
  : CloneableCollision(collision_name, colliding_electron_species_E, collision_partner_B, domain, 0.0){

    if (negative_species_Bm.is_in_domain(domain)){
      _negative_species_Bm = &negative_species_Bm;
    }else{
      std::cerr << "Negative species is not in domain !" << std::endl;
      exit(-1);
    }
  }

Attachment_EB_Bm::~Attachment_EB_Bm(){
}

void Attachment_EB_Bm::user_initialize(int thread_id){

  _negative_swarm_Bm = _negative_species_Bm->particle_swarm(*_domain, thread_id);
}

void Attachment_EB_Bm::collide(int i_part){

  // Get initial negative ion properties from attaching electron
  double pos_x = _colliding_particle_swarm->pos_x[i_part];
  double pos_y = _colliding_particle_swarm->pos_y[i_part];
  double pos_z = _colliding_particle_swarm->pos_z[i_part];

  int weight = _colliding_particle_swarm->weight[i_part];

  double t_left = _colliding_particle_swarm->t_left[i_part] - _colliding_particle_swarm->t_step[i_part];

  // Create negative ion
  _negative_swarm_Bm->add_particle(pos_x, pos_y, pos_z, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, weight, t_left);

  // Remove electron
  _colliding_particle_swarm->remove_particle(i_part);
}
