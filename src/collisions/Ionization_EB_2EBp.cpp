#include "Ionization_EB_2EBp.h"

#include "ParticleSpecies.h"
#include "ParticleSwarm.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Ionization: e- + B -> 2e- + B+
Ionization_EB_2EBp::Ionization_EB_2EBp(std::string collision_name, ParticleSpecies& electron_species_E, std::string collision_partner_B, Domain& domain, ParticleSpecies& positive_species_Bp, double energy_loss)
  : CloneableCollision(collision_name, electron_species_E, collision_partner_B, domain, energy_loss){
    if (positive_species_Bp.is_in_domain(domain)){
      _positive_species_Bp = &positive_species_Bp;
    }else{
      std::cerr << "Positive Species is not in domain." << std::endl;
      exit(-1);
    }
  }

Ionization_EB_2EBp::~Ionization_EB_2EBp(){
}

void Ionization_EB_2EBp::user_initialize(int thread_id){

  _positive_swarm_Bp = _positive_species_Bp->particle_swarm(*_domain, thread_id);
}

void Ionization_EB_2EBp::collide(int i_part){

  // Reduce ionizing electron energy
  _colliding_particle_swarm->vel_x[i_part] /= 2;
  _colliding_particle_swarm->vel_y[i_part] /= 2;
  _colliding_particle_swarm->vel_z[i_part] /= 2;

  // Get all initial values for newly created electron based on ionizing electron
  double pos_x = _colliding_particle_swarm->pos_x[i_part];
  double pos_y = _colliding_particle_swarm->pos_y[i_part];
  double pos_z = _colliding_particle_swarm->pos_z[i_part];

  double vel_x = _colliding_particle_swarm->vel_x[i_part];
  double vel_y = _colliding_particle_swarm->vel_y[i_part];
  double vel_z = _colliding_particle_swarm->vel_z[i_part];

  double accel_x = 0.0;
  double accel_y = 0.0;
  double accel_z = 0.0;

  int weight = _colliding_particle_swarm->weight[i_part];

  double t_left = _colliding_particle_swarm->t_left[i_part] - _colliding_particle_swarm->t_step[i_part];

  // Create new electron
  _colliding_particle_swarm->add_particle(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, accel_x, accel_y, accel_z, weight, t_left);

  // Create positive particle Bp at ionizing electron position but without vel and accel
  _positive_swarm_Bp->add_particle(pos_x, pos_y, pos_z, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, weight, t_left);
}
