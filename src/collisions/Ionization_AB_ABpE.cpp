#include "Ionization_AB_ABpE.h"

#include "ParticleSpecies.h"
#include "ParticleSwarm.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Ionization: A + B -> A + B+ + e-
Ionization_AB_ABpE::Ionization_AB_ABpE(std::string collision_name, ParticleSpecies& colliding_species_A, std::string collision_partner_B, Domain& domain, ParticleSpecies& positive_species_Bp, ParticleSpecies& electron_species_E, double energy_loss)
  : CloneableCollision(collision_name, colliding_species_A, collision_partner_B, domain, energy_loss){

    if (positive_species_Bp.is_in_domain(domain)){
      _positive_species_Bp = &positive_species_Bp;
    }else{
      std::cerr << "Positive species not in domain !" << std::endl;
      exit(-1);
    }

    if (electron_species_E.is_in_domain(domain)){
      _electron_species_E = &electron_species_E;
    }else{
      std::cerr << "Electron species not in domain !" << std::endl;
      exit(-1);
    }
  }

Ionization_AB_ABpE::~Ionization_AB_ABpE(){
}

void Ionization_AB_ABpE::user_initialize(int thread_id){

  _positive_swarm_Bp = _positive_species_Bp->particle_swarm(*_domain, thread_id);
  _electron_swarm_E = _electron_species_E->particle_swarm(*_domain, thread_id);
}

void Ionization_AB_ABpE::collide(int i_part){

  // Reduce energy of ionizing species
  _colliding_particle_swarm->vel_x[i_part] /= 2;
  _colliding_particle_swarm->vel_y[i_part] /= 2;
  _colliding_particle_swarm->vel_z[i_part] /= 2;

  // Get all initial values of newly created electron from ionizing particle properties
  double pos_x = _colliding_particle_swarm->pos_x[i_part];
  double pos_y = _colliding_particle_swarm->pos_y[i_part];
  double pos_z = _colliding_particle_swarm->pos_z[i_part];

  double vel_x = _colliding_particle_swarm->vel_x[i_part];
  double vel_y = _colliding_particle_swarm->vel_y[i_part];
  double vel_z = _colliding_particle_swarm->vel_z[i_part];

  double accel_x = 0.0;
  double accel_y = 0.0;
  double accel_z = 0.0;

  int weight = _colliding_particle_swarm->weight[i_part];

  double t_left = _colliding_particle_swarm->t_left[i_part] - _colliding_particle_swarm->t_step[i_part];

  // Create new electron
  _electron_swarm_E->add_particle(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, accel_x, accel_y, accel_z, weight, t_left);

  // Create positive particle Bp at ionizing particle position but without vel and accel
  _positive_swarm_Bp->add_particle(pos_x, pos_y, pos_z, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, weight, t_left);
}
