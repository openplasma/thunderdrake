#include "Detachment_AmB_E.h"

#include "ParticleSpecies.h"
#include "ParticleSwarm.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Detachment: A- + B -> E + (neutrals)
Detachment_AmB_E::Detachment_AmB_E(std::string collision_name, ParticleSpecies& detaching_species_Am, std::string collision_partner_B, Domain& domain, ParticleSpecies& electron_species_E)
  : CloneableCollision(collision_name, detaching_species_Am, collision_partner_B, domain, 0.0){

    if (electron_species_E.is_in_domain(domain)){
      _electron_species_E = &electron_species_E;
    }else{
      std::cerr << "Electorn species is not in domain !" << std::endl;
      exit(-1);
    }
}

Detachment_AmB_E::~Detachment_AmB_E(){
}

void Detachment_AmB_E::user_initialize(int thread_id){

  _electron_swarm_E = _electron_species_E->particle_swarm(*_domain, thread_id);
}

void Detachment_AmB_E::collide(int i_part){
  // In this context "colliding" means "detaching"

  // Get all initial values of newly created electron from detaching particle properties
  double pos_x = _colliding_particle_swarm->pos_x[i_part];
  double pos_y = _colliding_particle_swarm->pos_y[i_part];
  double pos_z = _colliding_particle_swarm->pos_z[i_part];

  int weight = _colliding_particle_swarm->weight[i_part];

  double t_left = _colliding_particle_swarm->t_left[i_part] - _colliding_particle_swarm->t_step[i_part];

  // Create new electron
  _electron_swarm_E->add_particle(pos_x, pos_y, pos_z, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, weight, t_left);

  // Delete negative ion
  _colliding_particle_swarm->remove_particle(i_part);
}
