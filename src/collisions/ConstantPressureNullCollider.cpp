#include "ConstantPressureNullCollider.h"

#include "ParticleSwarm.h"
#include "ScalarField.h"
#include "Domain.h"
#include "InputData.h"
#include "Collision.h"

ConstantPressureNullCollider::ConstantPressureNullCollider(): CloneableParticleCollider(){
}

ConstantPressureNullCollider::~ConstantPressureNullCollider(){
}

void ConstantPressureNullCollider::collide_particles(int i_part_start, int i_part_end){

  int n_collisions = _particle_swarm->n_collisions();
  if (n_collisions > 0){
    const InputData& inv_total_collision_frequency = _particle_swarm->inv_total_collision_frequency();
    const std::vector<InputData>& collision_frequencies = _particle_swarm->collision_frequencies();
    const std::vector<std::unique_ptr<Collision>>& collisions = _particle_swarm->collisions();

    std::vector<double>& pos_x = _particle_swarm->pos_x;
    std::vector<double>& pos_y = _particle_swarm->pos_y;
    std::vector<double>& pos_z = _particle_swarm->pos_z;

    std::vector<double>& vel_x = _particle_swarm->vel_x;
    std::vector<double>& vel_y = _particle_swarm->vel_y;
    std::vector<double>& vel_z = _particle_swarm->vel_z;

    std::vector<double>& t_coll = _particle_swarm->t_coll;
    std::vector<double>& t_step = _particle_swarm->t_step;
    std::vector<int>& weight = _particle_swarm->weight;

    for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){

      // If the particle should collide now AND the particle has actually moved
      if (t_coll[i_part] == 0 && t_step[i_part] > 0 && weight[i_part] > 0){

        double random_number_collision = _particle_swarm->random_01();
        double v2 = vel_x[i_part] * vel_x[i_part] + vel_y[i_part] * vel_y[i_part] + vel_z[i_part] * vel_z[i_part];
        double inv_total_collision_frequency_particle = inv_total_collision_frequency(v2);
        double collision_frequency_particle = 0.0;

        for (int i_collision = 0; i_collision < n_collisions; ++i_collision){

          collision_frequency_particle += collision_frequencies[i_collision](v2);

          if (random_number_collision <= collision_frequency_particle * inv_total_collision_frequency_particle){

            collisions[i_collision]->collide(i_part);
          }
        }
      }
    }
  }
}
