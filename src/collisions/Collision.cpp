#include "Collision.h"

#include <iostream>

#include "ParticleSpecies.h"

Collision::Collision(std::string collision_name, ParticleSpecies& colliding_particle_species_A, std::string collision_partner, Domain& domain, double energy_loss)
  : _collision_name(collision_name),
    _collision_partner(collision_partner),
    _energy_loss(energy_loss),
    _domain(&domain),
    _colliding_particle_species_A(&colliding_particle_species_A){
  std::cout << "Creating Collision" << std::endl;
}

Collision::~Collision(){
  std::cout << "Deleting Collision" << std::endl;
}

// GETTERS
////////////////////////////////////////////////////////////////////////////////
std::string Collision::collision_name() const{
  return _collision_name;
}

std::string Collision::collision_partner() const{
  return _collision_partner;
}

double Collision::energy_loss() const{
  return _energy_loss;
}

Domain* Collision::domain() const{
  return _domain;
}

ParticleSpecies* Collision::colliding_particle_species_A() const{
  return _colliding_particle_species_A;
}

ParticleSwarm* Collision::colliding_particle_swarm() const{
  return _colliding_particle_swarm;
}

int Collision::thread_id() const{
  return _thread_id;
}
////////////////////////////////////////////////////////////////////////////////

void Collision::before_collide_particles(int i_part_start, int i_part_end){
  // To be defined by the user.
}

void Collision::after_collide_particles(int i_part_start, int i_part_end){
  // To be defined by the user.
}

void Collision::initialize(int thread_id){
  // The pointer to the colliding particle_swarm of the given thread_id is set here.
  if (_colliding_particle_species_A->is_in_domain(*_domain)){
    _colliding_particle_swarm = _colliding_particle_species_A->particle_swarm(*_domain, thread_id);
  }else{
    std::cerr << "Colliding particle species is not present in the domain !" << std::endl;
    std::cerr << "Collision is invalid ! Please add relevant Domains to ParticleSpecies !" << std::endl;
    exit(-1);
  }

  _thread_id = thread_id;

  // User defined initialization function is called afterwards.
  this->user_initialize(thread_id);
}

void Collision::user_initialize(int thread_id){
  // To be defined by the user.
}
