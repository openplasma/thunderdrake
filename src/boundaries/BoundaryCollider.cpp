#include "BoundaryCollider.h"

#include "ParticleSwarm.h"
#include "Boundary.h"
#include "Domain.h"



BoundaryCollider::BoundaryCollider(){
}

BoundaryCollider::~BoundaryCollider(){
}

// GETTERS
////////////////////////////////////////////////////////////////////////////////
ParticleSwarm* BoundaryCollider::particle_swarm() const{
  return _particle_swarm;
}
////////////////////////////////////////////////////////////////////////////////

// SETTERS
////////////////////////////////////////////////////////////////////////////////
void BoundaryCollider::set_particle_swarm(ParticleSwarm* particle_swarm){
  _particle_swarm = particle_swarm;
}
////////////////////////////////////////////////////////////////////////////////

// OPERATIONS
////////////////////////////////////////////////////////////////////////////////
void BoundaryCollider::before_advance_swarm(int i_part_start, int i_part_end){
}

void BoundaryCollider::after_advance_swarm(int i_part_start, int i_part_end){
}

void BoundaryCollider::before_boundary_check(int i_part_start, int i_part_end){

  const std::map<Domain*, std::map<int, std::unique_ptr<Boundary>>>& boundary_conditions = _particle_swarm->boundary_conditions();

  auto it_bc_begin = boundary_conditions.begin();
  auto it_bc_end = boundary_conditions.end();

  for (auto it_bc = it_bc_begin; it_bc != it_bc_end; ++it_bc){
    auto it_bc_domain_begin = it_bc->second.begin();
    auto it_bc_domain_end = it_bc->second.end();

    for (auto it_bc_domain = it_bc_domain_begin; it_bc_domain != it_bc_domain_end; ++it_bc_domain){
      it_bc_domain->second->before_boundary_check(i_part_start, i_part_end);
    }
  }
}

void BoundaryCollider::after_boundary_check(int i_part_start, int i_part_end){

  const std::map<Domain*, std::map<int, std::unique_ptr<Boundary>>>& boundary_conditions = _particle_swarm->boundary_conditions();

  auto it_bc_begin = boundary_conditions.begin();
  auto it_bc_end = boundary_conditions.end();

  for (auto it_bc = it_bc_begin; it_bc != it_bc_end; ++it_bc){
    auto it_bc_domain_begin = it_bc->second.begin();
    auto it_bc_domain_end = it_bc->second.end();

    for (auto it_bc_domain = it_bc_domain_begin; it_bc_domain != it_bc_domain_end; ++it_bc_domain){
      it_bc_domain->second->after_boundary_check(i_part_start, i_part_end);
    }
  }
}
////////////////////////////////////////////////////////////////////////////////
