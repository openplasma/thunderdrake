#include "Boundary.h"

#include <iostream>

#include "ParticleSpecies.h"
#include "ParticleSwarm.h"


Boundary::Boundary(ParticleSpecies& particle_species, Domain& domain_from, int boundary_idx_from, Domain& domain_to)
  : _particle_species(&particle_species),
    _domain_from(&domain_from),
    _domain_to(&domain_to),
    _boundary_idx_from(boundary_idx_from){
}

Boundary::Boundary(ParticleSpecies& particle_species, Domain& domain_from, int boundary_idx_from)
  : _particle_species(&particle_species),
    _domain_from(&domain_from),
    _domain_to(NULL),
    _boundary_idx_from(boundary_idx_from){
}

Boundary::~Boundary(){
}

// Getters
////////////////////////////////////////////////////////////////////////////////
ParticleSwarm* Boundary::particle_swarm_from() const{
  return _particle_swarm_from;
}

ParticleSwarm* Boundary::particle_swarm_to() const{
  return _particle_swarm_to;
}

ParticleSpecies* Boundary::particle_species() const{
  return _particle_species;
}

int Boundary::boundary_idx() const{
  return _boundary_idx_from;
}

Domain* Boundary::domain_from() const{
  return _domain_from;
}

Domain* Boundary::domain_to() const{
  return _domain_to;
}

int Boundary::thread_id() const{
  return _thread_id;
}
////////////////////////////////////////////////////////////////////////////////

void Boundary::before_boundary_check(int i_part_start, int i_part_end){
  // To be defined by the user.
}

void Boundary::after_boundary_check(int i_part_start, int i_part_end){
  // To be defined by the user.
}

void Boundary::initialize(int thread_id){

  if (_domain_to == NULL || !_particle_species->is_in_domain(*_domain_to)){
    _particle_swarm_to = NULL;
  }else{
    _particle_swarm_to = _particle_species->particle_swarm(*_domain_to, thread_id);
  }

  if (_particle_species->is_in_domain(*_domain_from)){
    _particle_swarm_from = _particle_species->particle_swarm(*_domain_from, thread_id);
  }else{
    std::cerr << "Particle species is not present in the domains provided !" << std::endl;
    std::cerr << "Boundary is invalid ! Please add relevant Domains to ParticleSpecies !" << std::endl;
    exit(-1);
  }

  _thread_id = thread_id;

  // User defined initialization function is called afterwards.
  this->user_initialize(thread_id);
}

void Boundary::user_initialize(int thread_id){
  // To be defined by the user.
}
