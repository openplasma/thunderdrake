#include "PeriodicBoundary_Box.h"

#include <iostream>
#include <cmath>

#include "BoxDomain.h"
#include "ParticleSwarm.h"

PeriodicBoundary_Box::PeriodicBoundary_Box(ParticleSpecies& particle_species, BoxDomain& domain_from, int boundary_idx_from)
  : CloneableBoundary(particle_species, domain_from, boundary_idx_from){

    _box_domain_from = &domain_from;
}

PeriodicBoundary_Box::~PeriodicBoundary_Box(){
  std::cout << "Destroying PeriodicBoundary_Box" << std::endl;
}

void PeriodicBoundary_Box::operator()(int& i_part_from, double& dt_collision){
  double diff_pos = (*_pos)[i_part_from] - _crossed_boundary_pos;
  int n_delta_pos = ceil(diff_pos / -_delta_pos);

  (*_pos)[i_part_from] += n_delta_pos * _delta_pos;
}

void PeriodicBoundary_Box::user_initialize(int thread_id){
  if (_boundary_idx_from == _box_domain_from->boundary_idx("left")){
    // x_min boundary
    _delta_pos = _box_domain_from->delta_x();
    _pos = &_particle_swarm_from->pos_x;
  }else if (_boundary_idx_from == _box_domain_from->boundary_idx("right")){
    // x_max boundary
    _delta_pos = -_box_domain_from->delta_x();
    _pos = &_particle_swarm_from->pos_x;
  }else if (_boundary_idx_from == _box_domain_from->boundary_idx("top")){
    // y_max boundary
    _delta_pos = -_box_domain_from->delta_y();
    _pos = &_particle_swarm_from->pos_y;
  }else if (_boundary_idx_from == _box_domain_from->boundary_idx("bottom")){
    // y_min boundary
    _delta_pos = _box_domain_from->delta_y();
    _pos = &_particle_swarm_from->pos_y;
  }else if (_boundary_idx_from == _box_domain_from->boundary_idx("front")){
    // z_max boundary
    _delta_pos = -_box_domain_from->delta_z();
    _pos = &_particle_swarm_from->pos_z;
  }else if (_boundary_idx_from == _box_domain_from->boundary_idx("back")){
    // z_min boundary
    _delta_pos = _box_domain_from->delta_z();
    _pos = &_particle_swarm_from->pos_z;
  }else{
    std::cout << "REFERENCE NOT SET" << std::endl;
  }

  _crossed_boundary_pos = _box_domain_from->boundary_pos(_boundary_idx_from);
}
