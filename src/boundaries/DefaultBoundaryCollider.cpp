#include "DefaultBoundaryCollider.h"

#include <iostream>
#include <vector>

#include "Domain.h"
#include "ParticleSwarm.h"
#include "Boundary.h"

DefaultBoundaryCollider::DefaultBoundaryCollider(): CloneableBoundaryCollider(){
}

DefaultBoundaryCollider::~DefaultBoundaryCollider(){
}

void DefaultBoundaryCollider::boundary_check(int i_part_start, int i_part_end){
  const Domain* current_domain = _particle_swarm->current_domain();

  std::vector<double>& pos_x = _particle_swarm->pos_x;
  std::vector<double>& pos_y = _particle_swarm->pos_y;
  std::vector<double>& pos_z = _particle_swarm->pos_z;

  std::vector<int>& weight = _particle_swarm->weight;

  std::vector<double>& t_step = _particle_swarm->t_step;

  for (int i_part = i_part_start; i_part <= i_part_end; ++i_part){

    // Only check particles that are not yet deleted (due to collisions)
    // AND particles that have actually moved.
    if (weight[i_part] > 0 && t_step[i_part] > 0){
      double& part_pos_x = pos_x[i_part];
      double& part_pos_y = pos_y[i_part];
      double& part_pos_z = pos_z[i_part];

      if (current_domain->is_coord_inside(part_pos_x, part_pos_y, part_pos_z)){
          // If there are domains inside the current domain we need to check if
          // the particle is inside the current domain or inside a contained domain
          if (current_domain->n_contained_domains() > 0){
            for (Domain* contained_domain: current_domain->contained_domains()){
                // If there are particles inside a contained domain this means that
                // the particle crossed a domain boundary
                if (contained_domain->is_coord_inside(part_pos_x, part_pos_y, part_pos_z)){
                  const std::map<int, std::unique_ptr<Boundary>>& boundary_conditions_with_contained_domain = _particle_swarm->boundary_conditions(contained_domain);

                  if (boundary_conditions_with_contained_domain.size() > 0){
                    std::pair<int, double> boundary_collision_dt = current_domain->get_boundary_collision_time(_particle_swarm, i_part);

                    auto it = boundary_conditions_with_contained_domain.find(boundary_collision_dt.first);
                    if (it != boundary_conditions_with_contained_domain.end()){
                      (it->second)->operator()(i_part, boundary_collision_dt.second);
                    }else{
                      std::cerr << "Particle crossed domain boundary into contained domain, ";
                      std::cerr << "but no boundary condition was defined for this boundary idx. So we are deleting this particle." << std::endl;
                      _particle_swarm->remove_particle(i_part);
                    }
                  }else{
                      std::cerr << "Particle crossed domain boundary into contained domain, ";
                      std::cerr << "but no boundary conditions were defined. So we are deleting this particle." << std::endl;
                      _particle_swarm->remove_particle(i_part);
                  }
                }
            }
          }
      }else{
        // Particle is outside of current domain. So the particle either crossed
        // the domain boundary to the parent domain OR it crossed a domain boundary
        // of a domain placed against the current domain.


        // If the parent domain doesn't exist (current domain is the base) or
        // the parent domain did not have any other contained domains then
        // the particle crossed a boundary from the current domain to the parent
        if (current_domain->parent_domain() == NULL || current_domain->parent_domain()->n_contained_domains() == 1){
          const std::map<int, std::unique_ptr<Boundary>>& boundary_conditions_with_parent_domain = _particle_swarm->boundary_conditions(current_domain->parent_domain());

          if (boundary_conditions_with_parent_domain.size() > 0){
            std::pair<int, double> boundary_collision_dt = current_domain->get_boundary_collision_time(_particle_swarm, i_part);

            auto it = boundary_conditions_with_parent_domain.find(boundary_collision_dt.first);
            if (it != boundary_conditions_with_parent_domain.end()){
              (it->second)->operator()(i_part, boundary_collision_dt.second);
            }else{
              std::cerr << "Particle crossed domain boundary into parent domain, ";
              std::cerr << "but no boundary condition was defined for this boundary idx. So we are deleting this particle." << std::endl;
              _particle_swarm->remove_particle(i_part);
            }
          }else{
            std::cout << part_pos_x << "\t" << part_pos_y << "\t" << part_pos_z << std::endl;
            std::cerr << "Particle crossed domain boundary into parent domain, ";
            std::cerr << "but no boundary conditions were defined. So we are deleting this particle." << std::endl;
            _particle_swarm->remove_particle(i_part);
          }
        }else{
          // We need to check if the particle is inside any of the contained domains
          // of the parent domain. If there is a boundary condition defined between
          // 2 domains of the parent domain then we assume they share a physical boundary
          // and we can apply this boundary condition

          bool part_in_contained_domain_parent = false;

          for (Domain* contained_domain_in_parent: current_domain->parent_domain()->contained_domains()){
            if (contained_domain_in_parent->is_coord_inside(part_pos_x, part_pos_y, part_pos_z)){
              part_in_contained_domain_parent = true;

              const std::map<int, std::unique_ptr<Boundary>>& boundary_conditions_with_contained_domain_parent = _particle_swarm->boundary_conditions(contained_domain_in_parent);

              if (boundary_conditions_with_contained_domain_parent.size() > 0){
                std::pair<int, double> boundary_collision_dt = current_domain->get_boundary_collision_time(_particle_swarm, i_part);

                auto it = boundary_conditions_with_contained_domain_parent.find(boundary_collision_dt.first);
                if (it != boundary_conditions_with_contained_domain_parent.end()){
                  (it->second)->operator()(i_part, boundary_collision_dt.second);
                }else{
                  std::cerr << "Particle crossed domain boundary into contained domain of parent domain, ";
                  std::cerr << "but no boundary condition was defined for this boundary idx. So we are deleting this particle." << std::endl;
                  _particle_swarm->remove_particle(i_part);
                }
              }else{
                std::cout << part_pos_x << "\t" << part_pos_y << "\t" << part_pos_z << std::endl;
                std::cerr << "Particle crossed domain boundary into contained domain of parent domain, ";
                std::cerr << "but no boundary conditions were defined. So we are deleting this particle." << std::endl;
                _particle_swarm->remove_particle(i_part);
              }

              break;
            }
          }

          if (!part_in_contained_domain_parent){
            // Particle is inside the parent domain after all and we need to
            // process this particle with the boundary condition between the current
            // domain and the parent domain.
            const std::map<int, std::unique_ptr<Boundary>>& boundary_conditions_with_parent_domain = _particle_swarm->boundary_conditions(current_domain->parent_domain());

            if (boundary_conditions_with_parent_domain.size() > 0){
              std::pair<int, double> boundary_collision_dt = current_domain->get_boundary_collision_time(_particle_swarm, i_part);

              auto it = boundary_conditions_with_parent_domain.find(boundary_collision_dt.first);
              if (it != boundary_conditions_with_parent_domain.end()){
                (it->second)->operator()(i_part, boundary_collision_dt.second);
              }else{
                std::cerr << "Particle crossed domain boundary into parent domain, ";
                std::cerr << "but no boundary condition was defined for this boundary idx. So we are deleting this particle." << std::endl;
                _particle_swarm->remove_particle(i_part);
              }
            }else{
              std::cout << part_pos_x << "\t" << part_pos_y << "\t" << part_pos_z << std::endl;
              std::cerr << "Particle crossed domain boundary into parent domain, ";
              std::cerr << "but no boundary conditions were defined. So we are deleting this particle." << std::endl;
              _particle_swarm->remove_particle(i_part);
            }
          }
        }
      }
    }
  }
}
