#include "TransmissionBoundary.h"

#include <iostream>


TransmissionBoundary::TransmissionBoundary(ParticleSpecies& particle_species, Domain& domain_from, int boundary_idx_from, Domain& domain_to)
  : CloneableBoundary(particle_species, domain_from, boundary_idx_from, domain_to){
}

TransmissionBoundary::~TransmissionBoundary(){
}

void TransmissionBoundary::operator()(int& i_part_from, double& dt_collision){
  // Move particle back in time to the boundary collision point
  _particle_swarm_from->move_particle(i_part_from, dt_collision);

  double pos_x = _particle_swarm_from->pos_x[i_part_from];
  double pos_y = _particle_swarm_from->pos_y[i_part_from];
  double pos_z = _particle_swarm_from->pos_z[i_part_from];

  double vel_x = _particle_swarm_from->vel_x[i_part_from];
  double vel_y = _particle_swarm_from->vel_y[i_part_from];
  double vel_z = _particle_swarm_from->vel_z[i_part_from];

  double accel_x = _particle_swarm_from->accel_x[i_part_from];
  double accel_y = _particle_swarm_from->accel_y[i_part_from];
  double accel_z = _particle_swarm_from->accel_z[i_part_from];

  int weight = _particle_swarm_from->weight[i_part_from];

  double tleft = _particle_swarm_from->t_left[i_part_from] - (_particle_swarm_from->t_step[i_part_from] + dt_collision);

  _particle_swarm_to->add_particle(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, accel_x, accel_y, accel_z, weight, tleft);
  _particle_swarm_from->remove_particle(i_part_from);
}
