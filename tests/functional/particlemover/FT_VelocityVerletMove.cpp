#include <iostream>
#include <string>
#include <map>
#include <cmath>
#include <memory>

#include <chrono>
#include "omp.h"

#include "BoxDomain.h"
#include "FlightController.h"
#include "ParticleSpecies.h"
#include "ParticleSwarm.h"
#include "VectorField.h"
#include "ScalarField.h"
#include "Units.h"
#include "PhysicalConstants.h"
#include "GeneralUtilities.h"


int main(){
    double dt_max = 0.5;
    double dt = 1;
    double mass = 1.0;
    int n_steps = 5;
    int n_particles = 10;
    int n_timing_runs = 1;

    int n_threads = 1;
    FlightController simulation(n_threads);

    ParticleSpecies electrons("testrons", 1.0, 1);
    BoxDomain experiment_room(-10, 10, -10, 10, -10, 10);

    electrons.add_domain_recursive(&experiment_room);
   
    electrons.generate_particles_origin(&experiment_room, n_particles);
    
    //std::cout << "n parts: " << electrons.n_active_particles(&experiment_room) << std::endl;
    
    simulation.add_particle_species(&electrons);

    VectorField E_field(1e-5 * TD::V / TD::m, 0,  0);

    simulation.set_electric_field(&E_field);

 
    for (int i = 0; i < n_steps; ++i){
        simulation.advance_particles(dt);
    }

    //std::cout << "n parts: " << electrons.n_active_particles(&experiment_room) << std::endl;

    double F = E_field.x_at_pos(0.0, 0.0, 0.0);
    double acceleration = F / electrons.mass();
    double analytical_pos_x = 0.5 * acceleration * simulation.current_time() * simulation.current_time();
    //std::cout << "Analytical position = " << analytical_pos_x << std::endl;

    for (auto pos_x: electrons.pos_x(&experiment_room)){
        if (pos_x != analytical_pos_x){
            return -1;
        }
    }

    return 0;
}
