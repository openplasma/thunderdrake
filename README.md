# ThunderDrake (WIP)

This is a set of free and open-source code to build your own particle based plasma physics simulation.

The library is built with vectorization (SIMD) in mind by using a Data Oriented Design.

Particles are not stored as a Particle structure but collectively in a SoA ParticleSwarm class.

All other classes are designed to change the data within the ParticleSwarm utilizing SIMD.

Shared memory parallelization is a core concept in designing this library. 

Practically all classes which manipulate ParticleSwarm data are copied to each thread so that users do not have to worry about typical paralellization issues like false sharing, cache line invalidation, and race conditions. This includes the ease of adding and removing particles.

# Build

CMake is used to generate build files. To compile the library (with make) and tests (can be disabled) use the following commands

```bash
cd build
cmake ..
make
```

# Tests

A test for the implemented Velocity Verlet particle mover is implemented in tests/functional/particlemover and can be run using ctest or standalone.

Using ctest:

```bash
cd build
ctest
```

As a standalone binary it can be run as:

```bash
./tests/functional/particlemover/FT_VelocityVerletMover
```