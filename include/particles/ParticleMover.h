#ifndef PARTICLEMOVER_H
#define PARTICLEMOVER_H

#include <memory>

// Forward Declarations
class ParticleSwarm;

class ParticleMover{
public:
  ParticleMover();
  ~ParticleMover();

  // Getters
  ParticleSwarm* particle_swarm() const;

  // Setters
  void set_particle_swarm(ParticleSwarm* particle_swarm);

  // Operations
  virtual void before_advance_swarm(int i_part_start, int i_part_end);
  virtual void after_advance_swarm(int i_part_start, int i_part_end);
  virtual void move_particle(int i_part, double dt) = 0;
  virtual void move_particles(int i_part_start, int i_part_end) = 0;

  virtual std::unique_ptr<ParticleMover> clone() const = 0; // Implemented by the template class below
protected:
  ParticleSwarm* _particle_swarm;
};


/* This template class is designed using the paradigm: Curiously Recursive Template Programming (CRTP).
The problem: We want collisions/boundary conditions/particle movers/etc. to be passed to a function by pointer/reference.
Because we are making use of polymorphism the type that these functions expect will be
pointers and references to the base class (Collision, Boundary, ParticleMover, etc.) eventhough we want to pass
for instance Ionization collision, PeriodicBoundary, VelocityVerletMover, etc.

This is all fine until we want to make a deep copy of these objects passed in the function.
We want to have a deep copy of for instance Ionization collision when we only have
access to a reference or pointer to Collision. (We do this for our multithreading purposes).

The Solution: Each derived class should have a function called "clone()" which
creates a deep copy of the reference or pointer passed. But, this was deemed to
much copy and paste work and also to much knowledge was needed by the users on how to
create their own custom Collision, Boundary, ParticleMover, etc.. For this we chose to create a "new"
class (which is the template class below called CloneableParticleMover) which makes the
clone() function automagically for any derived class. The only thing the user
now needs to do when they create a custom Collision, Boundary, ParticleMover, etc. is to inherit from
this cloneable class.

So for collisions:

class CustomCollision: public CloneableCollision<CustomCollision>
{
  all the typical class stuff goes here
}

and for boundary conditions:

class CustomBoundary: public CloneableBoundary<CustomBoundary>
{
  all the typical class stuff goes here
}
*/
template<typename DerivedParticleMover>
class CloneableParticleMover: public ParticleMover{
public:
  CloneableParticleMover()
    : ParticleMover(){};

  virtual std::unique_ptr<ParticleMover> clone() const{
    return std::make_unique<DerivedParticleMover>(static_cast<const DerivedParticleMover&>(*this));
  };
};
 #endif
