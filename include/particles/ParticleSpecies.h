#ifndef PARTICLESPECIES_H
#define PARTICLESPECIES_H

#include <map>
#include <vector>
#include <memory>
#include <iostream>

#include "FlightController.h"

// Forward Declarations
class Domain;
class BoxDomain;
class ParticleSwarm;
class VectorField;
class Boundary;
class InputData;
class Collision;
class ParticleMover;
enum class ParticleMoverType;
class ParticleCollider;
enum class ParticleColliderType;
class BoundaryCollider;
enum class BoundaryColliderType;
class Rng;


class ParticleSpecies{
  friend FlightController;
public:
  ParticleSpecies(std::string species_name, double mass, int charge_in_e);
  ParticleSpecies(const ParticleSpecies& copy_particlespecies);
  ~ParticleSpecies();

  // Getters
  double mass() const;
  double inv_mass() const;
  std::string species_name() const;
  double charge() const;
  int charge_in_e() const;
  bool has_to_be_advanced() const; // TODO
  int n_active_particles(Domain* domain) const;
  bool is_in_domain(Domain& domain) const;
  bool swarms_have_advanced(Domain* domain) const;
  ParticleSwarm* particle_swarm(Domain& domain, int thread_id=0) const;
  const std::vector<InputData>& collision_cross_sections(Domain* domain) const;
  const std::vector<InputData>& collision_frequencies(Domain* domain) const;
  const std::vector<std::string>& collision_partners(Domain* domain) const;
  const InputData& total_collision_frequency(Domain* domain) const;
  const InputData& inv_total_collision_frequency(Domain* domain) const;
  int n_collisions(Domain* domain) const;
  // Check if a particle was added in one of the particle swarms during the advancement of the swarm
  // or after the advancement of the swarms/specie. We need to advance all newly added
  // particles until their t_left = 0.
  bool particle_was_added_during_or_after_advancement() const;

  std::vector<double> pos_x(Domain* domain) const;
  std::vector<double> pos_y(Domain* domain) const;
  std::vector<double> pos_z(Domain* domain) const;
  std::vector<double> vel_x(Domain* domain) const;
  std::vector<double> vel_y(Domain* domain) const;
  std::vector<double> vel_z(Domain* domain) const;
  std::vector<double> accel_x(Domain* domain) const;
  std::vector<double> accel_y(Domain* domain) const;
  std::vector<double> accel_z(Domain* domain) const;
  std::vector<int> weight(Domain* domain) const;
  std::vector<double> t_coll(Domain* domain) const;
  std::vector<double> t_step(Domain* domain) const;
  std::vector<double> t_left(Domain* domain) const;

  void print_pos_x(Domain* domain, std::ostream& output=std::cout) const;
  void print_pos_y(Domain* domain, std::ostream& output=std::cout) const;
  void print_pos_z(Domain* domain, std::ostream& output=std::cout) const;
  void print_vel_x(Domain* domain, std::ostream& output=std::cout) const;
  void print_vel_y(Domain* domain, std::ostream& output=std::cout) const;
  void print_vel_z(Domain* domain, std::ostream& output=std::cout) const;
  void print_accel_x(Domain* domain, std::ostream& output=std::cout) const;
  void print_accel_y(Domain* domain, std::ostream& output=std::cout) const;
  void print_accel_z(Domain* domain, std::ostream& output=std::cout) const;
  void print_weight(Domain* domain, std::ostream& output=std::cout) const;
  void print_t_coll(Domain* domain, std::ostream& output=std::cout) const;
  void print_t_step(Domain* domain, std::ostream& output=std::cout) const;
  void print_t_left(Domain* domain, std::ostream& output=std::cout) const;

  // Setters
  void set_species_name(std::string species_name);
  void set_mass(double mass);
  void set_charge_in_e(int charge_in_e);
  void set_has_to_be_advanced(bool has_to_be_advanced); // TODO

  void add_domain(Domain* domain, int n_max_particles=2e6);
  void add_domain_recursive(Domain* domain, int n_max_particles=2e6);
  void set_dt_max(Domain* domain, double dt_max);

  void set_particle_mover(ParticleMover& particle_mover, Domain& domain);
  void set_particle_mover(ParticleMover& particle_mover);
  void set_particle_mover(ParticleMoverType particle_mover_type, Domain& domain);
  void set_particle_mover(ParticleMoverType particle_mover_type);

  void set_particle_collider(ParticleCollider& particle_collider, Domain& domain);
  void set_particle_collider(ParticleCollider& particle_collider);
  void set_particle_collider(ParticleColliderType particle_collider_type, Domain& domain);
  void set_particle_collider(ParticleColliderType particle_collider_type);

  void set_boundary_collider(BoundaryCollider& boundary_collider, Domain& domain);
  void set_boundary_collider(BoundaryCollider& boundary_collider);
  void set_boundary_collider(BoundaryColliderType boundary_collider_type, Domain& domain);
  void set_boundary_collider(BoundaryColliderType boundary_collider_type);

  // Add collisions to a domain
  void add_collision(Collision& collision, InputData& cross_section, std::string collision_partner);
  void add_collision(Collision& collision, double cross_section, std::string collision_partner); // TODO

  void add_collision_w_reaction_rate(Collision& collision, InputData& reaction_rate, std::string collision_partner);// TODO
  void add_collision_w_reaction_rate(Collision& collision, double reaction_rate, std::string collision_partner);// TODO

  // Collision Notation
  // The first letter in the collision is the species for which the collision is added
  // e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
  // Ap == A+
  // Am == A-
  // As == A*
  // E == e- (electrons)

  // Ionization: e- + B -> 2e- + B+
  void add_ionization_collision_EB_2EBp(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& positive_species_Bp, double ionization_energy);
  void add_ionization_collision_EB_2EBp(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& positive_species_Bp, double ionization_energy);

  // Ionization: A + B -> A + B+ + e-
  void add_ionization_collision_AB_ABpE(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& positive_species_Bp, ParticleSpecies& electron_species_E, double ionization_energy);
  void add_ionization_collision_AB_ABpE(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& positive_species_Bp, ParticleSpecies& electron_species_E, double ionization_energy);

  // Elastic Collision: A + B -> A + B
  void add_elastic_collision_AB_AB(Domain& domain, InputData& cross_section, std::string collision_partner_B);
  void add_elastic_collision_AB_AB(Domain& domain, double cross_section, std::string collision_partner_B);

  // Attachment: e- + B -> B-
  void add_attachment_collision_EB_Bm(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& negative_species_Bm);
  void add_attachment_collision_EB_Bm(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& negative_species_Bm);

  // Excitation: A + B -> A + B*
  void add_excitation_collision_AB_ABs(Domain& domain, InputData& cross_section, std::string collision_partner_B, double excitation_energy);
  void add_excitation_collision_AB_ABs(Domain& domain, double cross_section, std::string collision_partner_B, double excitation_energy);

  void add_excitation_collision_AB_ABs(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& excited_species_Bs, double excitation_energy);
  void add_excitation_collision_AB_ABs(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& excited_species_Bs, double excitation_energy);

  // Detachment: A- + B -> E + (neutrals)
  void add_detachment_collision_AmB_E(Domain& domain, InputData& cross_section, std::string collision_partner_B, ParticleSpecies& electron_species_E);
  void add_detachment_collision_AmB_E(Domain& domain, double cross_section, std::string collision_partner_B, ParticleSpecies& electron_species_E);

  // Add boundary conditions
  void add_boundary_condition(Boundary& boundary_condition);

  void set_boundary_periodic(BoxDomain& box_domain_from);
  void set_boundary_periodic(BoxDomain& box_domain_from, int boundary_idx);
  void set_boundary_periodic(BoxDomain& box_domain_from, std::string boundary_label);

  void set_boundary_transmission(Domain& domain_from, Domain& domain_to);
  void set_boundary_transmission(Domain& domain_from, int boundary_idx_from, Domain& domain_to);
  void set_boundary_transmission(Domain& domain_from, std::string boundary_label_from, Domain& domain_to);

  // Generate particles inside a domain
  void generate_particles_origin(Domain* domain, int n_particles);
  void generate_particles_uniform(BoxDomain* box_domain, int n_particles);
  bool add_particle(Domain* domain, double x_coord, double y_coord, double z_coord, double vx=0.0, double vy=0.0, double vz=0.0, int weight=1, int thread_id=0);

  // Advance particle swarms to dt
  void advance_species(double dt, VectorField& force);
private:
  // Particle management of ParticleSwarms
  void _remove_queued_particles_from_swarms();
  void _load_balance_particle_swarms();
  void _copy_n_particles_between_swarms(ParticleSwarm* swarm_from, ParticleSwarm* swarm_to, int n_particles);

  // Particle defining constants
  double _mass;
  double _inv_mass;
  std::string _species_name;
  double _charge;
  int _charge_in_e;
  bool _has_to_be_advanced;

  // Collisions
  void _set_collision_input(Domain* domain, InputData& cross_section, std::string collision_partner);
  std::map<Domain*, std::vector<InputData>> _collision_cross_sections;
  std::map<Domain*, std::vector<InputData>> _collision_frequencies;
  std::map<Domain*, std::vector<std::string>> _collision_partners;
  std::map<Domain*, InputData> _total_collision_frequency;
  std::map<Domain*, InputData> _inv_total_collision_frequency;
  std::map<Domain*, int> _n_collisions;

  std::map<Domain*, std::vector<std::unique_ptr<ParticleSwarm>>> _particle_swarms;

  // The FlightController can reset the advancement state of all
  // particlespecies at the beginning of the "advance_particles" function.
  void _set_has_advanced(bool has_advanced);

  // The FlightController can set itself as the FlightController
  // of the ParticleSpecies that was added to the FlightController.
  void _set_flightcontroller(FlightController* flight_controller);
  FlightController* _flight_controller;

  void _set_rng(std::unique_ptr<Rng> rng);
  void _initialize_rng_with_seed(int rng_seed);
  std::unique_ptr<Rng> _rng;
};

#endif
