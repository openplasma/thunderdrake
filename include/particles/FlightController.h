#ifndef FLIGHTCONTROLLER_H
#define FLIGHTCONTROLLER_H

#include <vector>
#include <ctime>
#include <memory>

class VectorField;
class ParticleSpecies;
class Rng;
enum class RngType;

class FlightController{
public:
  FlightController(int n_threads, int rng_seed=std::time(nullptr));
  ~FlightController();

  // Getters
  int n_threads() const;
  int rng_seed() const;
  double current_time() const;
  
  // Setters
  void set_electric_field(VectorField* electric_field);
  void add_particle_species(ParticleSpecies* particle_species);

  void set_rng(Rng& rng);
  void set_rng(RngType rng_type);

  // Operations
  void advance_particles(double dt);

protected:
  bool _particle_was_added_after_advancement_of_species();
  void _set_has_advanced_particle_species(bool has_advanced);

private:
  std::vector<ParticleSpecies*> _particle_species;
  int _n_threads;

  std::unique_ptr<Rng> _rng;
  int _rng_seed;

  double _current_time;

  VectorField* _electric_field;
};


#endif
