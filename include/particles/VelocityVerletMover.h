#ifndef VELOCITYVERLETMOVER_H
#define VELOCITYVERLETMOVER_H

#include "ParticleMover.h"

class VelocityVerletMover: public CloneableParticleMover<VelocityVerletMover>{
public:
  VelocityVerletMover();
  ~VelocityVerletMover();

  virtual void move_particle(int i_part, double dt);
  virtual void move_particles(int i_part_start, int i_part_end);
private:
  void _update_pos(int i_part_start, int i_part_end);
  void _update_accel(int i_part_start, int i_part_end);
  void _update_vel(int i_part_start, int i_part_end);
  void _update_times(int i_part_start, int i_part_end);
};
#endif
