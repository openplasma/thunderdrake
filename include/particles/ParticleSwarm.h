#ifndef PARTICLESWARM_H
#define PARTICLESWARM_H

#include <vector>
#include <memory>
#include <map>
#include <set>

class Domain;
class ParticleSpecies;
class Collision;
class Boundary;
class InputData;
class ParticleCollider;
class BoundaryCollider;
class ParticleMover;
class Rng;

#include "VectorField.h"

/*
ParticleSwarms are always manipulated by a single thread alone !
*/

class ParticleSwarm{
  friend ParticleSpecies;
public:
  // Lifetime. Beware ! A ParticleSwarm is never intended to be used alone
  // but std::make_unique requires the constructor to be public.
  ParticleSwarm(ParticleSpecies* particle_species_parent, Domain* domain, int n_max_particles, int thread_id=0);
  ~ParticleSwarm();

  // Copy constructor
  ParticleSwarm(const ParticleSwarm& copy_swarm, bool with_particles=true);

  // Operators
  ParticleSwarm& operator=(const ParticleSwarm& rhs);

  // Getters
  std::string species_name() const;
  double charge() const;
  int charge_in_e() const;
  double mass() const;
  double inv_mass() const;
  double dt_max() const;
  int n_active_particles() const;
  int n_max_particles() const;
  int n_collisions() const;
  int thread_id() const;
  const VectorField* force() const;
  const std::set<int, std::greater<int>>& idx_parts_to_remove() const;
  bool currently_advancing() const;
  bool has_advanced() const;
  bool particle_added_during_or_after_advancement() const;
  int first_idx_of_particle_added_during_or_after_advancement() const;
  double max_tleft_of_particles_added_during_or_after_advancement() const;
  const std::vector<std::unique_ptr<Collision>>& collisions() const;
  const std::map<Domain*, std::map<int, std::unique_ptr<Boundary>>>& boundary_conditions() const;
  const std::map<int, std::unique_ptr<Boundary>>& boundary_conditions(Domain* domain_to) const; //TODO
  const std::vector<InputData>& collision_cross_sections() const;
  const std::vector<InputData>& collision_frequencies() const;
  const std::vector<std::string>& collision_partners() const;
  const InputData& total_collision_frequency() const;
  const InputData& inv_total_collision_frequency() const;
  const ParticleSpecies* particle_species_parent() const;
  const Domain* current_domain() const;
  const Rng* rng() const;
  double random_01();

  // Publically accessible swarm properties. This level of access is needed
  // so that all collisions and boundaries can access and change these variables.
  std::vector<double> pos_x;
  std::vector<double> pos_y;
  std::vector<double> pos_z;
  std::vector<double> vel_x;
  std::vector<double> vel_y;
  std::vector<double> vel_z;
  std::vector<double> accel_x;
  std::vector<double> accel_y;
  std::vector<double> accel_z;
  std::vector<int> weight;
  std::vector<double> t_coll;
  std::vector<double> t_step;
  std::vector<double> t_left;

  // Manipulate particle swarm
  void add_particle(double x, double y, double z, double vx, double vy, double vz, double ax, double ay, double az, int w, double tleft=0);
  void remove_particle(int i_part);
  void remove_particles(std::vector<int>& i_parts);
  void clear_particles();

  // Advance swarm to dt
  void advance_swarm(double dt, int i_part_start=-1, int i_part_end=-1);
  // Move a single particle dt
  void move_particle(int i_part, double dt);
  // Move a range of particles using their t_step[i_part]
  void move_particles(int i_part_start, int i_part_end);

protected:
  // Setters
  void set_mass(double mass);
  void set_dt_max(double dt_max);
  void set_n_max_particles(int n_max_particles);
  void set_t_left(double tleft);
  void set_thread_id(int thread_id);
  void set_force(VectorField& force);
  void set_has_advanced(bool has_advanced);
  void initialize_rng_with_seed(int rng_seed);
  void add_collision(std::unique_ptr<Collision> my_collision);
  void add_boundary_condition(std::unique_ptr<Boundary> my_boundary, Domain* domain_to, int boundary_idx_from);
  void set_particle_mover(std::unique_ptr<ParticleMover> particle_mover);
  void set_particle_collider(std::unique_ptr<ParticleCollider> particle_collider);
  void set_boundary_collider(std::unique_ptr<BoundaryCollider> boundary_collider);
  void set_rng(std::unique_ptr<Rng> rng);

private:
  // Swarm Properties
  double _mass;
  double _inv_mass;
  double _dt_max;
  int _n_active_particles;
  int _n_max_particles;
  int _n_collisions;
  int _thread_id;
  VectorField* _force;
  std::unique_ptr<Rng> _rng;

  // Advancement Properties
  bool _currently_advancing;
  bool _has_advanced;
  bool _particle_added_during_or_after_advancement;
  int _first_idx_of_particle_added_during_or_after_advancement;
  double _max_tleft_of_particles_added_during_or_after_advancement;
  void _advance_swarm_w_collisions(double dt, int i_part_start, int i_part_end);
  void _advance_swarm_wo_collisions(double dt, int i_part_start, int i_part_end);

  // Parents
  ParticleSpecies* _particle_species_parent; // MOMMY
  Domain* _current_domain; // DADDY

  // Interpolates the forces in space to the particle positions and multiplies with inv_mass to get acceleration.
  void _update_accel(int i_part_start, int i_part_end);
  std::unique_ptr<ParticleMover> _particle_mover;

  double _calc_t_step_w_collisions(int i_part, double& swarm_t_step);
  double _calc_t_step_wo_collisions(int i_part, double& swarm_t_step);
  void _calc_t_step_all_w_collisions(int i_part_start, int i_part_end, double swarm_t_step);
  void _calc_t_step_all_wo_collisions(int i_part_start, int i_part_end, double swarm_t_step);

  // Collisions
  std::vector<std::unique_ptr<Collision>> _collisions;
  void _initialize_collisions();
  std::unique_ptr<ParticleCollider> _particle_collider;

  // Boundary Conditions
  std::map<Domain*, std::map<int, std::unique_ptr<Boundary>>> _boundary_conditions;
  void _initialize_boundary_conditions();
  std::unique_ptr<BoundaryCollider> _boundary_collider;

  // Particle removal
  void _remove_particle(int i_part);
  void _remove_particles_queued_for_removal();
  std::set<int, std::greater<int>> _idx_parts_to_remove;
};

#endif
