#ifndef BOXDOMAIN_H
#define BOXDOMAIN_H

#include <string>
#include <map>

#include "Domain.h"

class BoxDomain: public Domain{
public:
  BoxDomain(double x_min, double x_max, double y_min, double y_max, double z_min, double z_max);
  BoxDomain(const BoxDomain& copy_domain);
  ~BoxDomain();

  void set_x_coords(double x_min, double x_max);
  void set_y_coords(double y_min, double y_max);
  void set_z_coords(double z_min, double z_max);

  double delta_x();
  double delta_y();
  double delta_z();

  double x_min() const;
  double x_max() const;
  double y_min() const;
  double y_max() const;
  double z_min() const;
  double z_max() const;

  virtual int boundary_idx(std::string boundary_label);
  virtual int boundary_idx(double x, double y, double z);
  int oppposite_boundary_idx(int boundary_idx);
  double boundary_pos(int boundary_idx);
  double boundary_pos(std::string boundary_label);

  virtual int is_particle_inside(ParticleSwarm* particle_swarm, int i_part) const noexcept;
  virtual int is_coord_inside(double& pos_x, double& pos_y, double& pos_z) const noexcept;

  virtual std::pair<int, double> get_boundary_collision_time(ParticleSwarm* particle_swarm, int i_part) const noexcept;
private:

  std::map<int, double> _boundaries;

  double _x_min;
  double _x_max;
  double _delta_x;

  double _y_min;
  double _y_max;
  double _delta_y;

  double _z_min;
  double _z_max;
  double _delta_z;

  // Indices for the faces of the box
  const int _y_max_boundary_idx = 0;
  const int _x_max_boundary_idx = 1;

  const int _y_min_boundary_idx = 2;
  const int _x_min_boundary_idx = 3;

  const int _z_min_boundary_idx = 4;
  const int _z_max_boundary_idx = 5;

  // This gives the boundary index for the opposite boundary
  std::vector<int> _opposite_boundary_indices;
  std::map<std::string, int> _boundary_idx_from_label = {{"left", _x_min_boundary_idx},
                                                         {"right", _x_max_boundary_idx},
                                                         {"top", _y_max_boundary_idx},
                                                         {"bottom", _y_min_boundary_idx},
                                                         {"front", _z_max_boundary_idx},
                                                         {"back", _z_min_boundary_idx},
                                                         {"x_min", _x_min_boundary_idx},
                                                         {"x_max", _x_max_boundary_idx},
                                                         {"y_max", _y_max_boundary_idx},
                                                         {"y_min", _y_min_boundary_idx},
                                                         {"z_max", _z_max_boundary_idx},
                                                         {"z_min", _z_min_boundary_idx}};

  double _calc_boundary_collision_time(double boundary_pos, double pos, double vel, double accel) const;
};

#endif
