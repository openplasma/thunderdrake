#ifndef DOMAIN_H
#define DOMAIN_H

#include <vector>
#include <map>

#include "ScalarField.h"

class ParticleSwarm;

class Domain{
public:
  Domain();
  Domain(const Domain& copy_domain);
  ~Domain();

  // Modifiers / Setters
  void contains_domain(Domain* contained_domain);
  void set_pressure(double pressure);
  void set_pressure(ScalarField pressure);
  void set_temperature(double pressure);
  void set_temperature(ScalarField temperature);
  void add_species(std::string species_name, double fraction_of_gas=1.0);

  // Getters
  int n_contained_domains() const;
  const std::vector<Domain*>& contained_domains() const;
  Domain* parent_domain() const;

  const std::vector<int>& boundary_indices() const;
  int n_boundaries() const;
  virtual int boundary_idx(std::string boundary_label) = 0;
  virtual int boundary_idx(double x, double y, double z) = 0;

  const ScalarField& pressure() const;
  const ScalarField& temperature() const;
  const std::map<std::string, ScalarField>& number_densities() const;
  const ScalarField& number_density(std::string species_name) const;
  const std::map<std::string, double>& number_densities_max() const;
  double number_density_max(std::string species_name) const;
  const std::map<std::string, double>& inv_number_densities_max() const;
  double inv_number_density_max(std::string species_name) const;

  virtual int is_particle_inside(ParticleSwarm* particle_swarm, int i_part) const noexcept = 0;
  virtual int is_coord_inside(double& pos_x, double& pos_y, double& pos_z) const noexcept = 0;
  virtual std::pair<int, double> get_boundary_collision_time(ParticleSwarm* particle_swarm, int i_part) const noexcept = 0;

protected:
  void _set_parent_domain(Domain* parent_domain);
  void _calculate_number_densities();

  std::vector<int> _boundary_indices;
  int _n_boundaries;
  int _n_contained_domains;
  std::vector<Domain*> _contained_domains;
  ScalarField _pressure;
  ScalarField _temperature;
  bool _pressure_set;
  bool _temperature_set;
  std::map<std::string, double> _species_fraction;
  std::map<std::string, ScalarField> _number_densities;
  std::map<std::string, double> _number_densities_max;
  std::map<std::string, double> _inv_number_densities_max;
  Domain* _parent_domain;
};
#endif
