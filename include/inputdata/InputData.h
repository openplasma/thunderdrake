#ifndef INPUTDATA_H
#define INPUTDATA_H

#include <string>
#include <vector>


class InputData{
public:
  InputData(std::vector<double> x, std::vector<double> y, double unit_x=1.0, double unit_y=1.0);
  InputData(std::string filename, double unit_x=1.0, double unit_y=1.0, std::string delimiter=" ", int n_header_lines=0);
  InputData(double y);
  ~InputData();

  // Copy
  InputData(const InputData& copy_input_data);

  // Modifiers
  void multiply_x(double val);
  void divide_x(double val);
  void add_x(double val);
  void subtract_x(double val);

  void multiply_y(double val);
  void divide_y(double val);
  void add_y(double val);
  void subtract_y(double val);
  void invert_y();

  // Operator Overloads
  // Combine InputData objects. If an x-value is not present in either InputData
  // it will be linearly interpolated towards by the one not having it.
  // If both InputData have n x-points but neither have a coinciding x-point
  // then the resulting InputData will have 2n x-points.
  InputData operator+(const InputData& rhs);
  InputData operator-(const InputData& rhs);
  InputData operator*(const InputData& rhs);
  InputData operator/(const InputData& rhs);
  InputData& operator+=(const InputData& rhs);
  InputData& operator-=(const InputData& rhs);
  InputData& operator*=(const InputData& rhs);
  InputData& operator/=(const InputData& rhs);
  InputData& operator=(const InputData& rhs);

  // Getters
  std::vector<double> x() const;
  std::vector<double> y() const;
  bool is_constant() const;
  int n_datapoints() const;
  int n_header_lines() const;
  std::string filename() const;
  std::string delimiter() const;
  int delimiter_length() const;
  double max_x() const;
  double min_x() const;
  double max_y() const;
  double min_y() const;

  double operator()(double x=0.0) const;
  double at(double x=0.0) const;

protected:
  // This constructor is only used by derived classes.
  // This constructor does not call read_file but let's the derived class
  // call it.
  InputData(std::string filename, bool called_from_derived);

  // This function should be called in the constructor of the derived class
  virtual void _read_file();

  void _sort_x_y();
  std::pair<int, int> _find_boundary_idx(double x) const;
  std::pair<double, double> _extract_column_data(std::string line, std::string delimiter, int delimiter_length) const;
  void _find_extrema_x();
  void _find_extrema_y();

  std::vector<double> _x;
  std::vector<double> _y;
  int _n_datapoints;
  std::string _filename;
  std::string _delimiter;
  int _delimiter_length;
  int _n_header_lines;
  double _max_x;
  double _min_x;
  double _max_y;
  double _min_y;

  bool _is_constant;
};


#endif
