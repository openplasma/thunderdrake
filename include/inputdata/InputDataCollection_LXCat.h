#ifndef INPUTDATACOLLECTION_LXCAT_H
#define INPUTDATACOLLECTION_LXCAT_H

#include <string>
#include <map>

#include "InputData_LXCat.h"

class InputDataCollection_LXCat{
public:
  InputDataCollection_LXCat(std::string filename, std::string process_type, std::string neutral_species, double unit_x=1.0, double unit_y=1.0);
  ~InputDataCollection_LXCat();

  // Getters
  std::string filename() const;
  std::string process_type() const;
  std::string neutral_species() const;
  double unit_x() const;
  double unit_y() const;
  InputData_LXCat* get_process_inputdata(std::string process);

private:
  void _read_file();

  std::map<std::string, InputData_LXCat> _processes_inputdata;

  std::string _filename;
  std::string _sanitized_process_type;
  std::string _sanitized_neutral_species;
  int _neutral_species_length;
  double _unit_x;
  double _unit_y;
};
#endif
