#ifndef INPUTDATA_LXCAT_H
#define INPUTDATA_LXCAT_H

#include "InputData.h"

class InputData_LXCat: public InputData{
public:

  InputData_LXCat(std::string filename, std::string process_type, std::string reaction, double unit_x=1.0, double unit_y=1.0);
  ~InputData_LXCat();
  
  // Getters
  std::string reaction() const;
  std::string process_type() const;

protected:
  virtual void _read_file();

  std::string _sanitized_reaction;
  std::string _sanitized_process_type;
};

#endif
