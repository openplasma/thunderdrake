/*

The MIT License (MIT)

Copyright (c) 2015 Jacques-Henri Jourdan <jourgun@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef MATHAPPROX_H
#define MATHAPPROX_H

/* Relative error bounded by 1e-5 for normalized outputs
   Returns invalid outputs for nan inputs
   Continuous error */
float expapprox(float val);

/* Relative error bounded by 3e-9 for normalized outputs
   Returns invalid outputs for nan inputs
   Continuous error

   Vectorizable only with AVX512dq extensions because of the
   double->int64 cast. On GCC, use option -mavx512dq. */
double expapprox_d(double val);

/* Absolute error bounded by 1e-6 for normalized inputs
   Returns a finite number for +inf input
   Returns -inf for nan and <= 0 inputs.
   Continuous error. */
float logapprox(float val);

/* Absolute error bounded by 2e-9 for normalized inputs
   Returns a finite number for +inf input
   Returns -inf for nan and <= 0 inputs.
   Continuous error.

   Vectorizable only with AVX512dq extensions because of the
   int64 left shift. On GCC, use option -mavx512dq. */
double logapprox_d(double val);

/* Correct only in [-pi, pi]
   Absolute error bounded by 5e-5
   Continuous error */
float cosapprox(float val);

/* Correct only in [-pi, pi]
   Absolute error bounded by 2e-10
   Continuous error */
double cosapprox_d(double val);

/* Correct only in [-pi, pi]
   Absolute error bounded by 6e-6
   Continuous error */
float sinapprox(float val);

/* Correct only in [-pi, pi]
   Absolute error bounded by 2e-9
   Continuous error */
double sinapprox_d(double val);

#endif
