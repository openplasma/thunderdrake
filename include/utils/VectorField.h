#ifndef VECTORFIELD_H
#define VECTORFIELD_H

#include <vector>

class VectorField{
public:
  VectorField();
  VectorField(double x_comp, double y_comp, double z_comp);
  VectorField(const VectorField& copy_field);
  ~VectorField();

  VectorField& operator=(const VectorField& rhs);

  // Getters
  double x_at_pos(double pos_x, double pos_y, double pos_z) const;
  double y_at_pos(double pos_x, double pos_y, double pos_z) const;
  double z_at_pos(double pos_x, double pos_y, double pos_z) const;

  // Setters
  void set_x_component(double x_comp);
  void set_x_component(std::vector<double> x_comp_vec);

  void set_y_component(double y_comp);
  void set_y_component(std::vector<double> y_comp_vec);

  void set_z_component(double z_comp);
  void set_z_component(std::vector<double> z_comp_vec);

private:

  // If the vectorfield is spatially constant we only need 3 values
  bool _is_constant;
  double _x_component;
  double _y_component;
  double _z_component;

  // If the vectorfield varies in space we need values for the field
  // at grid points
  std::vector<double> _grid;
  std::vector<double> _x_component_vec;
  std::vector<double> _y_component_vec;
  std::vector<double> _z_component_vec;
};

#endif
