#ifndef SCALARFIELD_H
#define SCALARFIELD_H

class ScalarField{
public:
  ScalarField(double value); // Constant scalar field
  //ScalarField(Grid, GridValues) // Gridded data on the gridpoints of the given grid
  ~ScalarField();

  double operator()(double x=0.0, double y=0.0, double z=0.0) const;
  double max() const;
  double min() const;
  double at(double x=0.0, double y=0.0, double z=0.0) const;

  bool is_constant() const;

  // Operator overloads
  ScalarField operator+(const ScalarField& rhs);
  ScalarField operator-(const ScalarField& rhs);
  ScalarField operator*(const ScalarField& rhs);
  ScalarField operator/(const ScalarField& rhs);
  ScalarField& operator+=(const ScalarField& rhs);
  ScalarField& operator-=(const ScalarField& rhs);
  ScalarField& operator*=(const ScalarField& rhs);
  ScalarField& operator/=(const ScalarField& rhs);

  ScalarField operator+(const double& rhs);
  ScalarField operator-(const double& rhs);
  ScalarField operator*(const double& rhs);
  ScalarField operator/(const double& rhs);
  ScalarField& operator+=(const double& rhs);
  ScalarField& operator-=(const double& rhs);
  ScalarField& operator*=(const double& rhs);
  ScalarField& operator/=(const double& rhs);


private:
  double _value_const;
  bool _is_constant;

  void _find_extrema();
  double _max;
  double _min;

  // Grid _mygrid
  // GridValues _mygridvalues
};

#endif
