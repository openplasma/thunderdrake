#ifndef PHYSICALCONSTANTS_H
#define PHYSICALCONSTANTS_H

#include "Units.h"

namespace TD{

  static const double boltzmann_constant = 1.38 * 1e-23 * TD::J / TD::K;
  static const double kb = boltzmann_constant;

  static const double electron_charge = 1.602 * 1e-19 * TD::C;
  static const double e = electron_charge;

  static const double planck_constant = 6.626 * 1e-34 * TD::J * TD::s;
  static const double h = planck_constant;

  static const double reduced_planck_constant = 1.054 * 1e-34 * TD::J * TD::s;
  static const double hbar = reduced_planck_constant;
}

#endif
