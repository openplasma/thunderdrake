#ifndef UNITS_H
#define UNITS_H

// The unit system used is:
/*
    Length = meter
    Time = second
    Energy = joule
    Charge = coulomb
    Mass = kilogram
    Temperature = kelvin
    Amount of Substance = mole

    All other quantities are derived from these:
      surface, frequency, power, force, pressure, current, electric potential,
      electric resistance, electric capacitance, electric inductance, magnetic flux,
      and magnetic field.

    Changing the unit system changes the derived quantities as well.
*/



namespace TD{

  // Length
  static const double meter = 1.0;
  static const double kilometer = 1e3 * meter;
  static const double decimeter = 1e-1 * meter;
  static const double centimeter = 1e-2 * meter;
  static const double millimeter = 1e-3 * meter;
  static const double micrometer = 1e-6 * meter;
  static const double nanometer = 1e-9 * meter;
  static const double angstrom = 1e-10 * meter;
  static const double picometer = 1e-12 * meter;
  static const double femtometer = 1e-15 * meter;

  static const double meter2 = meter * meter;
  static const double meter3 = meter * meter * meter;
  static const double centimeter2 = centimeter * centimeter;
  static const double centimeter3 = centimeter * centimeter * centimeter;

  // Symbols
  static const double m = meter;
  static const double m2 = meter2;
  static const double m3 = meter3;
  static const double km = kilometer;
  static const double dm = decimeter;
  static const double cm = centimeter;
  static const double cm2 = centimeter2;
  static const double cm3 = centimeter3;
  static const double mm = millimeter;
  static const double um = micrometer;
  static const double nm = nanometer;
  static const double pm = picometer;
  static const double fm = femtometer;


  // Surface
  static const double barn = 1e-28 * meter2;
  static const double millibarn = 1e-3 * barn;
  static const double microbarn = 1e-6 * barn;
  static const double nanobarn = 1e-9 * barn;
  static const double picobarn = 1e-12 * barn;
  static const double femtobarn = 1e-15 * barn;

  // Symbols
  static const double b = barn;
  static const double mb = millibarn;
  static const double ub = microbarn;
  static const double nb = nanobarn;
  static const double pb = picobarn;
  static const double fb = femtobarn;


  // Time
  static const double second = 1.0;
  static const double millisecond = 1e-3 * second;
  static const double microsecond = 1e-6 * second;
  static const double nanosecond = 1e-9 * second;
  static const double picosecond = 1e-12 * second;
  static const double femtosecond = 1e-15 * second;
  static const double minute = 60 * second;
  static const double hour = 3600 * second;

  // Symbols
  static const double s = second;
  static const double ms = millisecond;
  static const double us = microsecond;
  static const double ns = nanosecond;
  static const double ps = picosecond;
  static const double fs = femtosecond;
  static const double min = minute;
  static const double hr = hour;


  // Frequency
  static const double hertz = 1.0 / second;
  static const double kilohertz = 1e3 * hertz;
  static const double megahertz = 1e6 * hertz;

  // Symbols
  static const double Hz = hertz;
  static const double kHz = kilohertz;
  static const double MHz = megahertz;


  // Energy
  static const double joule = 1.0;
  static const double megajoule = 1e6 * joule;
  static const double kilojoule = 1e3 * joule;
  static const double millijoule = 1e-3 * joule;
  static const double microjoule = 1e-6 * joule;

  static const double electronvolt = 1.602176 * 1e-19 * joule;
  static const double megaelectronvolt = 1e6 * electronvolt;
  static const double kiloelectronvolt = 1e3 * electronvolt;
  static const double millielectronvolt = 1e-3 * electronvolt;
  static const double microelectronvolt = 1e-6 * electronvolt;
  static const double nanoelectronvolt = 1e-9 * electronvolt;
  static const double picoelectronvolt = 1e-12 * electronvolt;
  static const double femtoelectronvolt = 1e-15 * electronvolt;

  // Symbols
  static const double J = joule;
  static const double MJ = megajoule;
  static const double kJ = kilojoule;
  static const double mJ = millijoule;
  static const double uJ = microjoule;

  static const double eV = electronvolt;
  static const double MeV = megaelectronvolt;
  static const double keV = kiloelectronvolt;
  static const double meV = millielectronvolt;
  static const double neV = nanoelectronvolt;
  static const double peV = picoelectronvolt;
  static const double feV = femtoelectronvolt;


  // Charge
  static const double coulomb = 1.0;
  static const double megacoulomb = 1e6 * coulomb;
  static const double kilocoulomb = 1e3 * coulomb;
  static const double millicoulomb = 1e-3 * coulomb;
  static const double microcoulomb = 1e-6 * coulomb;
  static const double nanocoulomb = 1e-9 * coulomb;
  static const double picocoulomb = 1e-12 * coulomb;
  static const double femtocoulomb = 1e-15 * coulomb;

  // Symbols
  static const double C = coulomb;
  static const double MC = megacoulomb;
  static const double kC = kilocoulomb;
  static const double mC = millicoulomb;
  static const double uC = microcoulomb;
  static const double nC = nanocoulomb;
  static const double pC = picocoulomb;
  static const double fC = femtocoulomb;


  // Mass
  static const double kilogram = 1.0;
  static const double gram = 1e-3 * kilogram;
  static const double milligram = 1e-6 * kilogram;

  // Symbols
  static const double g = gram;
  static const double kg = kilogram;
  static const double mg = milligram;


  // Power
  static const double watt = joule / second;
  static const double terawatt = 1e12 * watt;
  static const double gigawatt = 1e9 * watt;
  static const double megawatt = 1e6 * watt;
  static const double kilowatt = 1e3 * watt;
  static const double milliwatt = 1e-3 * watt;
  static const double microwatt = 1e-6 * watt;
  static const double nanowatt = 1e-9 * watt;
  static const double picowatt = 1e-12 * watt;
  static const double femtowatt = 1e-15 * watt;

  // Symbols
  static const double W = watt;
  static const double TW = terawatt;
  static const double GW = gigawatt;
  static const double MW = megawatt;
  static const double kW = kilowatt;
  static const double mW = milliwatt;
  static const double uW = microwatt;
  static const double nW = nanowatt;
  static const double pW = picowatt;
  static const double fW = femtowatt;


  // Force
  static const double newton = joule / meter;
  static const double giganewton = 1e9 * newton;
  static const double meganewton = 1e6 * newton;
  static const double kilonewton = 1e3 * newton;
  static const double millinewton = 1e-3 * newton;
  static const double micronewton = 1e-6 * newton;
  static const double nanonewton = 1e-9 * newton;
  static const double piconewton = 1e-12 * newton;
  static const double femtonewton = 1e-15 * newton;

  // Symbols
  static const double N = newton;
  static const double GN = giganewton;
  static const double MN = meganewton;
  static const double kN = kilonewton;
  static const double mN = millinewton;
  static const double uN = micronewton;
  static const double nN = nanonewton;
  static const double pN = piconewton;
  static const double fN = femtonewton;


  // Pressure
  static const double pascal = newton / (meter2);
  static const double kilopascal = 1e3 * pascal;
  static const double hectopascal = 1e2 * pascal;
  static const double millipascal = 1e-3 * pascal;
  static const double micropascal = 1e-6 * pascal;
  static const double nanopascal = 1e-9 * pascal;
  static const double picopascal = 1e-12 * pascal;
  static const double femtopascal = 1e-15 * pascal;

  static const double bar = 1e5 * pascal; // also symbol
  static const double millibar = 1e-3 * bar;

  static const double atmosphere = 101325 * pascal;

  static const double torr = 133.3224 * pascal;
  static const double millitorr = 1e-3 * torr;

  static const double psi = 6.894755e3 * pascal; // also symbol

  // Symbols
  static const double Pa = pascal;
  static const double kPa = kilopascal;
  static const double hPa = hectopascal;
  static const double mPa = millipascal;
  static const double uPa = micropascal;
  static const double nPa = nanopascal;
  static const double pPa = picopascal;
  static const double fPa = femtopascal;

  static const double mbar = millibar;

  static const double atm = atmosphere;

  static const double Torr = torr;
  static const double mTorr = millitorr;


  // Current
  static const double ampere = coulomb / second;
  static const double gigaampere = 1e9 * ampere;
  static const double megaampere = 1e6 * ampere;
  static const double kiloampere = 1e3 * ampere;
  static const double milliampere = 1e-3 * ampere;
  static const double microampere = 1e-6 * ampere;
  static const double nanoampere = 1e-9 * ampere;
  static const double picoampere = 1e-12 * ampere;
  static const double femtoampere = 1e-15 * ampere;

  // Symbols
  static const double A = ampere;
  static const double GA = gigaampere;
  static const double MA = megaampere;
  static const double kA = kiloampere;
  static const double mA = milliampere;
  static const double uA = microampere;
  static const double nA = nanoampere;
  static const double pA = picoampere;
  static const double fA = femtoampere;


  // Electric Potential
  static const double volt = joule / coulomb;
  static const double gigavolt = 1e9 * volt;
  static const double megavolt = 1e6 * volt;
  static const double kilovolt = 1e3 * volt;
  static const double millivolt = 1e-3 * volt;
  static const double microvolt = 1e-6 * volt;
  static const double nanovolt = 1e-9 * volt;
  static const double picovolt = 1e-12 * volt;
  static const double femtovolt = 1e-15 * volt;

  // Symbols
  static const double V = volt;
  static const double GV = gigavolt;
  static const double MV = megavolt;
  static const double kV = kilovolt;
  static const double mV = millivolt;
  static const double uV = microvolt;
  static const double nV = nanovolt;
  static const double pV = picovolt;
  static const double fV = femtovolt;


  // Electric Resistance
  static const double ohm = volt / ampere; // also symbol


  // Electric Capacitance
  static const double farad = coulomb / volt;
  static const double millifarad = 1e-3 * farad;
  static const double microfarad = 1e-6 * farad;
  static const double nanofarad = 1e-9 * farad;
  static const double picofarad = 1e-12 * farad;
  static const double femtofarad = 1e-15 * farad;

  // Symbols
  static const double F = farad;
  static const double mF = millifarad;
  static const double uF = microfarad;
  static const double nF = nanofarad;
  static const double pF = picofarad;
  static const double fF = femtofarad;


  // Magnetic Field
  static const double tesla = volt * second / (meter2);

  static const double gauss = 1e-4 * tesla;

  // Symbols
  static const double T = tesla;

  static const double G = gauss;


  // Temperature
  static const double kelvin = 1.0;

  // Symbols
  static const double K = kelvin;


  // Amount of substance
  static const double mole = 1.0;

  // Symbols
  static const double mol = mole;


  // Reduced Electric Field
  static const double townsend = 1.0;

  // Symbols
  static const double Td = townsend;


}

#endif
