#ifndef GENERAL_UTILITIES_H
#define GENERAL_UTILITIES_H

#include <string>
#include <algorithm>
#include <vector>
#include <utility>
#include <iostream>


// Ow yes, here it is. The dreaded dumping ground of commonly used functions
// Something that many objects use in different places.
// A lot of the code has to be tested when making changes here.
// Keep the amount of code here to a MINIMUM

namespace TD{

  inline std::string lowercase_and_remove_spaces(std::string my_string){

    std::string sanitized_string = my_string;

    // Lower case all letters
    std::transform(sanitized_string.begin(), sanitized_string.end(), sanitized_string.begin(), [](unsigned char c){ return std::tolower(c); });


    // Remove whitespace
    sanitized_string.erase(remove_if(sanitized_string.begin(), sanitized_string.end(), isspace), sanitized_string.end());

    return sanitized_string;
  };

  inline std::pair<std::vector<double>, std::vector<double>> sort_two_vectors(std::vector<double>& x, std::vector<double>& y){

    if (x.size() == y.size()){
      // First create a vector of (x, y) pairs
      std::vector<std::pair<double, double>> x_y_pairs;

      for (int i = 0; i < x.size(); ++i){
        x_y_pairs.push_back(std::pair<double, double>(x[i], y[i]));
      }

      // Make a comparison function for this vector of pairs that only looks
      // at the x cooordinate of each pair.
      // When given two pairs (x1, y1) and (x2, y2) we want to sort both pairs
      // looking only at x1 and x2. e.g. if x1 > x2 the order of pairs becomes
      // (x2, y2), (x1, y1).
      auto pair_comparison = [](std::pair<double, double> p1, std::pair<double, double> p2){
        return (p1.first < p2.first);
      };

      // Sort vector of pairs using this pair comparison

      std::sort(x_y_pairs.begin(), x_y_pairs.end(), pair_comparison);

      // get back two vectors x and y which are now sorted in ascending order of x

      std::vector<double> x_sorted(x.size());
      std::vector<double> y_sorted(y.size());

      for (int i = 0; i < x_sorted.size(); ++i){
        x_sorted[i] = x_y_pairs[i].first;
        y_sorted[i] = x_y_pairs[i].second;
      }

      return std::pair<std::vector<double>, std::vector<double>>(x_sorted, y_sorted);
    }else{
      std::cerr << "Cannot merge two vectors of different sizes" << std::endl;
      exit(-1);
    }
  }


  inline std::vector<double> merge_two_sorted_vectors(std::vector<double> vec1, std::vector<double> vec2){
    // ASSUMPTION IS VECTORS ARE SORTED IN ASCENDING ORDER
    if (vec1.size() == 0 && vec2.size() == 0){
      std::cerr << "Cannot sort two vectors of zero size" << std::endl;
      exit(-1);
    }
    // The vector we insert into, should not be empty. Algorithm design, not fundamental property.
    std::vector<double>& non_empty_vector = vec1.size() > 0 ? vec1 : vec2;

    std::vector<double> merged_vector(non_empty_vector);
    std::vector<double>::iterator it = merged_vector.begin();

    for (int i = 0; i < vec2.size(); ++i){

      for (int j = 0; j < merged_vector.size(); ++j){
        if (vec2[i] < merged_vector[j]){
          merged_vector.insert(it + j, vec2[i]);
          break;
        }else if (vec2[i] == merged_vector[j]){
          break;
        }
      }

      if (vec2[i] > merged_vector[merged_vector.size() - 1]){
        // vec2[i] must be larger than all merged_vector so append at back
        merged_vector.push_back(vec2[i]);
      }

      it = merged_vector.begin();
    }
    return merged_vector;
  }

}

#endif
