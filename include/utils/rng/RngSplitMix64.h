#ifndef RNGSPLITMIX64_H
#define RNGSPLITMIX64_H

#include "Rng.h"

class RngSplitMix64: public CloneableRng<RngSplitMix64>{
public:
  RngSplitMix64(uint64_t seed=0);
  ~RngSplitMix64();

  // Setters
  virtual void set_seed(uint64_t seed);

  // Getters
  uint64_t seed_for_next_random_call() const;

  // Operations
  virtual uint64_t random_01();

  // Generate a random number (from 0 to 2^64)
  uint64_t random();
  virtual uint64_t cast_random_to_range01(uint64_t random);
private:
  // Seed that is used for every call to random(). This number changes every iteration
  uint64_t _seed_for_next_random_call;
};




#endif
