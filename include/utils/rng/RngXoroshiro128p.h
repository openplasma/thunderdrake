#ifndef RNGXOROSHIRO128P_H
#define RNGXOROSHIRO128P_H

#include "Rng.h"

#include <vector>

// This class implements xoroshiro128+ in accordance to:
// http://pRng.di.unimi.it/xoroshiro128plus.c

//LICENSE WITHIN http://pRng.di.unimi.it/xoroshiro128plus.c

/*  Written in 2016-2018 by David Blackman and Sebastiano Vigna (vigna@acm.org)

To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty.

See <http://creativecommons.org/publicdomain/zero/1.0/>. */

// We use a single 64 bit seed which we use in a splitmix64 Rng to generate
// two starting values for the xoroshiro128+ Rng.
// The splitmix64 implementation is in accordance to: http://pRng.di.unimi.it/splitmix64.c

// LICENSE WITHIN http://pRng.di.unimi.it/splitmix64.c

/*  Written in 2015 by Sebastiano Vigna (vigna@acm.org)

To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty.

See <http://creativecommons.org/publicdomain/zero/1.0/>. */

class RngXoroshiro128p: public CloneableRng<RngXoroshiro128p>{
public:
  RngXoroshiro128p(uint64_t seed=0);
  ~RngXoroshiro128p();

  // Setters
  void set_a(uint64_t a);
  void set_b(uint64_t b);
  void set_c(uint64_t c);
  virtual void set_seed(uint64_t seed);

  // Getters
  uint64_t a() const;
  uint64_t b() const;
  uint64_t c() const;
  std::vector<uint64_t> generated_initial_seeds() const;
  std::vector<uint64_t> seeds_for_next_random_call() const;

  // Operations
  virtual uint64_t random_01();

  // Generate a random number (from 0 to 2^64)
  uint64_t random();
  virtual uint64_t cast_random_to_range01(uint64_t random);
protected:
  // Parameters of xoroshiro128+
  uint64_t _a;
  uint64_t _b;
  uint64_t _c;
  // Seed array generated from the input seed, to be used by the xoroshiro128+ Rng
  std::vector<uint64_t> _generated_initial_seeds;
  // Seed array that is used for every call to random(). These numbers change every iteration
  std::vector<uint64_t> _seeds_for_next_random_call;

  // Rotate and shift function used for xoroshiro128+
  uint64_t _rotl(uint64_t x, int k);
  // Method to generate the seed array from 1 64 bit number. Currently, splitmix64
  void _generate_initial_seeds(uint64_t seed);
};

#endif
