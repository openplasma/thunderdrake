#ifndef RNGTYPE_H
#define RNGTYPE_H

enum class RngType{
  SplitMix64,
  Xoroshiro128p
};


#endif
