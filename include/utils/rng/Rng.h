#ifndef RNG_H
#define RNG_H

#include <cstdint>
#include <string>
#include <memory>

class Rng{
public:
  Rng(uint64_t seed=0);
  ~Rng();

  // Setters
  virtual void set_seed(uint64_t seed);
  virtual void set_seed(uint64_t seed, std::string seed_string_modifier);

  // Getters
  uint64_t seed() const;

  // Operations
  uint64_t operator()();
  virtual uint64_t random_01() = 0; // Produce a random number in [0, 1] (or ]0, 1], [0, 1[, ]0, 1[)

  virtual std::unique_ptr<Rng> clone() const = 0; // Implemented by the template class below
protected:
  // Initial input seed
  uint64_t _seed;
  // Method to hash a string into a unique number to be used for a seed.
  uint64_t _hash(std::string str);
};


/* This template class is designed using the paradigm: Curiously Recursive Template Programming (CRTP).
The problem: We want collisions/boundary conditions/particle movers/etc. to be passed to a function by pointer/reference.
Because we are making use of polymorphism the type that these functions expect will be
pointers and references to the base class (Collision, Boundary, ParticleMover, etc.) eventhough we want to pass
for instance Ionization collision, PeriodicBoundary, VelocityVerletMover, etc.

This is all fine until we want to make a deep copy of these objects passed in the function.
We want to have a deep copy of for instance Ionization collision when we only have
access to a reference or pointer to Collision. (We do this for our multithreading purposes).

The Solution: Each derived class should have a function called "clone()" which
creates a deep copy of the reference or pointer passed. But, this was deemed to
much copy and paste work and also to much knowledge was needed by the users on how to
create their own custom Collision, Boundary, ParticleMover, etc.. For this we chose to create a "new"
class (which is the template class below called CloneableParticleMover) which makes the
clone() function automagically for any derived class. The only thing the user
now needs to do when they create a custom Collision, Boundary, ParticleMover, etc. is to inherit from
this cloneable class.

So for collisions:

class CustomCollision: public CloneableCollision<CustomCollision>
{
  all the typical class stuff goes here
}

and for boundary conditions:

class CustomBoundary: public CloneableBoundary<CustomBoundary>
{
  all the typical class stuff goes here
}
*/
template<typename DerivedRng>
class CloneableRng: public Rng{
public:
  CloneableRng(uint64_t seed=0)
    : Rng(seed){};

  virtual std::unique_ptr<Rng> clone() const{
    return std::make_unique<DerivedRng>(static_cast<const DerivedRng&>(*this));
  };
};
#endif
