#ifndef PARTICLECOLLIDERTYPE_H
#define PARTICLECOLLIDERTYPE_H

enum class ParticleColliderType{
  VariablePressureNullCollider,
  ConstantPressureNullCollider
};

#endif
