#ifndef VARIABLEPRESSURENULLCOLLIDER_H
#define VARIABLEPRESSURENULLCOLLIDER_H

#include "ParticleCollider.h"


class VariablePressureNullCollider: public CloneableParticleCollider<VariablePressureNullCollider>{
public:
  VariablePressureNullCollider();
  ~VariablePressureNullCollider();

  virtual void collide_particles(int i_part_start, int i_part_end);
};

#endif
