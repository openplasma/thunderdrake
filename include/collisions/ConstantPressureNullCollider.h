#ifndef CONSTANTPRESSURENULLCOLLIDER_H
#define CONSTANTPRESSURENULLCOLLIDER_H

#include "ParticleCollider.h"


class ConstantPressureNullCollider: public CloneableParticleCollider<ConstantPressureNullCollider>{
public:
  ConstantPressureNullCollider();
  ~ConstantPressureNullCollider();

  virtual void collide_particles(int i_part_start, int i_part_end);
};

#endif
