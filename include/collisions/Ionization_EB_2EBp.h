#ifndef IONIZATION_EB_2EBP_H
#define IONIZATION_EB_2EBP_H

#include "Collision.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Ionization: e- + B -> 2e- + B+
class Ionization_EB_2EBp: public CloneableCollision<Ionization_EB_2EBp>{
public:
  Ionization_EB_2EBp(std::string collision_name, ParticleSpecies& electron_species_E, std::string collision_partner_B, Domain& domain, ParticleSpecies& positive_species_Bp, double energy_loss);
  ~Ionization_EB_2EBp();

  virtual void user_initialize(int thread_id);
  virtual void collide(int i_part);
private:
  ParticleSpecies* _positive_species_Bp;
  ParticleSwarm* _positive_swarm_Bp;
};

#endif
