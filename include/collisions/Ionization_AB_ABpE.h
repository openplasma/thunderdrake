#ifndef IONIZATION_AB_ABPE_H
#define IONIZATION_AB_ABPE_H

#include "Collision.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Ionization: A + B -> A + B+ + e-
class Ionization_AB_ABpE: public CloneableCollision<Ionization_AB_ABpE>{
public:
  Ionization_AB_ABpE(std::string collision_name, ParticleSpecies& colliding_species_A, std::string collision_partner_B, Domain& domain, ParticleSpecies& positive_species_Bp, ParticleSpecies& electron_species_E, double energy_loss);
  ~Ionization_AB_ABpE();

  virtual void user_initialize(int thread_id);
  virtual void collide(int i_part);
private:
  ParticleSpecies* _positive_species_Bp;
  ParticleSwarm* _positive_swarm_Bp;

  ParticleSpecies* _electron_species_E;
  ParticleSwarm* _electron_swarm_E;
};

#endif
