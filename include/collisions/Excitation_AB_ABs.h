#ifndef EXCITATION_AB_ABS_H
#define EXCITATION_AB_ABS_H

#include "Collision.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Excitation: A + B -> A + B*
class Excitation_AB_ABs: public CloneableCollision<Excitation_AB_ABs>{
public:
  Excitation_AB_ABs(std::string collision_name, ParticleSpecies& colliding_species_A, std::string collision_partner_B, Domain& domain, ParticleSpecies& excited_species_Bs, double energy_loss);
  Excitation_AB_ABs(std::string collision_name, ParticleSpecies& colliding_species_A, std::string collision_partner_B, Domain& domain, double energy_loss);
  ~Excitation_AB_ABs();

  virtual void user_initialize(int thread_id);
  virtual void collide(int i_part);
private:
  ParticleSpecies* _excited_species_Bs;
  ParticleSwarm* _excited_swarm_Bs;
};

#endif
