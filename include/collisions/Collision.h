#ifndef COLLISION_H
#define COLLISION_H

#include <memory>
#include <string>

// Forward Declarations
class Domain;
class ParticleSpecies;
class ParticleSwarm;


class Collision{
public:
  Collision(std::string collision_name, ParticleSpecies& colliding_particle_species_A, std::string collision_partner, Domain& domain, double energy_loss);
  ~Collision();

  // Getters
  std::string collision_name() const;
  std::string collision_partner() const;
  double energy_loss() const;
  Domain* domain() const;
  ParticleSpecies* colliding_particle_species_A() const;
  ParticleSwarm* colliding_particle_swarm() const;
  int thread_id() const;

  // Operations
  virtual void before_collide_particles(int i_part_start, int i_part_end);
  virtual void after_collide_particles(int i_part_start, int i_part_end);
  virtual void collide(int i_part) = 0;

  // Set member pointers to correct entities based on thread id (each thread has own ParticleSwarm for example)
  void initialize(int thread_id);
  virtual void user_initialize(int thread_id);

  virtual std::unique_ptr<Collision> clone() const = 0; // Implemented by the template class below
protected:
  std::string _collision_name;
  std::string _collision_partner;
  double _energy_loss;
  Domain* _domain;
  ParticleSpecies* _colliding_particle_species_A;
  ParticleSwarm* _colliding_particle_swarm;
  int _thread_id;
};



/* This template class is designed using the paradigm: Curiously Recursive Template Programming (CRTP).
The problem: We want collisions/boundary conditions to be passed to a function by pointer/reference.
Because we are making use of polymorphism the type that these functions expect will be
pointers and references to the base class (Collision or Boundary) eventhough we want to pass
for instance Ionization collision or PeriodicBoundary.

This is all fine until we want to make a deep copy of these objects passed in the function.
We want to have a deep copy of for instance Ionization collision when we only have
access to a reference or pointer to Collision. (We do this for our multithreading purposes).

The Solution: Each derived class should have a function called "clone()" which
creates a deep copy of the reference or pointer passed. But, this was deemed to
much copy and paste work and also to much knowledge was needed by the users on how to
create their own custom Collision or Boundary. For this we chose to create a "new"
class (which is the template class below called CloneableCollision) which makes the
clone() function automagically for any derived class. The only thing the user
now needs to do when they create a custom Collision or Boundary is to inherit from
this cloneable class.

So for collisions:

class CustomCollision: public CloneableCollision<CustomCollision>
{
  all the typical class stuff goes here
}

and for boundary conditions:

class CustomBoundary: public CloneableBoundary<CustomBoundary>
{
  all the typical class stuff goes here
}
*/
template<typename DerivedCollision>
class CloneableCollision: public Collision{
public:
  CloneableCollision(std::string collision_name, ParticleSpecies& particle_species, std::string collision_partner, Domain& domain, double energy_loss=0.0)
    : Collision(collision_name, particle_species, collision_partner, domain, energy_loss){};

  virtual std::unique_ptr<Collision> clone() const{
    return std::make_unique<DerivedCollision>(static_cast<const DerivedCollision&>(*this));
  };
};




#endif
