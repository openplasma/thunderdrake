#ifndef ATTACHMENT_EB_BM_H
#define ATTACHMENT_EB_BM_H

#include "Collision.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Attachment: e- + B -> B-
class Attachment_EB_Bm: public CloneableCollision<Attachment_EB_Bm>{
public:
  Attachment_EB_Bm(std::string collision_name, ParticleSpecies& colliding_electron_species_E, std::string collision_partner_B, Domain& domain, ParticleSpecies& negative_species_Bm);
  ~Attachment_EB_Bm();

  virtual void user_initialize(int thread_id);
  virtual void collide(int i_part);
private:
  ParticleSpecies* _negative_species_Bm;
  ParticleSwarm* _negative_swarm_Bm;
};


#endif
