#ifndef DETACHMENT_AMB_E_H
#define DETACHMENT_AMB_E_H

#include "Collision.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Detachment: A- + B -> E + (neutrals)
class Detachment_AmB_E: public CloneableCollision<Detachment_AmB_E>{
public:
  Detachment_AmB_E(std::string collision_name, ParticleSpecies& detaching_species_Am, std::string collision_partner_B, Domain& domain, ParticleSpecies& electron_species_E);
  ~Detachment_AmB_E();

  virtual void user_initialize(int thread_id);
  virtual void collide(int i_part);
private:
  ParticleSpecies* _electron_species_E;
  ParticleSwarm* _electron_swarm_E;
};

#endif
