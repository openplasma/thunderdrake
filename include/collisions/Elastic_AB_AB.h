#ifndef ELASTIC_AB_AB_H
#define ELASTIC_AB_AB_H

#include "Collision.h"

// Collision Notation
// The first letter in the collision is the species for which the collision is added
// e.g. test_species.add_excitation_collision_AB_ABs()  ->  A == test_species
// Ap == A+
// Am == A-
// As == A*
// E == e- (electrons)

// Elastic Collision: A + B -> A + B
class Elastic_AB_AB: public CloneableCollision<Elastic_AB_AB>{
public:
  Elastic_AB_AB(std::string collision_name, ParticleSpecies& colliding_species_A, std::string collision_partner_B, Domain& domain);
  ~Elastic_AB_AB();

  virtual void collide(int i_part);
};

#endif
