#ifndef STATICSTRUCTUREDGRID_H
#define STATICSTRUCTUREDGRID_H

#include <vector>
#include <map>

class BoxDomain;

class StaticStructuredGrid{
public:
  StaticStructuredGrid();
  ~StaticStructuredGrid();

private:

  int _n_cells;
  int _n_cells_bulk;
  int _n_cells_boundary;

  double _delta_x;
  double _delta_y;
  double _delta_z;

  std::vector<double> _x_center_bulk;
  std::vector<double> _y_center_bulk;
  std::vector<double> _z_center_bulk;

  std::map<BoxDomain*, std::vector<double>> _x_center_boundary;
  std::map<BoxDomain*, std::vector<double>> _y_center_boundary;
  std::map<BoxDomain*, std::vector<double>> _z_center_boundary;

  BoxDomain* _parent_domain;
};

#endif
