#ifndef PERIODICBOUNDARY_BOX_H
#define PERIODICBOUNDARY_BOX_H

#include "Boundary.h"

#include <vector>

class BoxDomain;

class PeriodicBoundary_Box: public CloneableBoundary<PeriodicBoundary_Box>{
public:
  PeriodicBoundary_Box(ParticleSpecies& particle_species, BoxDomain& domain_from, int boundary_idx_from);
  ~PeriodicBoundary_Box();

  virtual void operator()(int& i_part_from, double& dt_collision);

  virtual void user_initialize(int thread_id);
protected:
  BoxDomain* _box_domain_from;

  double _delta_pos;
  double _crossed_boundary_pos;
  std::vector<double>* _pos;
};


#endif
