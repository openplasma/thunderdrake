#ifndef TRANSMISSIONBOUNDARY_H
#define TRANSMISSIONBOUNDARY_H

#include "Boundary.h"
#include "ParticleSwarm.h"

class TransmissionBoundary: public CloneableBoundary<TransmissionBoundary>{
public:
  TransmissionBoundary(ParticleSpecies& particle_species, Domain& domain_from, int boundary_idx_from, Domain& domain_to);
  ~TransmissionBoundary();

  virtual void operator()(int& i_part_from, double& dt_collision);
};

#endif
