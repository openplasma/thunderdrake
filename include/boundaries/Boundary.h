#ifndef BOUNDARY_H
#define BOUNDARY_H

#include <memory>

// Forward Declaration
class Domain;
class ParticleSpecies;
class ParticleSwarm;

class Boundary{
  friend class ParticleSpecies;
public:
  Boundary(ParticleSpecies& particle_species, Domain& domain_from, int boundary_idx_from, Domain& domain_to);
  Boundary(ParticleSpecies& particle_species, Domain& domain_from, int boundary_idx_from);
  ~Boundary();

  // Getters
  ParticleSwarm* particle_swarm_from() const;
  ParticleSwarm* particle_swarm_to() const;
  ParticleSpecies* particle_species() const;
  int boundary_idx() const;
  Domain* domain_from() const;
  Domain* domain_to() const;
  int thread_id() const;

  // Operations
  virtual void before_boundary_check(int i_part_start, int i_part_end);
  virtual void operator()(int& i_part_from, double& dt_collision) = 0;
  virtual void after_boundary_check(int i_part_start, int i_part_end);

  virtual std::unique_ptr<Boundary> clone() const = 0; // Implemented by the template class below

  // Set member pointers to correct entities based on thread id (each thread has own ParticleSwarm for example)
  void initialize(int thread_id);
  virtual void user_initialize(int thread_id);
protected:
  Domain* _domain_from;
  Domain* _domain_to;
  ParticleSpecies* _particle_species;
  ParticleSwarm* _particle_swarm_from;
  ParticleSwarm* _particle_swarm_to;
  int _boundary_idx_from;
  int _thread_id;
};


/* This template class is designed using the paradigm: Curiously Recursive Template Programming (CRTP).
The problem: We want reactions/boundary conditions to be passed to a function by pointer/reference.
Because we are making use of polymorphism the type that these functions expect will be
pointers and references to the base class (Reaction or Boundary) eventhough we want to pass
for instance IonizationReaction or PeriodicBoundary.

This is all fine until we want to make a deep copy of these objects passed in the function.
We want to have a deep copy of for instance IonizationReaction when we only have
access to a reference or pointer to Reaction. (We do this for our multithreading purposes).

The Solution: Each derived class should have a function called "clone()" which
creates a deep copy of the reference or pointer passed. But, this was deemed to
much copy and paste work and also to much knowledge was needed by the users on how to
create their own custom Reaction or Boundary. For this we chose to create a "new"
class (which is the template class below called CloneableReaction) which makes the
clone() function automagically for any derived class. The only thing the user
now needs to do when they create a custom Reaction or Boundary is to inherit from
this cloneable class.

So for reactions:

class CustomReaction: public CloneableReaction<CustomReaction>
{
  all the typical class stuff goes here
}

and for boundary conditions:

class CustomBoundary: public CloneableBoundary<CustomBoundary>
{
  all the typical class stuff goes here
}
*/
template<typename DerivedBoundary>
class CloneableBoundary: public Boundary{
public:
  CloneableBoundary(ParticleSpecies& particle_species, Domain& domain_from, int boundary_idx_from, Domain& domain_to)
    : Boundary(particle_species, domain_from, boundary_idx_from, domain_to){};

  CloneableBoundary(ParticleSpecies& particle_species, Domain& domain_from, int boundary_idx_from)
    : Boundary(particle_species, domain_from, boundary_idx_from){};


  virtual std::unique_ptr<Boundary> clone() const{
    return std::make_unique<DerivedBoundary>(static_cast<const DerivedBoundary&>(*this));
  };
};

















#endif
