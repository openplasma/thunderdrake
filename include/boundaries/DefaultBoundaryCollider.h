#ifndef DEFAULTBOUNDARYCOLLIDER_H
#define DEFAULTBOUNDARYCOLLIDER_H

#include "BoundaryCollider.h"

class DefaultBoundaryCollider: public CloneableBoundaryCollider<DefaultBoundaryCollider>{
public:
  DefaultBoundaryCollider();
  ~DefaultBoundaryCollider();

  virtual void boundary_check(int i_part_start, int i_part_end);
};

#endif
