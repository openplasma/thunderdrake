cmake_minimum_required(VERSION 3.10.2)
project(ThunderDrake LANGUAGES CXX)
# This project will contain tests
include(CTest)
# We need OpenMP to be installed for this whole project to work
find_package(OpenMP REQUIRED)

# CREATE THE STATIC LIBRARY THUNDERDRAKE
################################################################################
# Create a variable containing all include directories
list(APPEND THUNDERDRAKE_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/include)
list(APPEND THUNDERDRAKE_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/include/boundaries)
list(APPEND THUNDERDRAKE_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/include/particles)
list(APPEND THUNDERDRAKE_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/include/domains)
list(APPEND THUNDERDRAKE_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/include/grids)
list(APPEND THUNDERDRAKE_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/include/inputdata)
list(APPEND THUNDERDRAKE_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/include/collisions)
list(APPEND THUNDERDRAKE_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/include/utils)
list(APPEND THUNDERDRAKE_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/include/utils/rng)

# Add thunderdrake as a target
add_library(thunderdrake STATIC "")
# Add all subdirectories which contain a CMakeLists.txt detailing which source files
# of the directory are available to the thunderdrake library
add_subdirectory(src/boundaries)
add_subdirectory(src/domains)
add_subdirectory(src/inputdata)
add_subdirectory(src/particles)
add_subdirectory(src/collisions)
add_subdirectory(src/utils)
# Set the output directory of the static library
set_target_properties(thunderdrake PROPERTIES ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)
# Specify the C++ standard to be used
target_compile_features(thunderdrake PRIVATE cxx_std_14)
# add the necessary compiler and linker flags for OpenMP
target_link_libraries(thunderdrake PRIVATE OpenMP::OpenMP_CXX)
# Add include directories for thunderdrake
target_include_directories(thunderdrake PUBLIC ${THUNDERDRAKE_INCLUDE_DIRS})

if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  # Default options: -g
  target_compile_options(thunderdrake PUBLIC $<$<CONFIG:Debug>: -Og -march=native>)
  # Default options: -O2 -g -DNDEBUG
  target_compile_options(thunderdrake PUBLIC $<$<CONFIG:RelWithDebInfo>: -march=native>)
  # Default options: -O3 -DNDEBUG
  target_compile_options(thunderdrake PUBLIC $<$<CONFIG:Release>: -march=native>)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
  # Default options: -g
  target_compile_options(thunderdrake PUBLIC $<$<CONFIG:Debug>: -O0 -traceback -xHost>)
  # Default options: -O2 -g -DNDEBUG
  target_compile_options(thunderdrake PUBLIC $<$<CONFIG:RelWithDebInfo>:-qopt-report=5 -qopt-report-phase=vec,openmp -ansi-alias -ipo -fargument-noalias -xHost>)
  # Default options: -O3 -DNDEBUG
  target_compile_options(thunderdrake PUBLIC $<$<CONFIG:Release>: -ansi-alias -ipo -fargument-noalias -xHost>)
endif()
################################################################################

# Only build the tests if BUILD_TESTING is ON (default).
# Disable building tests when invoking cmake by adding -DBUILD_TESTING=OFF
if (BUILD_TESTING)
  message(STATUS "Building Tests")
  # Add UT
  add_subdirectory(tests)
else()
  message(STATUS "NOT Building Tests")
endif()
